# zarinex Website V1

This is the main website for zarinex

[![pipeline status](https://git.arianagermany.com/bestino/frontend/bestino-website-v1/badges/master/pipeline.svg)](https://git.arianagermany.com/bestino/frontend/bestino-website-v1/-/commits/master)
[![coverage report](https://git.arianagermany.com/bestino/frontend/bestino-website-v1/badges/master/coverage.svg)](https://git.arianagermany.com/bestino/frontend/bestino-website-v1/-/commits/master)

## Getting Started

# Theme Guidl Line

<img src="./designGuidLine1.png"
alt="Markdown Monster icon"
style="height: 400px;" />

- please don't use the Colors and SHadows and the other default Value directly in Code. First put it in Material UI Theme withe createTheme() function and use theme (useTheme() or other) in code
