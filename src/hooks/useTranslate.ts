import React from 'react';
import { getPath, translations } from 'src/utils/i18n';
import useUserLanguage from './useUserLanguage';

export default function useTranslate() {
  const userLanguage = useUserLanguage();
  console.log('language is ', userLanguage);
  return React.useMemo(
    () =>
      function translate(key: string, options: { ignoreWarning?: boolean } = {}) {
        const { ignoreWarning = false } = options;
        const wordings = translations[userLanguage];

        if (!wordings) {
          console.error(`Missing language: ${userLanguage}.`);
          return '…';
        }

        const translation = getPath(wordings, key);

        if (!translation) {
          const fullKey = `${userLanguage}:${key}`;
          // No warnings in CI env
          if (!ignoreWarning && typeof window !== 'undefined') {
            console.error(`Missing translation for ${fullKey}`);
          }
          return getPath(translations.fa, key);
        }

        return translation;
      },
    [userLanguage],
  );
}
