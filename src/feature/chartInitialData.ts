import { alpha } from '@mui/system';

export const ChartInitialData = (positive: boolean) => {
  return {
    datasets: [
      {
        data: positive ? [5, 20, 35, 65, 50, 20, 62, 70, 70, 80, 150] : [150, 90, 25, 60, 50, 130, 87, 121, 99, 10, 1],
        fill: true,
        tension: 0.2,
        backgroundColor: alpha(positive ? '#339CFE' : '#FE33D1', 0.1),
        borderColor: positive ? '#339CFE' : '#FE33D1',
        borderWidth: 2,
        borderCapStyle: 'round',
        pointStyle: 'circle',
      },
    ],
    labels: ['', '', '', '', '', '', '', '', '', '', ''],
  };
};
