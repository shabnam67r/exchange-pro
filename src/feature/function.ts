const handleNatureNumber = (num: number) => Math.abs(num);

const validDisplayPrice = (num: number | string) => Number(num)?.toLocaleString();

const stringFormatTable = (e: string, limit = 8) => {
  return e?.length > limit ? `${e.substr(0, limit)} ...` : e;
};

const generatingUniqueId = () => {
  return Math.floor(Math.random() * 100000000000).toString();
};

export const handleColorStatus = (status: string) => {
  switch (status) {
    case 'rejected':
      return 'error';
    case 'success':
      return 'success';
    case 'pending':
      return 'warning';
    default:
      return 'error';
  }
};

export const handleTextStatus = (status: string) => {
  switch (status) {
    case 'rejected':
      return 'تایید نشده';
    case 'success':
      return 'تایید شده';
    case 'pending':
      return 'در حال بررسی';
    default:
      return 'تایید نشده';
  }
};

export { handleNatureNumber, validDisplayPrice, stringFormatTable, generatingUniqueId };
