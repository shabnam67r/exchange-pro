export const AssetsCardData = [
  { title: 'سکه آزادی', amount: 0 },
  { title: 'نیم سکه آزادی', amount: 0 },
  { title: 'ربع سکه آزادی', amount: 0 },
  { title: 'سکه امامی', amount: 0 },
  { title: 'نیم سکه امامی', amount: 0 },
  { title: 'ربع سکه امامی', amount: 0 },
  { title: 'ریالی', amount: 1000000 },
];

export const initialDataWalletPage = [
  {
    depositType: 'درگاه پرداخت آنلاین',
    amount: 10000,
    paymentCode: '21345687',
    creatAt: '1400/5/4',
    status: 'pending',
  },
  {
    depositType: 'درگاه پرداخت آنلاین',
    amount: 10000,
    paymentCode: '21345687',
    creatAt: '1400/5/4',
    status: 'rejected',
  },
  {
    depositType: 'درگاه پرداخت آنلاین',
    amount: 10000,
    paymentCode: '21345687',
    creatAt: '1400/5/4',
    status: 'success',
  },
  {
    depositType: 'درگاه پرداخت آنلاین',
    amount: 10000,
    paymentCode: '21345687',
    creatAt: '1400/5/4',
    status: 'pending',
  },
  {
    depositType: 'درگاه پرداخت آنلاین',
    amount: 10000,
    paymentCode: '21345687',
    creatAt: '1400/5/4',
    status: 'rejected',
  },
  {
    depositType: 'درگاه پرداخت آنلاین',
    amount: 10000,
    paymentCode: '21345687',
    creatAt: '1400/5/4',
    status: 'success',
  },
  {
    depositType: 'درگاه پرداخت آنلاین',
    amount: 10000,
    paymentCode: '21345687',
    creatAt: '1400/5/4',
    status: 'pending',
  },
  {
    depositType: 'درگاه پرداخت آنلاین',
    amount: 10000,
    paymentCode: '21345687',
    creatAt: '1400/5/4',
    status: 'rejected',
  },
  {
    depositType: 'درگاه پرداخت آنلاین',
    amount: 10000,
    paymentCode: '21345687',
    creatAt: '1400/5/4',
    status: 'success',
  },
  {
    depositType: 'درگاه پرداخت آنلاین',
    amount: 10000,
    paymentCode: '21345687',
    creatAt: '1400/5/4',
    status: 'rejected',
  },
];

export const coinSelectBoxList = [
  'سکه آزادی - ریال',
  'نیم سکه آزادی - ریال',
  'ربع سکه آزادی - ریال',
  'سکه امامی - ریال',
  'نیم سکه امامی - ریال',
  'ربع سکه امامی - ریال',
];

export const coinAndMoneySelectBoxList = [
  'برداشت ریالی',
  'برداشت سکه',
  'واریز درگاه پرداخت',
  'واریز بانکی',
  'واریز سکه',
  'معاملات خرید',
  'معاملات فروش',
  'فاکتور معاملات',
  'لاگین',
];
