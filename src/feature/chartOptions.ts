import _ from 'lodash';

/******************************************************************************
 *                            Chart Option [ Axes Less ]
 *****************************************************************************/

export const OptionChartAxesLess = {
  responsive: true,
  scales: {
    xAxes: {
      grid: {
        display: false,
        drawBorder: false,
      },
    },
    yAxes: {
      grid: {
        display: false,
        drawBorder: false,
      },
      ticks: {
        display: false,
      },
    },
  },
  plugins: {
    legend: {
      display: false,
    },
  },
  pointRadius: 0,
};

export const OptionChartAxesLessAndWithPoint = (len: number) => {
  let initArray = _.range(0, len);
  let finalArray = initArray.map((_, index) => {
    return initArray.length - 1 === index ? 5 : 0;
  });
  return { ...OptionChartAxesLess, pointRadius: finalArray };
};
