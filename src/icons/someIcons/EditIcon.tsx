export default function EditIcon() {
  return (
    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M10.535 2.55664H7.10757C4.28882 2.55664 2.52148 4.55222 2.52148 7.37739V14.9986C2.52148 17.8237 4.28057 19.8193 7.10757 19.8193H15.1962C18.0242 19.8193 19.7832 17.8237 19.7832 14.9986V11.3062"
        stroke="url(#paint0_linear_369:23335)"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.09208 10.0111L14.9423 3.16086C15.7957 2.30836 17.179 2.30836 18.0324 3.16086L19.148 4.27644C20.0014 5.12986 20.0014 6.51403 19.148 7.36653L12.2647 14.2498C11.8917 14.6229 11.3857 14.8328 10.8577 14.8328H7.42383L7.50999 11.3678C7.52283 10.8581 7.73091 10.3723 8.09208 10.0111Z"
        stroke="url(#paint1_linear_369:23335)"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M13.9023 4.21973L18.0878 8.40523"
        stroke="url(#paint2_linear_369:23335)"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <defs>
        <linearGradient
          id="paint0_linear_369:23335"
          x1="1.2551"
          y1="-3.44613"
          x2="26.4545"
          y2="7.36249"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#FF9500" />
          <stop offset="1" stopColor="#FF5E3A" />
          <stop offset="1" stopColor="#FF5E3A" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_369:23335"
          x1="6.51675"
          y1="-1.75954"
          x2="24.5421"
          y2="6.00562"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#FF9500" />
          <stop offset="1" stopColor="#FF5E3A" />
          <stop offset="1" stopColor="#FF5E3A" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_369:23335"
          x1="13.5953"
          y1="2.7643"
          x2="19.7053"
          y2="5.38519"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#FF9500" />
          <stop offset="1" stopColor="#FF5E3A" />
          <stop offset="1" stopColor="#FF5E3A" />
        </linearGradient>
      </defs>
    </svg>
  );
}
