export default function UserCounterIcon() {
  return (
    <svg width="78" height="78" viewBox="0 0 78 78" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M56.2055 23.6959C56.2055 33.2413 48.5522 40.8951 39.0001 40.8951C29.4513 40.8951 21.7946 33.2413 21.7946 23.6959C21.7946 14.1506 29.4513 6.5 39.0001 6.5C48.5522 6.5 56.2055 14.1506 56.2055 23.6959ZM39 71.4998C24.9027 71.4998 13 69.2085 13 60.3684C13 51.5251 24.9775 49.315 39 49.315C53.1005 49.315 65 51.6063 65 60.4464C65 69.2897 53.0225 71.4998 39 71.4998Z"
        fill="url(#paint0_linear_1008:38599)"
      />
      <defs>
        <linearGradient id="paint0_linear_1008:38599" x1="13" y1="6" x2="65" y2="71" gradientUnits="userSpaceOnUse">
          <stop stopColor="#9A9AAF" />
          <stop offset="1" stopColor="#9A9AAF" stopOpacity="0.5" />
        </linearGradient>
      </defs>
    </svg>
  );
}
