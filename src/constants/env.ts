export const DEV = process.env.NODE_ENV !== 'production';

export const WAITING_WITH = 3000;
export const REVALIDATE_TIME = 40;

/******************************************************************************
 *                            MSG Yup
 *****************************************************************************/
export const REQUIRED = 'اجباری';
export const BE_LESS = (props: number) => ` کمتر از ${props} کاراکتر باشد `;
export const BE_MORE = (props: number) => ` بیشتر از ${props} کاراکتر باشد `;
export const FILE_TOO_LARGE = 'حجم فایل بیشتر از مقدار تعیین شده است .';
export const AVATAR_SIZE = 5_000_000;
export const DOCUMENT_SIZE = 5_000_000;
export const NORMAL_MONTHS_RANSOM = 4_800_000_000;
export const UnNORMAL_MONTHS_RANSOM = 6_400_000_000;
export const ONE_OF = 'مورد انتخاب شده معتبر نیست .';
export const NOT_VALID = 'معتبر نیست';
