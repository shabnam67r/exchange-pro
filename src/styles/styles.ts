import { adaptV4Theme, alpha, createTheme, ThemeOptions } from '@mui/material/styles';
import { detect } from 'detect-browser';
import merge from 'lodash/merge';
import { defaultTheme } from './defaultTheme';
import { grey } from '@mui/material/colors';

const available = [
  'edge',
  'edge-ios',
  'beaker',
  'edge-chromium',
  'chrome',
  'chromium-webview',
  'opera-mini',
  'android',
  'ios',
  'safari',
  'ios-webview',
];

export const fonts = {
  text: 'Vazir',
  textBold: 'Vazir Bold',
  textLight: 'Vazir Light',
  title: 'Anjoman',
  titleBold: 'Anjoman Bold',
};

const backdropAvailable = available.includes(detect()!.name);

const theme = createTheme();

export const availabilityColor = (d: string = '', level: 'default' | 'low' | 'high' = 'default'): string => {
  let newColor: string = '';
  if (d.includes('rgb')) {
    if (backdropAvailable) {
      newColor = alpha(d, defaultTheme?.states?.transparency[level].rgb || 1);
    }
  }
  if (d.includes('#')) {
    if (backdropAvailable) {
      newColor = d + defaultTheme?.states?.transparency[level];
    }
  }
  if (!backdropAvailable) {
    newColor = d;
  }
  return newColor;
};
const baseOptions: ThemeOptions = {
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
  shape: {
    borderRadius: defaultTheme.shape,
  },
  // direction: 'rtl',
  typography: {
    h1: {
      fontSize: 48,
      fontFamily: fonts.title,
    },
    h2: {
      fontSize: 32,
      fontFamily: fonts.title,
    },
    subtitle1: {
      fontSize: 22,
      fontFamily: fonts.textBold,
    },
    subtitle2: {
      fontSize: 18,
      fontFamily: fonts.text,
    },
    body1: {
      fontSize: 16,
      fontFamily: fonts.textBold,
    },
    body2: {
      fontSize: 14,
      fontFamily: fonts.text,
    },
    caption: {
      fontSize: 12,
      fontFamily: fonts.textLight,
    },
    overline: {
      fontSize: 10,
      fontFamily: fonts.text,
    },
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        html: {
          scrollBehavior: 'smooth',
          scrollbarWidth: 'thin',
        },
        body: {
          scrollBehavior: 'smooth',
          direction: 'rtl !important',
          paddingRight: '0 !important',
          overflowX: 'hidden !important',
        },

        '::-webkit-scrollbar': {
          width: 8,
          background: 'transparent',
        },
        '::-webkit-scrollbar-thumb': {
          background: defaultTheme.palette?.primary?.main,
          borderRadius: defaultTheme.shape + 'px',
          cursor: 'pointer',
        },

        '.ps__rail-y': {
          zIndex: '3000',
          right: '0 !important',
        },
      },
    },
    MuiTooltip: { styleOverrides: { tooltip: { background: 'transparent' } } },
    MuiAppBar: {
      styleOverrides: {
        root: {
          backgroundImage: 'none',
          boxShadow: 'none',
          borderRadius: '0px',
          height: 80,
        },
      },
    },
    MuiDrawer: {
      styleOverrides: {
        paper: {
          borderRadius: 0,
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          backgroundImage: 'none !important',
          backdropFilter: defaultTheme.states?.backDrop.default,
          borderRadius: defaultTheme.shape + 'px',
          padding: theme.spacing(
            defaultTheme.size.padding.y,
            defaultTheme.size.padding.x,
            defaultTheme.size.padding.y,
            defaultTheme.size.padding.x,
          ),
        },
      },
    },

    MuiBackdrop: {
      styleOverrides: {
        root: {
          backdropFilter: defaultTheme.states?.backDrop.default,
          backgroundColor: 'transparent',
        },
      },
    },
    MuiSelect: {
      styleOverrides: { select: { padding: defaultTheme.size.padding.x * 2 } },
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          borderRadius: defaultTheme.shape,
        },
        sizeMedium: {
          padding: theme.spacing(
            defaultTheme.size.component.medium.t,
            defaultTheme.size.component.medium.r,
            defaultTheme.size.component.medium.b,
            defaultTheme.size.component.medium.l,
          ),
        },
        sizeLarge: {
          padding: theme.spacing(
            defaultTheme.size.component.large.t,
            defaultTheme.size.component.large.r,
            defaultTheme.size.component.large.b,
            defaultTheme.size.component.large.l,
          ),
        },
        sizeSmall: {
          padding: theme.spacing(
            defaultTheme.size.component.small.t,
            defaultTheme.size.component.small.r,
            defaultTheme.size.component.small.b,
            defaultTheme.size.component.small.l,
          ),
        },
      },
    },
    MuiButtonBase: {
      defaultProps: { disableRipple: true },
      styleOverrides: {
        root: {
          fontFamily: `${fonts.text} !important`,
        },
      },
    },

    MuiButton: {
      defaultProps: { variant: 'contained' },
      styleOverrides: {
        root: {
          transition: theme.transitions.create(['box-shadow'], { duration: 100 }),
          borderRadius: defaultTheme.shape,
          minWidth: '120px',
          boxShadow: 'none',
          ':hover': {
            boxShadow: 'none',
          },
        },
        sizeMedium: {
          padding: theme.spacing(
            defaultTheme.size.component.medium.t,
            defaultTheme.size.component.medium.r,
            defaultTheme.size.component.medium.b,
            defaultTheme.size.component.medium.l,
          ),
        },
        sizeLarge: {
          padding: theme.spacing(
            defaultTheme.size.component.large.t,
            defaultTheme.size.component.large.r * 20,
            defaultTheme.size.component.large.b,
            defaultTheme.size.component.large.l * 20,
          ),
        },
        sizeSmall: {
          padding: theme.spacing(
            defaultTheme.size.component.small.t,
            defaultTheme.size.component.small.r,
            defaultTheme.size.component.small.b,
            defaultTheme.size.component.small.l,
          ),
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          width: '100%',
          marginTop: 5,
        },
      },
    },

    MuiMenu: {
      styleOverrides: {
        root: {
          '& .MuiBackdrop-root': {
            backdropFilter: 'none',
            backgroundColor: 'inherit',
          },
        },
        paper: {
          overflow: 'visible !important',
          marginTop: '10px',
          backgroundImage: 'none !important',
          border: `1px solid ${alpha(defaultTheme.palette?.text.dark?.disabled || '', 0.2)}`,
          padding: '8px 0 !important',
          borderRadius: `${defaultTheme.shape / 3}px !important`,
        },
      },
    },

    MuiMenuItem: {
      styleOverrides: {
        root: {
          '&:not(:last-child)': {
            borderBottom: `1px solid ${alpha(defaultTheme.palette?.text.dark?.disabled || '', 0.2)}`,
            padding: '10px 14px',
          },
        },
      },
    },

    MuiBottomNavigationAction: {
      styleOverrides: {
        label: {
          fontFamily: fonts.text,
        },
      },
    },
    MuiFormControlLabel: {
      styleOverrides: {
        label: {
          fontFamily: fonts.text,
          fontSize: 14,
        },
      },
    },

    MuiCard: {
      styleOverrides: {
        root: {
          borderRadius: defaultTheme?.shape,
          border: `2px solid ${defaultTheme?.palette?.divider}`,
          backdropFilter: 'blur(16px)',
          [theme.breakpoints.up('md')]: {
            padding: '32px 39px',
          },
        },
      },
    },

    MuiChip: {
      styleOverrides: {
        root: {
          fontFamily: fonts.text,
        },
      },
    },

    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          width: '100%',
          height: '100% !important',
          caretColor: defaultTheme.palette?.primary?.main,
          borderRadius: defaultTheme.shape * 1.5,
        },
        sizeSmall: {
          padding: theme.spacing(defaultTheme.size.component.medium.r / 2, defaultTheme.size.component.medium.t / 2),
        },
        input: {
          padding: theme.spacing(
            defaultTheme.size.component.medium.t * 1.8,
            defaultTheme.size.component.medium.r,
            defaultTheme.size.component.medium.b * 1.8,
            defaultTheme.size.component.medium.l * 1.8,
          ),
        },
      },
    },
    MuiDialog: {
      styleOverrides: {
        paper: {
          padding: theme.spacing(
            defaultTheme.size.component.medium.t * 2.8,
            defaultTheme.size.component.medium.r * 2.8,
            defaultTheme.size.component.medium.b * 2.8,
            defaultTheme.size.component.medium.l * 2.8,
          ),
          '& .MuiDialogTitle-root': {
            fontFamily: fonts.textBold,
          },
        },
      },
    },
  },
};

const LIGHTThemesOptions: ThemeOptions = {
  shadows: defaultTheme.shadows.light,
  palette: {
    mode: 'light',
    primary: defaultTheme.palette?.primary,
    secondary: defaultTheme.palette?.secondary,
    background: defaultTheme.palette?.background?.light,
    divider: defaultTheme.palette?.divider?.light,
    common: defaultTheme.palette?.common?.light,
    // warning: defaultTheme.palette?.warning,
    // error: defaultTheme.palette?.error,
    // info: defaultTheme.palette?.info,
    // success: defaultTheme.palette?.success,
    // action: defaultTheme.palette?.action?.dark,
  },
  components: {
    MuiBackdrop: {
      styleOverrides: { root: { background: defaultTheme.palette?.common?.light?.backDrop } },
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          color: defaultTheme.palette?.text?.light?.secondary,
          background: defaultTheme?.palette?.common?.light?.primaryCommonGradient,
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          boxShadow: defaultTheme.shadows.light[2],
          backgroundColor: availabilityColor(defaultTheme?.palette?.background?.light?.paper, 'high'),
        },
      },
    },
    MuiAppBar: {
      styleOverrides: {
        root: {
          backgroundColor: defaultTheme.palette?.background.light?.paper,
          color: defaultTheme.palette?.text?.light?.primary,
          backdropFilter: defaultTheme.states?.backDrop.default,

          display: 'flex',
          alignItems: 'center',
        },
      },
    },
    // input design :
    MuiOutlinedInput: {
      styleOverrides: {
        notchedOutline: {
          borderColor: 'transparent  !important',
          '&:focus': {
            borderColor: 'transparent  !important',
          },
        },
        root: {
          boxShadow: defaultTheme.shadows.light[1],
          background: defaultTheme.palette?.background.light?.default,
          border: `2px solid ${defaultTheme.palette?.divider?.light}`,
          '&.Mui-focused': {
            boxShadow: `0px 0px 0px 5px ${alpha(defaultTheme.palette?.primary?.light || '', 0.25)}`,
          },
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          color: defaultTheme.palette?.text.light?.secondary,
          '&.Mui-focused': {
            marginTop: '-5%',
            color: defaultTheme.palette?.text.light?.primary,
          },
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          minWidth: 200,
          color: defaultTheme.palette?.text.light?.primary,
        },
      },
    },
    MuiChip: {
      defaultProps: { color: 'primary' },
      variants: [
        {
          props: { color: 'primary' },
          style: {
            backgroundColor: defaultTheme?.palette?.common?.light?.primaryCommonGradient,
            color: defaultTheme.palette?.common?.light?.white,
          },
        },
      ],
    },
    MuiDrawer: {
      styleOverrides: {
        paper: {
          backgroundColor: defaultTheme.palette?.background?.light?.paper,
        },
      },
    },
    MuiSwitch: {},
    // button design :

    MuiMenu: {
      styleOverrides: {
        paper: {
          backgroundColor: defaultTheme.palette?.background.light?.default,
        },
      },
    },

    MuiMenuItem: {
      styleOverrides: {
        root: {
          color: alpha(defaultTheme.palette?.text?.light?.primary || '', 0.7),
        },
      },
    },

    MuiButton: {
      variants: [
        {
          props: { variant: 'contained', color: 'primary' },
          style: {
            backgroundColor: defaultTheme?.palette?.common?.light?.primaryCommonGradient,
            color: defaultTheme.palette?.common?.light?.white,
            border: '3px solid ',
            borderColor: defaultTheme.palette?.primary?.light,
            ':hover': {
              backgroundPosition: '100% 0',
              mozTransition: 'all .4s ease-in-out',
              OTransition: 'all .4s ease-in-out',
              WebkitTransition: 'all .4s ease-in-out',
              transition: 'all .4s ease-in-out',
            },
            ':active': {
              boxShadow: `0px 0px 0px 5px ${alpha(defaultTheme.palette?.primary?.light || '', 0.25)}`,
            },
          },
        },
        {
          props: { variant: 'outlined', color: 'primary' },
          style: {
            backgroundColor: 'inherit',
            border: '3px solid ',
            borderColor: defaultTheme.palette?.primary?.light,
            color: defaultTheme.palette?.primary?.light,
            ':hover': {
              border: '3px solid ',
              backgroundColor: 'inherit',
            },
            ':active': {
              boxShadow: `0px 0px 0px 5px ${alpha(defaultTheme.palette?.primary?.light || '', 0.25)}`,
            },
          },
        },
        {
          props: { variant: 'outlined', color: 'secondary' },
          style: {
            backgroundColor: 'inherit',
            border: '3px solid ',
            borderColor: grey[300],
            color: defaultTheme.palette?.primary?.light,
            ':hover': {
              border: '3px solid ',
              borderColor: grey[300],
              backgroundColor: 'inherit',
            },
            ':active': {
              boxShadow: `0px 0px 0px 5px ${alpha(grey[300], 0.25)}`,
            },
          },
        },
        {
          props: { variant: 'text', color: 'primary' },
          style: {
            backgroundColor: 'inherit',
            color: defaultTheme.palette?.primary?.light,
            ':hover': {
              backgroundColor: 'inherit',
            },
            ':active': {
              backgroundColor: defaultTheme.palette?.secondary?.light,
            },
          },
        },
      ],
    },
    MuiInputBase: {
      styleOverrides: {
        sizeSmall: {},
        input: {},
        root: {
          backgroundColor: availabilityColor(defaultTheme.palette?.background?.light?.paper, 'high'),
          backdropFilter: defaultTheme.states?.backDrop.default,
          transition: theme.transitions.create(['border-color', 'background-color', 'box-shadow']),
          borderRadius: defaultTheme.shape,
          boxShadow: defaultTheme.shadows.light[defaultTheme.components.inputBase.shadow.default],
          '&:hover': {
            boxShadow: defaultTheme.shadows.light[defaultTheme.components.inputBase.shadow.hover],
          },
          '&:focus': {
            boxShadow: defaultTheme.shadows.light[defaultTheme.components.inputBase.shadow.focus] + '!important',
          },
          '&:focus-within': {
            boxShadow: defaultTheme.shadows.light[defaultTheme.components.inputBase.shadow.focus] + '!important',
          },
          '&:active': {
            boxShadow: defaultTheme.shadows.light[defaultTheme.components.inputBase.shadow.active],
          },
        },
      },
    },
  },
};

const DARKThemesOptions: ThemeOptions = {
  shadows: defaultTheme.shadows.dark,
  palette: {
    mode: 'dark',
    primary: defaultTheme.palette?.primary,
    secondary: defaultTheme.palette?.secondary,
    background: defaultTheme.palette?.background?.dark,
    divider: defaultTheme.palette?.divider?.dark,
    common: defaultTheme.palette?.common?.dark,
    // warning: defaultTheme.palette?.warning,
    // error: defaultTheme.palette?.error,
    // info: defaultTheme.palette?.info,
    // success: defaultTheme.palette?.success,
    // action: defaultTheme.palette?.action?.dark,
  },
  components: {
    MuiBackdrop: {
      styleOverrides: { root: { background: defaultTheme.palette?.common?.dark?.backDrop } },
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          color: defaultTheme.palette?.text?.dark?.secondary,
          background: defaultTheme?.palette?.common?.dark?.primaryCommonGradient,
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          boxShadow: defaultTheme.shadows.dark[2],
          backgroundColor: availabilityColor(defaultTheme?.palette?.background?.dark?.paper, 'high'),
        },
      },
    },
    MuiAppBar: {
      styleOverrides: {
        root: {
          display: 'flex',
          alignItems: 'center',
          backgroundColor: defaultTheme.palette?.background.dark?.paper,
          color: defaultTheme.palette?.text?.dark?.primary,
          backdropFilter: defaultTheme.states?.backDrop.default,
        },
      },
    },
    // input design :
    MuiOutlinedInput: {
      styleOverrides: {
        // input: {
        //   '&:-webkit-autofill': {
        //     WebkitBoxShadow: `0 0 0 100px ${defaultTheme.palette?.background.dark?.paper} inset`,
        //     WebkitTextFillColor: 'red',
        //   },
        // },
        notchedOutline: {
          borderColor: 'transparent  !important',
          '&:focus': {
            borderColor: 'transparent  !important',
          },
        },
        root: {
          boxShadow: defaultTheme.shadows.dark[1],
          background: defaultTheme.palette?.background.dark?.paper,
          // backgroundImage: 'linear-gradient(rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.05))',
          border: `2px solid ${defaultTheme.palette?.divider?.dark}`,
          '&.Mui-focused': {
            boxShadow: `0px 0px 0px 5px ${alpha(defaultTheme.palette?.primary?.dark || '', 0.25)}`,
          },
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          color: defaultTheme.palette?.text.dark?.secondary,
          '&.Mui-focused': {
            marginTop: '-5%',
            color: defaultTheme.palette?.text.dark?.primary,
          },
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          minWidth: 200,
          color: defaultTheme.palette?.text.dark?.primary,
        },
      },
    },
    MuiChip: {
      defaultProps: { color: 'primary' },
      variants: [
        {
          props: { color: 'primary' },
          style: {
            backgroundColor: defaultTheme?.palette?.common?.dark?.primaryCommonGradient,
            color: defaultTheme.palette?.common?.dark?.white,
          },
        },
      ],
    },
    MuiDrawer: {
      styleOverrides: {
        paper: {
          backgroundColor: defaultTheme.palette?.background?.dark?.paper,
        },
      },
    },
    MuiSwitch: {},

    MuiBottomNavigation: {
      styleOverrides: {
        root: {
          backgroundColor: 'transparent',
        },
      },
    },

    MuiMenu: {
      styleOverrides: {
        paper: {
          backgroundColor: `${alpha(defaultTheme.palette?.background.dark?.default || '', 0.7)} !important`,
        },
      },
    },

    MuiMenuItem: {
      styleOverrides: {
        root: {
          color: alpha(defaultTheme.palette?.text?.dark?.primary || '', 0.5),
        },
      },
    },

    MuiButton: {
      variants: [
        {
          props: { variant: 'contained', color: 'primary' },
          style: {
            background: defaultTheme?.palette?.common?.dark?.primaryCommonGradient,
            color: defaultTheme.palette?.common?.dark?.white,
            border: '3px solid ',
            borderColor: defaultTheme.palette?.primary?.dark,
            ':hover': {
              backgroundPosition: '100% 0',
              // mozTransition: 'all .4s ease-in-out',
              // OTransition: 'all .4s ease-in-out',
              // WebkitTransition: 'all .4s ease-in-out',
              // transition: 'all .4s ease-in-out',
            },
            ':active': {
              boxShadow: `0px 0px 0px 5px ${alpha(defaultTheme.palette?.primary?.dark || '', 0.25)}`,
            },
          },
        },
        {
          props: { variant: 'outlined', color: 'primary' },
          style: {
            backgroundColor: 'inherit',
            border: '3px solid ',
            borderColor: defaultTheme.palette?.primary?.dark,
            color: defaultTheme.palette?.primary?.dark,
            ':hover': {
              border: '3px solid ',
              backgroundColor: 'inherit',
            },
            ':active': {
              boxShadow: `0px 0px 0px 5px ${alpha(defaultTheme.palette?.primary?.dark || '', 0.25)}`,
            },
          },
        },
        {
          props: { variant: 'outlined', color: 'secondary' },
          style: {
            backgroundColor: 'inherit',
            border: '3px solid ',
            borderColor: grey[300],
            color: defaultTheme.palette?.primary?.dark,
            ':hover': {
              border: '3px solid ',
              borderColor: grey[300],
              backgroundColor: 'inherit',
            },
            ':active': {
              boxShadow: `0px 0px 0px 5px ${alpha(grey[300], 0.25)}`,
            },
          },
        },
        {
          props: { variant: 'text', color: 'primary' },
          style: {
            backgroundColor: 'inherit',
            color: defaultTheme.palette?.primary?.dark,
            ':hover': {
              backgroundColor: 'inherit',
            },
            ':active': {
              backgroundColor: defaultTheme.palette?.secondary?.dark,
            },
          },
        },
      ],
    },
    MuiInputBase: {
      styleOverrides: {
        sizeSmall: {},
        input: {},
        root: {
          backgroundColor: availabilityColor(defaultTheme.palette?.background?.dark?.paper, 'high'),
          backdropFilter: defaultTheme.states?.backDrop.default,
          transition: theme.transitions.create(['border-color', 'background-color', 'box-shadow']),
          borderRadius: defaultTheme.shape,
          boxShadow: defaultTheme.shadows.dark[defaultTheme.components.inputBase.shadow.default],
          '&:hover': {
            boxShadow: defaultTheme.shadows.dark[defaultTheme.components.inputBase.shadow.hover],
          },
          '&:focus': {
            boxShadow: defaultTheme.shadows.dark[defaultTheme.components.inputBase.shadow.focus] + '!important',
          },
          '&:focus-within': {
            boxShadow: defaultTheme.shadows.dark[defaultTheme.components.inputBase.shadow.focus] + '!important',
          },
          '&:active': {
            boxShadow: defaultTheme.shadows.dark[defaultTheme.components.inputBase.shadow.active],
          },
        },
      },
    },
    MuiDialog: {
      styleOverrides: {
        paper: {
          backgroundColor: defaultTheme.palette?.common?.dark?.card,
        },
      },
    },
  },
};

export const createCustomTheme = (t: 'DARK' | 'LIGHT') => {
  let themeOptions = t === 'DARK' ? DARKThemesOptions : LIGHTThemesOptions;

  if (!themeOptions) {
    console.warn(new Error(`The theme ${t} is not valid`));
    themeOptions = themeOptions['LIGHT'];
  }
  let theme = createTheme(merge({}, baseOptions, themeOptions));

  return theme;
};
