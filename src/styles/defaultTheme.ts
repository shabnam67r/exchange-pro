import { DefaultTheme, StatesInterface } from 'src/interfaces/themeOptions';

// in default theme you can add all general color for website:
const states: StatesInterface = {
  navbar: 'md',
  transparency: {
    default: { hex: '88', rgb: 0.6 },
    low: { hex: '33', rgb: 0.3 },
    high: { hex: 'dd', rgb: 0.8 },
  },
  backDrop: {
    default: 'saturate(180%) blur(20px)',
    low: 'saturate(180%) blur(5px)',
    high: 'saturate(180%) blur(35px)',
  },
};

export const defaultTheme: DefaultTheme = {
  shadows: {
    light: [
      'none',
      '0px 0px 2px rgba(14, 31, 53, 0.12)',
      '0px 0px 2px rgba(14, 31, 53, 0.12), 0px 1px 4px rgba(14, 31, 53, 0.06)',
      '0px 1px 4px rgba(14, 31, 53, 0.12), 0px 4px 8px rgba(14, 31, 53, 0.08)',
      '0px 1px 4px rgba(14, 31, 53, 0.12), 0px 4px 8px rgba(14, 31, 53, 0.1), 0px 6px 12px rgba(14, 31, 53, 0.08)',
      '0px 2px 4px rgba(14, 31, 53, 0.06), 0px 6px 12px rgba(14, 31, 53, 0.08), 0px 12px 20px rgba(14, 31, 53, 0.06)',
      '0px 3px 6px rgba(14, 31, 53, 0.08), 0px 6px 12px rgba(14, 31, 53, 0.12), 0px 14px 24px rgba(14, 31, 53, 0.08)',
      // tslint:disable-next-line: max-line-length
      '0px 4px 8px rgba(14, 31, 53, 0.1), 0px 8px 16px rgba(14, 31, 53, 0.16), 0px 16px 28px -1px rgba(14, 31, 53, 0.1)',
      '0px 6px 10px rgba(14, 31, 53, 0.12), 0px 12px 18px rgba(14, 31, 53, 0.2), 0px 20px 40px -1px rgba(14, 31, 53, 0.12)',
      '0px 5px 10px rgba(14, 31, 53, 0.06), 0px 10px 20px rgba(14, 31, 53, 0.12), 0px 16px 24px -1px rgba(14, 31, 53, 0.12), 0px 20px 38px -2px rgba(14, 31, 53, 0.06)',
      '0px 5px 10px rgba(14, 31, 53, 0.08), 0px 10px 20px rgba(14, 31, 53, 0.16), 0px 24px 32px -1px rgba(14, 31, 53, 0.16), 0px 32px 64px -2px rgba(14, 31, 53, 0.08)',
      '0px 5px 10px -1px rgba(14, 31, 53, 0.06), 0px 10px 20px -2px rgba(14, 31, 53, 0.1), 0px 16px 32px -3px rgba(14, 31, 53, 0.12), 0px 32px 64px -4px rgba(14, 31, 53, 0.14), 0px 40px 72px -5px rgba(14, 31, 53, 0.24)',
      '0px 3px 6px rgba(238, 49, 83, .5), 0px 6px 12px rgba(238, 49, 83, .5), 0px 14px 24px rgba(238, 49, 83, .5)',
      '0px 0px 20px 6px rgb(255 120 29)',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
    ],
    dark: [
      'none',
      '0px 3px 3px rgba(14, 31, 53, 0.12)',
      '0px 0px 2px rgba(14, 31, 53, 0.12), 0px 1px 4px rgba(14, 31, 53, 0.06)',
      '0px 1px 4px rgba(14, 31, 53, 0.12), 0px 4px 8px rgba(14, 31, 53, 0.08)',
      '0px 1px 4px rgba(14, 31, 53, 0.12), 0px 4px 8px rgba(14, 31, 53, 0.1), 0px 6px 12px rgba(14, 31, 53, 0.08)',
      '0px 2px 4px rgba(14, 31, 53, 0.06), 0px 6px 12px rgba(14, 31, 53, 0.08), 0px 12px 20px rgba(14, 31, 53, 0.06)',
      '0px 3px 6px rgba(14, 31, 53, 0.08), 0px 6px 12px rgba(14, 31, 53, 0.12), 0px 14px 24px rgba(14, 31, 53, 0.08)',
      // tslint:disable-next-line: max-line-length
      '0px 4px 8px rgba(14, 31, 53, 0.1), 0px 8px 16px rgba(14, 31, 53, 0.16), 0px 16px 28px -1px rgba(14, 31, 53, 0.1)',
      '0px 6px 10px rgba(14, 31, 53, 0.12), 0px 12px 18px rgba(14, 31, 53, 0.2), 0px 20px 40px -1px rgba(14, 31, 53, 0.12)',
      '0px 5px 10px rgba(14, 31, 53, 0.06), 0px 10px 20px rgba(14, 31, 53, 0.12), 0px 16px 24px -1px rgba(14, 31, 53, 0.12), 0px 20px 38px -2px rgba(14, 31, 53, 0.06)',
      '0px 5px 10px rgba(14, 31, 53, 0.08), 0px 10px 20px rgba(14, 31, 53, 0.16), 0px 24px 32px -1px rgba(14, 31, 53, 0.16), 0px 32px 64px -2px rgba(14, 31, 53, 0.08)',
      '0px 5px 10px -1px rgba(14, 31, 53, 0.06), 0px 10px 20px -2px rgba(14, 31, 53, 0.1), 0px 16px 32px -3px rgba(14, 31, 53, 0.12), 0px 32px 64px -4px rgba(14, 31, 53, 0.14), 0px 40px 72px -5px rgba(14, 31, 53, 0.24)',
      '0px 5px 10px -1px rgba(14, 31, 53, 0.06), 0px 10px 20px -2px rgba(14, 31, 53, 0.1), 0px 16px 32px -3px rgba(14, 31, 53, 0.12), 0px 32px 64px -4px rgba(14, 31, 53, 0.14), 0px 56px 84px -5px rgba(14, 31, 53, 0.32)',
      '0px 3px 6px rgba(238, 49, 83, .5), 0px 6px 12px rgba(238, 49, 83, .5), 0px 14px 24px rgba(238, 49, 83, .5)',
      '0px 0px 20px 6px rgb(255 120 29)',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
      'empty',
    ],
  },
  palette: {
    common: {
      dark: {
        white: '#fff',
        black: '#000',
        backDrop: '#0000004D',
        card: '#29236A',
        drawer: 'rgba(41, 35, 106, 0.45)',
        primaryCommonGradient: 'linear-gradient(113.22deg, #FF9500 -15.57%, #FF5E3A 105.38%, #FF5E3A 105.38%)',
        secondaryCommonGradient: 'linear-gradient(180deg, #B52EED 0%, #3A3BBB 100%)',
      },
      light: {
        white: '#fff',
        black: '#000',
        backDrop: '#0000004D',
        card: '#F7F6FB',
        drawer: 'rgba(41, 35, 106, 0.45)',
        primaryCommonGradient: 'linear-gradient(113.22deg, #FF9500 -15.57%, #FF5E3A 105.38%, #FF5E3A 105.38%)',
        secondaryCommonGradient: 'linear-gradient(113.22deg, #FF9500 -15.57%, #FF5E3A 105.38%, #FF5E3A 105.38%)',
      },
    },
    // action: {},
    background: {
      light: { default: '#F7F6FB', paper: '#FFFFFF' },
      dark: { default: '#1E1A3D', paper: '#24204B' },
    },
    divider: { light: 'rgba(0, 0, 0, 0.12)', dark: 'rgba(60, 57, 110, 0.82)' },
    // error: {},
    // info: {},
    primary: { main: '#FF781D', light: '#FF9749', dark: '#E55A05' },
    secondary: { main: '#8B4FE7', light: '#AC74FF', dark: '#7934D4' },
    // success: {},
    text: {
      light: { primary: 'rgba(0, 0, 0, 0.87)', secondary: 'rgba(0, 0, 0, 0.54)', disabled: 'rgba(0, 0, 0, 0.38)' },
      dark: { primary: '#FFFFFF', secondary: 'rgba(255, 255, 255, 0.7)', disabled: 'rgba(255, 255, 255, 0.5)' },
    },
    // warning: {},
  },
  shape: 20,
  states,
  components: {
    inputBase: {
      shadow: {
        active: 12,
        default: 0,
        focus: 12,
        hover: 11,
      },
    },
  },
  size: {
    component: {
      small: {
        l: 0.5,
        t: 0.5,
        r: 0.5,
        b: 0.5,
      },
      medium: {
        l: 2,
        t: 1,
        r: 2,
        b: 1,
      },
      large: {
        l: 0.5,
        t: 1,
        r: 0.5,
        b: 1,
      },
    },
    navbar: {
      open: 32,
      close: 10,
    },
    padding: {
      x: 2,
      y: 2,
    },
  },
};
