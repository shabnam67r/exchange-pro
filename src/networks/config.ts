import { default as ax } from 'axios';
const axios = ax.create({
  baseURL: 'https://api.example.com',
});

axios.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';
export default axios;
