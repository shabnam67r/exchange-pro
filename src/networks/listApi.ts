import wait from '../utils/wait';
import { historyObj, messagesList, ticketsList } from '../constants/fakeData';
import { HistoryObj, MessageObj, TicketObj } from '../interfaces/totalData';
import { WAITING_WITH } from '../constants/env';

/******************************************************************************
 *                            MESSAGE API
 *****************************************************************************/
export const getMessagesList = async (): Promise<MessageObj[]> => {
  await wait(WAITING_WITH);
  return messagesList;
};

/******************************************************************************
 *                            TICKET API
 *****************************************************************************/
export const getTicketsList = async (): Promise<TicketObj[]> => {
  await wait(WAITING_WITH);
  return ticketsList;
};

export const getTicketItem = async (id: string): Promise<TicketObj | undefined> => {
  await wait(WAITING_WITH);
  let obj = ticketsList.find((val) => val._id === id);
  return obj;
};

/******************************************************************************
 *                            HISTORY API
 *****************************************************************************/
export const getHistoryList = async (): Promise<HistoryObj[]> => {
  await wait(WAITING_WITH);
  return [
    historyObj,
    historyObj,
    historyObj,
    historyObj,
    historyObj,
    historyObj,
    historyObj,
    historyObj,
    historyObj,
    historyObj,
    historyObj,
    historyObj,
    historyObj,
  ];
};
