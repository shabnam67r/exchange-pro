import { PurchaseOrderObj, TradeObj } from '../interfaces/totalData';
import wait from '../utils/wait';
import { WAITING_WITH } from '../constants/env';
import { buyersAndSellers, purchaseOrder } from '../constants/fakeData';

/******************************************************************************
 *                            TRADE API
 *****************************************************************************/
export const getBuyersList = async (): Promise<TradeObj[]> => {
  await wait(WAITING_WITH);
  return [
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
  ];
};

export const getSellersList = async (): Promise<TradeObj[]> => {
  await wait(WAITING_WITH);
  return [
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
    buyersAndSellers,
  ];
};

export const getPurchaseList = async (): Promise<PurchaseOrderObj[]> => {
  await wait(WAITING_WITH);
  return [
    purchaseOrder,
    purchaseOrder,
    purchaseOrder,
    purchaseOrder,
    purchaseOrder,
    purchaseOrder,
    purchaseOrder,
    purchaseOrder,
    purchaseOrder,
  ];
};
