import { Shadows } from '@mui/material/styles/shadows';
import {
  ActionInterface,
  BackgroundInterface,
  CommonInterface,
  dividerInterface,
  ErrorInterface,
  InfoInterface,
  PrimaryInterface,
  SecondaryInterface,
  SuccessInterface,
  TextInterface,
  WarningInterface,
} from './palletOptions';

interface ColorInterface {
  dark: string;
  light: string;
}

export interface GeneralPaletteInterface {
  common?: Partial<CommonInterface>;
  primary?: PrimaryInterface;
  secondary?: SecondaryInterface;
  error?: ErrorInterface;
  warning?: WarningInterface;
  info?: InfoInterface;
  success?: SuccessInterface;
  divider?: dividerInterface;
  text: Partial<TextInterface>;
  action?: Partial<ActionInterface>;
  background: Partial<BackgroundInterface>;
}
export interface StatesInterface {
  navbar: 'xs' | 'sm' | 'md' | 'lg' | 'xl';
  backDrop: {
    default: string;
    low: string;
    high: string;
  };
  transparency: {
    default: { hex: string; rgb: number };
    low: { hex: string; rgb: number };
    high: { hex: string; rgb: number };
  };
}
interface SizesInterface {
  component: {
    small: SizeInterface;
    medium: SizeInterface;
    large: SizeInterface;
  };
  padding: {
    x: number;
    y: number;
  };
  navbar: {
    open: number;
    close: number;
  };
}
interface ComponentShadowInterface {
  hover: number;
  default: number;
  active: number;
  focus: number;
}
interface ButtonInterface {
  background: ColorInterface;
  backgroundDisable: ColorInterface;
  color: ColorInterface;
  disable: ColorInterface;
  shadows: ComponentShadowInterface;
}
interface ButtonComponents {
  default: ButtonInterface;
  primary: ButtonInterface;
  secondary: ButtonInterface;
  text: ButtonInterface;
}
interface InputBaseComponents {
  shadow: ComponentShadowInterface;
}
interface Components {
  button?: ButtonComponents;
  inputBase: InputBaseComponents;
  textField?: any;
  checkBox?: any;
  switchBox?: any;
  radioBox?: any;
  menuItem?: any;
}
interface SizeInterface {
  l: number;
  t: number;
  r: number;
  b: number;
}

interface ShadowsInterface {
  light: Shadows;
  dark: Shadows;
}
export interface DefaultTheme {
  components: Components;
  shape: number;
  palette?: GeneralPaletteInterface;
  size: SizesInterface;
  states?: StatesInterface;
  shadows: ShadowsInterface;
}
