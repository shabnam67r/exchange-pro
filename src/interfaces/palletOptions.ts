import {
  CommonColors,
  SimplePaletteColorOptions,
  TypeAction,
  TypeBackground,
  TypeText,
} from '@mui/material/styles/createPalette';

export interface TextInterface {
  light: TypeText;
  dark: TypeText;
}
// tslint:disable-next-line: class-name
export interface dividerInterface {
  light: string;
  dark: string;
}
export interface ActionInterface {
  light: TypeAction;
  dark: TypeAction;
}
export interface BackgroundInterface {
  light: TypeBackground;
  dark: TypeBackground;
}

export interface CommonInterface {
  dark: CommonColors;
  light: CommonColors;
}

export type SuccessInterface = SimplePaletteColorOptions;
export type InfoInterface = SimplePaletteColorOptions;
export type WarningInterface = SimplePaletteColorOptions;
export type ErrorInterface = SimplePaletteColorOptions;
export type PrimaryInterface = SimplePaletteColorOptions;
export type SecondaryInterface = SimplePaletteColorOptions;

// *Important*
//  if you decide to add  commonColors you should add theme here too
declare module '@mui/material/styles/createPalette' {
  interface CommonColors {
    card: string;
    drawer?: string;
    backDrop: string;
    primaryCommonGradient: string;
    secondaryCommonGradient: string;
  }
}
