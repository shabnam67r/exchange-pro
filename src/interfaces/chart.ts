export interface ChartOptions {
  responsive?: boolean;
  plugins?: any;
  scales?: any;
}

export interface DataChart {
  labels?: string[];
  datasets: any;
}
