export interface InternalType {
  header: string;
  path: string;
}

export interface InternalTypeWithList {
  header: string;
  captionText?: any;
  titles: { title: string; index: string; path: string }[];
}

export interface MarketsPage {
  header: string;
  mostPrice: string;
  lessPrice: string;
  tables: any[];
}

export interface AuthObj {
  title: string;
  linkText: string;
  path: string;
}

export interface LoginPageObj {
  header: string;
  forgetPassword: AuthObj;
  newAccount: AuthObj;
}

export interface ForgetPasswordPageObj {
  forgetPasswordHeader: string;
  newPasswordHeader: any;
  alreadyHaveAccount: AuthObj;
}

export interface PersonalIdentificationObj {
  header: string;
  caution: string;
  tableHeader: string;
  table: string[];
  identiyCard: string;
  identiyCardInput: string;
  pictureText: string;
  sampleTitle: string;
}

export interface GlobalObj {
  header: string;
  caution: string;
  checkBox: AuthObj;
  forgetPassword: AuthObj;
  newAccount: AuthObj;
}

export interface RegisterPageObj {
  global: GlobalObj;
  personalIdentification: PersonalIdentificationObj;
}

export interface WebSiteConstant {
  termsPage: InternalType;
  aboutUsPage: InternalType;
  contactUsPage: InternalType;
  commonQuestionsPage: InternalTypeWithList;
  userManualPage: InternalTypeWithList;
  marketsPage: MarketsPage;
  loginPage: LoginPageObj;
  forgetPasswordPage: ForgetPasswordPageObj;
  registerPage: RegisterPageObj;
  personalIdentification: PersonalIdentificationObj;
}

export interface DefaultObj {
  title: string;
  table: string[];
}

export interface Link {
  name: string;
  path: string;
}

export interface TablesOfMarketPage {
  lastChanges: string[];
  sellers: string[];
  buyers: string[];
}

export interface Overview {
  title: string;
  desc: string;
  sellingPrice: string;
  buyingPrice: string;
}

export interface WalletPage {
  wallet: { title: string; coinUnit: string; moneyUnit: string };
  history: {
    title: string;
    sorts: string[];
    table: string[];
  };
}

export interface MyOrdersPage {
  title: string;
  sorts: string[];
  table: string[];
}

export interface OneTicket {
  title: string;
  box: { title: string; date: string };
  answer: { title: string };
}

export interface Ticket {
  allTicket: DefaultObj;
  oneTicket: OneTicket;
}

export interface InvitePage {
  invite: { title: string; desc: string };
  friends: { title: string; unit: string };
  profit: { title: string; unit: string };
}

export interface TwoStepVerification {
  title: string;
  desc: string;
  radioButtons: string[];
}

export interface TwoStepVerificationPage {
  title: string;
  desc: string;
  leftTime: string;
  timeUnit: string;
  edit: string;
}

export interface OrderObj {
  title: string;
  desc: string;
  bazar: string;
  inventory: string;
  lowestOffer: string;
}

export interface TradePage {
  sellOrder: OrderObj;
  buyOrder: OrderObj;
  sellers: DefaultObj;
  buyers: DefaultObj;
  myOrders: DefaultObj;
}

export interface ShopPage {
  title: string;
  wordWide: string;
  mySite: string;
  hightestPrice: string;
  lowestPrice: string;
  latest: string;
  transactionVolume: string;
}

export interface ResetPassword {
  title: string;
  desc1: string;
  desc2: string;
}

export interface SettingsPage {
  title: string;
  twoStepVerification: TwoStepVerification;
  resetPassword: ResetPassword;
}

export interface MyHistoryPage {
  coin: {
    title: string;
    table: string[];
  };
  money: DefaultObj;
}

export interface UserLevel {
  title: string;
  desc: string;
  card: {
    levels: string[];
    CurrentWage: string;
  };
}

export interface CardsFeatureOfHome {
  index: string;
  src: string;
  header: string;
  desc: string;
}

export interface CardsStatisticsOfHome {
  title: string;
  src: string;
}

export interface Intro {
  header1: string;
  header2: string;
  desc: string;
  imageAlt: string;
}

export interface CoinPriceOfHome {
  header: string;
}

export interface FeatureOfHome {
  header: string;
  desc: string;
  cards: CardsFeatureOfHome[];
}

export interface StatisticsOfHome {
  header: string;
  desc: string;
  cards: CardsStatisticsOfHome[];
}

export interface BazarBase {
  name: string;
  rial: string;
  toman: string;
}

export interface HomePage {
  intro: Intro;
  coinPrice: CoinPriceOfHome;
  feature: FeatureOfHome;
  statistics: StatisticsOfHome;
}

export interface WithdrawalRequestModal {
  title: string;
  descriptions: string[];
}

export interface TicketModal {
  title: string;
}

export interface Input {
  firstName: string;
  lastName: string;
  password: string;
  newPassword: string;
  passwordRepeat: string;
}

export interface HomePage {
  header: string;
  desc: string;
  Cards: { index: string; src: string; header: string; desc: string }[];
}

export interface Statistics {
  header: string;
  desc: string;
  Cards: { title: string; src: string }[];
}
export type Footer = {
  links: { name: string; subLinks: Link[] }[];
  contactus: { name: string; subLinks: [phoneNumber: string, activityHours: string] }[];
  webSiteRights: string;
};
export type Counter = {
  counts: { id: number; label: string; number: number; duration: number }[];
};

export interface Translates {
  footer: Footer;
}

export interface UserPanel {
  name: string;
  account: string;
  logOut: string;
}

export type Header = {
  links: Link[];
  bazarBase: BazarBase;
};

export interface Buttons {
  [key: string]: string;
}

export interface Inputs extends Buttons {}

export interface image {
  src: string;
  alt: string;
  index: number;
}
export interface MarketPage {
  header: string;
  mostPrice: string;
  lessPrice: string;
  tables: TablesOfMarketPage;
}

export interface Dashboard {
  homePage: HomePage;
  overview: Overview;
  userLevel: UserLevel;
  activeOrders: DefaultObj;
  allOrders: DefaultObj;
  walletPage: WalletPage;
  myOrdersPage: MyOrdersPage;
  myHistoryPage: MyHistoryPage;
  invitePage: InvitePage;
  mailPage: DefaultObj;
  ticket: Ticket;
  settingsPage: SettingsPage;
  twoStepVerificationPage: TwoStepVerificationPage;
  tradePage: TradePage;
  shopPage: ShopPage;
}

export type form = {
  name: string;
  subject: string;
  email: string;
  phoneNumber: string;
  description: string;
  submit: string;
};

export interface Modals {
  WithdrawalRequestModal: WithdrawalRequestModal;
  ticketModal: TicketModal;
}
