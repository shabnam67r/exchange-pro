// defined interface for snackBar notification in redux

export interface NotificationInterface {
  messages: { message: string; id: string; open: boolean }[];
}
export interface MessageNotificationInterface {
  message: string;
}

export interface IsMobileModeInterface {
  mobileMode: boolean;
}

export interface IsDrawerOpenInterface {
  openDrawer: boolean;
}
