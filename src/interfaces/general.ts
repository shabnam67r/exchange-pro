export interface SettingInterface {
  theme: 'DARK' | 'LIGHT';
}
