// message list
export interface MessageObj {
  _id: string;
  title: string;
  date: string;
  status: string;
  descriptions: string;
}

// message list
export interface MsgOfTicketObj {
  name: string;
  text: string;
  date: string;
}

// trade list
export interface TradeObj {
  amount: string;
  unitPrice: number;
  totalPrice: number;
}

// my purchase order
export interface PurchaseOrderObj {
  depositType: string;
  depositAmount: number;
  paymentCode: string;
  date: string;
  status: string;
}

// history obj
export interface HistoryObj {
  depositType: string;
  amount: number;
  loginDate: string;
  deposit: number;
  paymentCode: string;
  creatAt: string;
  status: string;
}

export interface TicketObj {
  _id: string;
  title: string;
  date: string;
  status: string;
  msg: MsgOfTicketObj[];
}

export interface CoinsInfoList {
  name: string;
  price: number;
  changes: number;
}

export interface RecentTransactionList {
  amount: number;
  unitPrice: number;
  creatAt: string;
}

export interface ActiveOrdersList {
  orderType: string;
  market: string;
  amount: number;
  unitPrice: number;
  totalPrice: number;
  full: number;
}

export interface AllOrdersList {
  orderType: string;
  market: string;
  amount: number;
  unitPrice: number;
  creatAt: string;
}
