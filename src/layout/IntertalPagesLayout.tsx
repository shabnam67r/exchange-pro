import { ReactElement } from 'react';
import { Box, useTheme } from '@mui/system';
import { makeStyles } from '@mui/styles';
import { Card, Theme, Typography, useMediaQuery } from '@mui/material';
import { Children } from '../interfaces/componentProps';
import { alpha } from '@mui/material/styles';

interface Props {
  title: string;
  children: Children;
  topComponent?: Children;
}

const InternalPagesLayout = (props: Props): ReactElement => {
  const theme = useTheme();
  const desktop = useMediaQuery(theme.breakpoints.up('md'));

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'column',
      marginBottom: desktop ? theme.spacing(4) : theme.spacing(15),
    },
    content: {
      position: 'relative',
      width: desktop ? '80%' : '90%',
      '& > div.MuiPaper-root': {
        border: `1px solid ${theme.palette.divider}`,
      },
      '& > div.layoutBoxFront': {
        width: '95%',
        height: '50px',
        position: 'absolute',
        bottom: '-20px',
        left: '50%',
        transform: 'translate(-50%, 20%)',
        background: alpha(theme.palette.background?.default, 0.95),
        borderRadius: theme.shape.borderRadius,
        border: `2px solid ${theme.palette.divider}`,
        zIndex: -1,
      },
      '& > div.layoutBoxBack': {
        content: "''",
        width: '90%',
        height: '70px',
        background: theme.palette.common?.primaryCommonGradient,
        position: 'absolute',
        bottom: '-15px',
        left: '50%',
        transform: 'translate(-50%, 50%)',
        borderRadius: theme.shape.borderRadius,
        zIndex: -2,
        boxShadow: `0 0 15px 3px ${theme.palette.primary.main}`,
      },
    },
  }));

  const classes = useStyles(props);

  return (
    <Box className={classes.root}>
      <Box sx={{ textAlign: 'center', mb: 8 }}>
        <Typography variant="h2">{props.title}</Typography>
      </Box>
      <Box className={classes.content}>
        {props?.topComponent && <Card sx={{ mb: 2 }}>{props?.topComponent}</Card>}
        <Card>{props.children}</Card>
        <Box className="layoutBoxFront" />
        <Box className="layoutBoxBack" />
      </Box>
    </Box>
  );
};

export default InternalPagesLayout;
