import React, { ReactElement } from 'react';
import AppBar from '../sections/AppBar';
import DefaultFooter from '../components/footer/DefaultFooter';
import { Children } from '../interfaces/componentProps';
import { Box } from '@mui/system';

interface Props {
  children: Children;
}

const HomeLayout = (props: Props): ReactElement => {
  return (
    <>
      <AppBar />
      <Box mt={15}>{props.children}</Box>
      <DefaultFooter />
    </>
  );
};

export default HomeLayout;
