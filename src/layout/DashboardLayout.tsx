import React, { ReactElement } from 'react';
import Sidebar from '../sections/Sidebar';
import { Box, BoxProps } from '@mui/system';
import { Grid, Toolbar } from '@mui/material';
import AppBar from '../sections/AppBar';
import { Children } from '../interfaces/componentProps';
import CoinsInfoCard from '../components/dashboard/CoinsInfoCard';
import RecentTransactionsCard from '../components/dashboard/RecentTransactionsCard';

interface Props {
  children: Children;
  componentProps?: BoxProps;
}

const DashboardLayout = (props: Props): ReactElement => {
  return (
    <Box
      sx={{
        display: 'flex',
        backgroundColor: (theme) => theme?.palette?.background?.default,
        height: '100%',
      }}
      {...props.componentProps}
    >
      <AppBar />
      <Sidebar />
      <Box sx={{ px: 4, pt: 5, width: '100%' }}>
        <Toolbar />
        <Grid container columnSpacing={2} rowSpacing={{ xs: 2, md: 0 }}>
          {/* @ts-ignore */}
          <Grid item xs={12} md={9.7} sx={{ display: 'flex' }}>
            {props.children}
          </Grid>
          {/* @ts-ignore */}
          <Grid item xs={12} md={2.3}>
            <Grid container rowSpacing={2} direction="column">
              <Grid item>
                <CoinsInfoCard />
              </Grid>
              <Grid item>
                <RecentTransactionsCard />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default DashboardLayout;
