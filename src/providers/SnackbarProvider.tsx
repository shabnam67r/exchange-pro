// provider for snack bar notification
import { Theme, Alert, Box, Snackbar, Stack } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { width } from '@mui/system';
import * as React from 'react';
import { RootState, useDispatch, useSelector } from 'src/redux';
import { onClose } from 'src/redux/Notification';

/******************************************************************************
 *                        Props Interface definition
 *****************************************************************************/
interface Props {
  children: any;
}

/******************************************************************************
 *                            Style definition
 *****************************************************************************/
const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
  root: {},
}));

/******************************************************************************
 *                           Component definition
 *****************************************************************************/

export default function SnackbarProvider(props: Props) {
  const notification = useSelector((state: RootState) => state.notification);
  const dispatch = useDispatch();

  return (
    <Box>
      <Box>
        {notification.messages.map((notification, i) => (
          <Snackbar
            key={notification.id}
            sx={{
              bottom: i * 70 + 'px !important',
              transition: (theme) => theme.transitions.create('all'),
              mb: 3,
              width: 300,
            }}
            open={notification.open}
            autoHideDuration={3000}
            onClose={(event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
              if (reason === 'clickaway') {
                return;
              }
              dispatch(onClose(notification.id));
            }}
          >
            <Alert
              sx={{ width: 1, borderRadius: (theme) => theme.shape.borderRadius + 'px' }}
              elevation={6}
              variant="filled"
            >
              {notification.message}
            </Alert>
          </Snackbar>
        ))}
      </Box>
      {props.children}
    </Box>
  );
}
// for showing notifications dispatch new message to redux notification
