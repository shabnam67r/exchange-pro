import { Divider, Grid, Paper, Theme, useTheme, Typography } from '@mui/material';
import { alpha, Box } from '@mui/system';
import useTranslate from '../../hooks/useTranslate';
import { ReactElement } from 'react';
import FooterTopCard from 'src/components/FooterTopCard';
import UseMethods from '../Usemethods';
import SymbolesTrust from '../SymbolesTrust';
import SocialMedia from '../SocialMedia';
import { Footer } from 'src/interfaces/translate';

/******************************************************************************
 *                        Props Interface definition
 *****************************************************************************/
interface Props {}

/******************************************************************************
 *                           Component definition
 *****************************************************************************/
export default function DefaultFooter({}: Props): ReactElement {
  const transition = useTranslate();
  const footer: Footer = transition('webSiteConstant.footer');
  const webSiteRights: string = transition('webSiteConstant.footer.webSiteRights');

  const theme = useTheme();

  return (
    <Box
      sx={{
        width: '100%',
        position: 'absolute',
        marginTop: '10%',
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      <Paper
        sx={{
          width: '80%',
          height: '100%',
          marginTop: '-1%',
          zIndex: 0,
          position: 'absolute',
          background: 'linear-gradient(22deg, #C644FC 13.5%, #5859D6 111.3%)',
        }}
      />

      <Paper
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          position: 'relative',
          zIndex: 1,
          background: alpha(theme.palette?.background.default || '', 0.95),
        }}
      >
        <Grid
          sx={{
            position: 'absolute',
            top: { xs: '-4%', md: '-10%' },
            left: { xs: '35%', sm: '40%' },
          }}
        >
          <FooterTopCard />
        </Grid>

        <Grid sx={{ display: 'flex', flexDirection: { xs: 'column', md: 'row' } }}>
          <Grid
            sx={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'baseline',
              margin: {
                xs: '20% 0',
                sm: '10% 0',
              },
            }}
          >
            <UseMethods data={footer} />
          </Grid>
          <Grid
            sx={{
              display: 'flex',
              flexDirection: { xs: 'column', sm: 'row' },
              justifyContent: 'center',
              alignItems: 'center',
              margin: { xs: '20% 0', sm: '10% 0' },
            }}
          >
            <SocialMedia />
            <SymbolesTrust />
          </Grid>
        </Grid>
        <Divider orientation="horizontal" sx={{ width: 0.6, borderColor: (theme) => theme.palette.primary.main }} />
        {/* TODO:how to change the name prop? */}
        <Typography
          variant="body2"
          sx={{
            margin: '2% auto',
          }}
        >
          {webSiteRights}
        </Typography>
      </Paper>
    </Box>
  );
}
