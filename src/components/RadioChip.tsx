import { ReactElement, useState } from 'react';
import {
  Chip,
  FormControl,
  FormControlLabel,
  MenuItem,
  RadioGroup,
  Select,
  SelectChangeEvent,
  Stack,
  useMediaQuery,
} from '@mui/material';
import { useTheme } from '@mui/system';

interface Props {
  filterList: string[];
}

const RadioChip = (props: Props): ReactElement => {
  const theme = useTheme();
  const media = useMediaQuery(theme.breakpoints.up('md'));
  const [activeChip, setActiveChip] = useState(props.filterList?.[0]);

  const el = (label: string): ReactElement => (
    <Chip
      label={label}
      component="span"
      color={label === activeChip ? 'primary' : 'default'}
      variant={label === activeChip ? 'filled' : 'outlined'}
      clickable
    />
  );

  const handleChange = (event: SelectChangeEvent) => {
    setActiveChip(event.target.value as string);
  };

  return media ? (
    <FormControl component="fieldset">
      <RadioGroup aria-label="filter" defaultValue={activeChip} name="radio-buttons-group">
        <Stack direction="row" flexWrap="wrap" letterSpacing={1} pb={2}>
          {props.filterList?.map((val, index) => (
            <FormControlLabel
              key={index}
              value={val}
              control={el(val)}
              label=""
              onClick={(e: any) => {
                setActiveChip(e?.target?.textContent);
              }}
            />
          ))}
        </Stack>
      </RadioGroup>
    </FormControl>
  ) : (
    <FormControl>
      <Select
        value={activeChip}
        onChange={handleChange}
        sx={{ fontSize: 10, backgroundColor: (theme) => theme?.palette?.primary?.main }}
      >
        {props.filterList?.map((val, index) => (
          <MenuItem value={val} key={index}>
            {val}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default RadioChip;
