import { Typography, TextField, StandardTextFieldProps } from '@mui/material';
import React, { ReactElement } from 'react';
import { Box, BoxProps } from '@mui/system';

type TextFieldProps = Omit<StandardTextFieldProps, 'variant'>;

interface Props extends TextFieldProps {
  variant?: 'outlined' | 'filled' | 'standard';
  componentProps?: BoxProps;
}
export default function CustomTextField(props: Props): ReactElement {
  const { label, ...newProps } = props;

  return (
    <Box {...props.componentProps} sx={{ width: '100%', margin: '0 3px' }}>
      <Typography variant="caption" sx={{ margin: '0 5%', fontWeight: (theme) => theme.typography.fontWeightMedium }}>
        {label}
      </Typography>
      {/* TODO:border color just be fixed with 0.6 opacity */}
      <TextField
        {...newProps}
        variant="outlined"
        sx={{
          borderRadius: (theme) => theme.shape.borderRadius + 'px',

          border: `1px solid rgba(49, 47, 98, 1)`,
          height: '64px',
        }}
      />
    </Box>
  );
}
