import React, { ReactElement } from 'react';
import AppbarDrawer from '../layout/AppbarDrawer';
import { useRouter } from 'next/router';
import useTranslate from '../../hooks/useTranslate';
import { Buttons, Header } from '../../interfaces/translate';
import { Box, Theme } from '@mui/system';
import Links from '../smallComponents/header/Links';
import { Button, Grid } from '@mui/material';
import SettingsHeader from '../smallComponents/header/SettingsHeader';
import AvatarTextButton from '../smallComponents/avatarButton/AvatarTextButton';
import SwitchThemeCard from '../cards/SwitchThemeCard';

interface Props {
  open: boolean;
  setOpen: any;
}

const DashboardDrawer = (props: Props): ReactElement => {
  const router = useRouter();
  const transition = useTranslate();
  const buttonsText: Buttons = transition('webSiteConstant.buttons');
  const headerText: Header = transition('webSiteConstant.header');

  const handleLoginLink = () => router.push('/auth/login');

  return (
    <AppbarDrawer open={props.open} setOpen={props.setOpen} mainColor>
      <Box sx={{ display: 'flex', flexDirection: 'column' }}>
        <Box
          component="span"
          sx={{
            background: (theme: Theme) => theme.palette.common?.primaryCommonGradient,
            width: 40,
            height: 40,
            mx: 'auto',
            borderRadius: '8px',
          }}
        />
        <Box sx={{ display: 'flex', alignItems: 'center', flexDirection: { xs: 'column', md: 'row' } }}>
          <Links data={headerText} />
        </Box>
        <Grid container rowSpacing={2}>
          <Grid item xs={12} sx={{ textAlign: 'center', '& > div': { width: 'auto' } }}>
            <SettingsHeader />
          </Grid>
          <Grid item xs={12} sx={{ textAlign: 'center', '& > label': { mx: '0 !important' } }}>
            <SwitchThemeCard />
          </Grid>
          <Grid item xs={12} sx={{ textAlign: 'center', '& button': { ml: 0 } }}>
            <AvatarTextButton />
          </Grid>
          <Grid item xs={12} sx={{ textAlign: 'center' }}>
            <Button variant="outlined" color="primary" onClick={handleLoginLink}>
              {buttonsText.logout}
            </Button>
          </Grid>
        </Grid>
      </Box>
    </AppbarDrawer>
  );
};

export default DashboardDrawer;
