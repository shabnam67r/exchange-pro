import React, { ReactElement, useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { makeStyles } from '@mui/styles';
import { CardMedia, Grid, Stack, Theme, Typography } from '@mui/material';
import { Box } from '@mui/system';
import useTranslate from '../hooks/useTranslate';

interface Props {}

const FileUploader = (props: Props): ReactElement => {
  const [files, setFiles] = useState<any>([]);
  const transition = useTranslate();
  const fileText: string = transition('webSiteConstant.registerPage.personalIdentification.identiyCardInput');

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    thumbsContainer: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginTop: 16,
    },
    thumb: {
      display: 'inline-flex',
      borderRadius: 2,
      border: '1px solid #eaeaea',
      marginBottom: 8,
      marginRight: 8,
      width: 100,
      height: 100,
      padding: 4,
      boxSizing: 'border-box',
    },
    thumbInner: {
      display: 'flex',
      minWidth: 0,
      overflow: 'hidden',
    },
    img: {
      display: 'block',
      width: 'auto',
      height: '100%',
    },
  }));

  const classes = useStyles(props);

  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/*',
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
    },
  });

  const thumbs = files.map((file: any) => (
    <Box className={classes.thumb} key={file?.name}>
      <Box className={classes.thumbInner}>
        <CardMedia component="img" src={file?.preview} className={classes.img} alt="Dropzone Image" />
      </Box>
    </Box>
  ));

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file: any) => URL.revokeObjectURL(file?.preview));
    },
    [files],
  );
  return (
    <Grid
      {...getRootProps()}
      sx={{
        display: 'flex',
        flexDirection: { xs: 'column', md: 'row' },
        alignItems: 'center',
        justifyContent: 'space-around',
      }}
    >
      <input {...getInputProps()} />
      <Stack direction="row">
        <Typography variant="body1">{fileText.slice(0, 11)}</Typography>
        <Typography variant="body1" color="primary" sx={{ mx: 1 }}>
          {fileText.slice(11, 23)}
        </Typography>
        <Typography variant="body1">{fileText.slice(23)}</Typography>
      </Stack>
      {files?.length ? (
        <Box className={classes.thumbsContainer}>{thumbs}</Box>
      ) : (
        <CardMedia
          component="img"
          sx={{ width: '20% !important' }}
          src="/static/images/add image.png"
          alt="Dropzone Image"
        />
      )}
    </Grid>
  );
};

export default FileUploader;
