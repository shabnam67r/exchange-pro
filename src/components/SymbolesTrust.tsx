import React, { ReactElement } from 'react';

import {
  Divider,
  Box,
  CardMedia,
  Card,
  Grid,
  Theme,
  Typography } from '@mui/material';
  import { makeStyles } from '@mui/styles';


type Props = {
  
};
const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
  card: {
    borderRadius:10,
    height:90,
    width:60,
    marginTop:15,backgroundColor:'#fff',
    display:'grid',
    alignItems:'center',
    justifyContent:'center'

  },
}));
export default function SymbolesTrust(props: Props, ref: any): ReactElement{
  const { ...other } = props;
 const classes = useStyles(props);
  
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: { xs: 'column', md: 'row' },
        alignContent:'center'
        
      }}
    >
      <Grid >
        
          <Grid item sx={{padding:{sm:'0 0 0 24px',xs:'24px 0 0 24px'}}}>
            <Box  sx={{ width: 'fit-content' ,margin:'auto'}}>
              <Box sx={{ width: 'fit-content' }}>
                <Typography variant="subtitle2" sx={{ fontWeight: (theme) => theme.typography.fontWeightMedium }}>
                  نماد های اعتماد
                </Typography>
                {/* <Divider
                  orientation="horizontal"
                  sx={{ width: 1, borderColor: (theme) => theme.palette.primary.main, borderWidth: '1px' }}
                /> */}
              </Box>
              <Box sx={{ display: 'flex', flexDirection: 'column'}}>
              <Grid sx={{display:'flex',flexDirection:'row'}}>
               {/* TODO */}
        {/* {لینک برای عکس ها رو ندارم} */}
            <Card className={classes.card} sx={{margin:'0 2px'}}>
 
             <CardMedia
               sx={{width:50}}
             component="img"
             image="/static/images/14765643 (1).svg"
             alt="Paella dish"
              />
           </Card>
 
            <Card 
             className={classes.card}
              sx={{margin:'0 5px'}}>
            <CardMedia
              sx={{width:50}}
              component="img"
              image="/static/images/neshanmelli.svg"
              alt="Paella dish"
              />
            </Card>
            </Grid>
              </Box>
              
            </Box>
          </Grid>
       
      </Grid>
    </Box>
  
  );
}


