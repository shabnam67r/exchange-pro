import React, { ReactElement } from 'react';
import { Drawer, List, ListItem, ListItemIcon, ListItemText, Toolbar, Theme } from '@mui/material';
import { Box } from '@mui/system';
import { makeStyles } from '@mui/styles';
import { useRouter } from 'next/router';
import SidebarLink from '../smallComponents/sidebar/SidebarLink';

interface linkList {
  link: string;
  icon: ReactElement;
  text: string;
}

interface Props {
  data: linkList[];
}

const Sidebar = (props: Props): ReactElement => {
  const router = useRouter();
  const pathName = router.pathname;
  const sameDashboardRoute = [
    '/dashboard/tickets',
    '/dashboard/tickets/[id]',
    '/dashboard/settings',
    '/dashboard/trade',
    '/dashboard/messages',
    '/dashboard',
  ];
  const pageRoute = sameDashboardRoute.includes(pathName) ? '/dashboard' : pathName;

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme>((theme: Theme) => ({
    list: {
      paddingLeft: 20,
    },
    icon: {
      minWidth: 0,
      '& > svg': {
        color: theme?.palette?.text?.secondary,
      },
    },
    link: {
      padding: 0,
      borderRadius: '20px 0 0 20px',
      '&:hover': {
        backgroundColor: theme.palette?.background?.default,
      },
      '& a': {
        display: 'flex',
        padding: '10px 20px',
        textDecoration: 'none',
        color: theme?.palette?.text?.secondary,
        alignItems: 'center',
        width: '100%',
        '& span': {
          textAlign: 'right',
          paddingRight: theme?.spacing(2),
        },
      },
      '&.Mui-selected': {
        backgroundColor: theme.palette?.background?.default,
        '&:hover': {
          backgroundColor: theme.palette?.background?.default,
        },
        '& svg': {
          color: theme?.palette?.primary?.main,
        },
        '& a:before': {
          content: "''",
          display: 'block',
          transform: 'translateX(200%)',
          width: 12,
          height: 12,
          borderRadius: '50%',
          background: theme?.palette?.primary?.main,
          boxShadow: theme.shadows[14],
        },
      },
    },
  }));

  const classes = useStyles();

  return (
    <Drawer
      variant="permanent"
      anchor="right"
      sx={{
        width: 240,
        flexShrink: 0,
        [`& .MuiDrawer-paper`]: { width: 240, boxSizing: 'border-box', px: 0 },
      }}
    >
      <Toolbar />
      <Box sx={{ overflow: 'auto' }}>
        <List className={classes.list}>
          {props?.data?.map((item: linkList, index) => (
            <ListItem button key={index} className={classes.link} selected={item.link === pageRoute}>
              <SidebarLink link={item.link}>
                <ListItemIcon className={classes.icon}>{item.icon}</ListItemIcon>
                <ListItemText primary={item.text} />
              </SidebarLink>
            </ListItem>
          ))}
        </List>
      </Box>
    </Drawer>
  );
};

export default Sidebar;
