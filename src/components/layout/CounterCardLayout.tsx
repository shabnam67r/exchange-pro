import { ReactElement } from 'react';
import { Card, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Children } from '../../interfaces/componentProps';
import { Box } from '@mui/system';
import ResetIcon from '../../icons/someIcons/ResetIcon';

interface Props {
  children: Children;
  icon?: ReactElement;
}

const CounterCardLayout = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      position: 'relative',
      overflow: 'visible',
      '& > div:first-child': {
        position: 'absolute',
        top: 0,
        left: '50%',
        width: 70,
        height: 70,
        background: theme.palette.background.paper,
        boxShadow: theme.shadows[8],
        border: `2px solid ${theme.palette.divider}`,
        borderRadius: theme.shape.borderRadius,
        transform: 'translate(-50%, -50%)',
        '& > div:first-child': {
          content: "''",
          display: 'block',
          borderRadius: Number(theme.shape.borderRadius) / 2,
          width: '70%',
          height: '70%',
          position: 'absolute',
          top: '50%',
          left: '49%',
          transform: 'translate(-50%, -50%)',
          '& > svg': {
            width: '100%',
            height: '100%',
          },
        },
      },
    },
  }));

  const classes = useStyles(props);

  return (
    <Card className={classes.root}>
      <Box>
        <Box>{props?.icon || <ResetIcon />}</Box>
      </Box>
      <Box pt={5} pb={3} px={4}>
        {props.children}
      </Box>
    </Card>
  );
};

export default CounterCardLayout;
