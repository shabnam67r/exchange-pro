import React, { ReactElement } from 'react';
import { Drawer } from '@mui/material';
import { useTheme } from '@mui/system';

interface Props {
  children: any;
  open: boolean;
  setOpen: any;
  mainColor?: boolean;
}

export default function AppbarDrawer(props: Props): ReactElement {
  const theme = useTheme();

  return (
    <Drawer
      open={props.open}
      anchor="left"
      PaperProps={{
        style: {
          width: '75%',
          background: `${props?.mainColor ? theme?.palette?.common?.drawer : 'default'}`,
        },
      }}
      onClose={() => props.setOpen(false)}
    >
      {props.children}
    </Drawer>
  );
}
