import React, { ReactElement } from 'react';
import { Children } from '../../interfaces/componentProps';
import { Grid } from '@mui/material';
import FormCardLayout from './FormCardLayout';
import HomeLayout from '../../layout/HomeLayout';
import { GridProps } from '@mui/system';

interface Props {
  children: Children;
  gridProps?: GridProps;
  size?: 'small' | 'normal';
}

const AuthLayout = (props: Props): ReactElement => {
  return (
    <HomeLayout>
      <Grid container justifyContent="center" mt={20} mb={{ xs: 8, md: 0 }}>
        {/*// @ts-ignore*/}
        <Grid item xs={12} md={props?.size === 'small' ? 3.5 : 5} {...props?.gridProps}>
          <FormCardLayout>{props.children}</FormCardLayout>
        </Grid>
      </Grid>
    </HomeLayout>
  );
};

export default AuthLayout;
