import { ReactElement } from 'react';
import { Card, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Children } from '../../interfaces/componentProps';
import { Box } from '@mui/system';

interface Props {
  children: Children;
}

const FormCardLayout = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      position: 'relative',
      overflow: 'visible',
      '&> div:first-child': {
        position: 'absolute',
        top: 0,
        left: '50%',
        width: 70,
        height: 70,
        background: theme.palette.background.default,
        border: `2px solid ${theme.palette.divider}`,
        borderRadius: theme.shape.borderRadius,
        transform: 'translate(-50%, -50%)',
        '&:before': {
          content: "''",
          display: 'block',
          borderRadius: Number(theme.shape.borderRadius) / 2,
          width: '60%',
          height: '60%',
          background: theme.palette.common?.primaryCommonGradient,
          position: 'absolute',
          top: '50%',
          left: '49%',
          transform: 'translate(-50%, -50%)',
        },
      },
    },
  }));

  const classes = useStyles(props);

  return (
    <Card className={classes.root}>
      <Box />
      <Box pt={5} pb={3} px={{ xs: 2, md: 4 }}>
        {props.children}
      </Box>
    </Card>
  );
};

export default FormCardLayout;
