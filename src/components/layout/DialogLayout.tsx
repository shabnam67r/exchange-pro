import {
  Dialog,
  DialogProps,
  DialogTitle,
  DialogTitleProps,
  IconButton,
} from '@mui/material';
import { ReactElement } from 'react';
import CloseIcon from '@mui/icons-material/Close';
import { Children } from '../../interfaces/componentProps';

export interface DialogTitlePropsDefault {
  id: string;
  children?: React.ReactNode;
  onClose: () => void;
}

interface Props {
  open: boolean;
  title: string | ReactElement;
  onClose: () => void;
  dialogProps?: DialogProps;
  dialogTitleProps?: DialogTitleProps;
  children: Children
}

const BootstrapDialogTitle = (props: DialogTitlePropsDefault) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, mb: 4, p: 2, textAlign: 'center' }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            borderRadius: '10px !important',
            color: (theme) => theme.palette.text.primary,
            background: (theme) => theme.palette.background.default,
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

const DialogLayout = (props: Props): ReactElement => {
  const { open, onClose, children } = props;

  return (
    <Dialog onClose={onClose} open={open} fullWidth maxWidth="md" {...props.dialogProps}>
      <BootstrapDialogTitle id="default-dialog-title" onClose={onClose} {...props.dialogTitleProps}>
        {props.title}
      </BootstrapDialogTitle>
      {children}
    </Dialog>
  );
};

export default DialogLayout;
