import { Theme, Box ,Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Typography } from '@mui/material';
import { SxProps } from '@mui/system';
import LogoFooter from 'src/icons/LogoFooter';
import { useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
/******************************************************************************
 *                        Props Interface definition
 *****************************************************************************/
interface Props {
  idKey?: string;
  full?: boolean; // if true --> Logo with Text
  width?: number;
  height?: number;
  type?: 'primary' | 'secondary';
  ref?: React.Ref<unknown>;
  sx?: SxProps<Theme>;
}

/******************************************************************************
 *                            Style definition
 *****************************************************************************/
const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
  root: {
      padding:'10px',
      // position:'absolute',
      borderRadius:theme.shape.borderRadius+'px',
  },
}));

/******************************************************************************
 *                           Component definition
 *****************************************************************************/
const FooterTopCard: React.FC<Props> = (props) => {
  const classes = useStyles(props);
  return (
    <Box  ref={props.ref} 
    sx={{background:' linear-gradient(180deg, rgba(36, 32, 75, 0.99) 0%, rgba(32, 31, 50, 0.99) 100%)'
    , maxHeight: props.height
    , ...props.sx }} 
    className={classes.root} key={props.idKey}>
      <Grid  sx={{display:'flex',alignItems:'center',flexDirection:{xs:'column',md:'row'}}}>
      
       <LogoFooter />
       <Grid sx={{alignItems:{xs:'center',md:'flex-start'}}}>
       <Typography 
       sx={{display:{md:'flex',xs:'none'}}} color="white" variant="h2">زرینکس</Typography>
       <Typography
       sx={{display:{md:'flex',xs:'none'}}} color="white" variant="body1">بازار معاملات سکه طلا</Typography>
       </Grid>
      </Grid>
    </Box>
  );
};

/******************************************************************************
 *                             Default values of Props
 *****************************************************************************/

export default FooterTopCard;
