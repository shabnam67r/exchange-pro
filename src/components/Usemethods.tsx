import { Box, Grid, Link, Typography } from '@mui/material';
import React, { ReactElement } from 'react';
import { Footer, Link as TypeLink } from 'src/interfaces/translate';

import useTranslate from 'src/hooks/useTranslate';
interface Props {
  data: Footer;
}

export default function UseMethods(props: Props): ReactElement {
  const transition = useTranslate();
  const links: { name: string; subLinks: TypeLink[] }[] = transition('webSiteConstant.footer.links');

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: { xs: 'column', md: 'row' },
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Grid spacing={3} container sx={{ margin: 'auto', flexWrap: { xs: 'wrap', md: 'nowrap' } }}>
        {links.map((list, index) => (
          <Grid item xs={6} sm={3} lg={3} key={index}>
            <Box key={list.name} sx={{ width: 'fit-content', margin: 'auto' }}>
              <Box sx={{ width: 'fit-content' }}>
                <Typography variant="subtitle2" sx={{ fontWeight: (theme) => theme.typography.fontWeightMedium }}>
                  {list.name}
                </Typography>
              </Box>
              <Box sx={{ display: 'flex', flexDirection: 'column', mt: 3 }}>
                {list.subLinks.map((link, ind) => (
                  <Link
                    key={ind}
                    href={link.path}
                    sx={{
                      textDecoration: 'none',
                      color: (theme) => theme.palette.text.secondary,
                      py: 0.5,
                      ':hover': { color: (theme) => theme.palette.primary.main },
                    }}
                  >
                    <Typography variant="body2">{link.name}</Typography>
                  </Link>
                ))}
              </Box>
            </Box>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
}
