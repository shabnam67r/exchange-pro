import React, { ReactElement } from 'react';
import { Pagination, PaginationProps } from '@mui/lab';
import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

interface Props {
  count: number;
  selected?: number;
  paginationProps?: PaginationProps;
}

const DefaultPagination = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      '& button.Mui-selected': {
        color: theme.palette.text.primary,
        background: theme.palette.common.primaryCommonGradient,
        borderRadius: 10,
      },
    },
  }));

  const classes = useStyles(props);

  return (
    <Pagination
      count={props.count}
      color="primary"
      shape="rounded"
      defaultPage={props?.selected}
      className={classes.root}
      {...props.paginationProps}
    />
  );
};

export default DefaultPagination;
