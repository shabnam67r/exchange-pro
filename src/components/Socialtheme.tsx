import { forwardRef } from 'react';

import {
  Box,
  CardMedia,
  Card,
  Grid,
  Theme,
  Typography } from '@mui/material';
  import { makeStyles } from '@mui/styles';
  import LinkedInIcon from '@mui/icons-material/LinkedIn';
  import InstagramIcon from '@mui/icons-material/Instagram';
  import TwitterIcon from '@mui/icons-material/Twitter';
  import WhatsAppIcon from '@mui/icons-material/WhatsApp';
  import TelegramIcon from '@mui/icons-material/Telegram';


type Props = {
  children:any
};
const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
  icon: {
      display:'flex',
      position:'absolute'
 
  },
}));

const SocialTheme = forwardRef((props: Props, ref: any) => {
  const { children,...other } = props;
 const classes = useStyles(props);
  
  return (
  <Box sx={{display:'flex',position:'relative',alignItems:'center',justifyContent:'center'}} ref={ref} {...other}>
   
      <CardMedia
      sx={{width:40,padding:'0 5%'}}
       component="img"
      image="/static/images/backSocial.svg"
       alt='background'/>
       {/* <TelegramIcon className={classes.icon} /> */}
      {children}
   
    
  </Box>
  );
});

export default SocialTheme;
