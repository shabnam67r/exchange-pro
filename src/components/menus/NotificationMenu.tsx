import React, { ReactElement } from 'react';
import { Button, Chip, ListItemIcon, Menu, MenuItem, Typography } from '@mui/material';
import { MailOutline } from '@mui/icons-material';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import useTranslate from '../../hooks/useTranslate';
import { Buttons } from '../../interfaces/translate';
import { Box } from '@mui/system';
import _ from 'lodash';
import { fonts } from '../../styles/styles';
import Link from 'next/link';

interface Props {
  open: boolean;
  handleClose: () => void;
  handleEl: null | HTMLElement;
  handleCloseEl: (anchorEl: null | HTMLElement) => void;
}

const NotificationMenu = (props: Props): ReactElement => {
  const translate = useTranslate();
  const textField: Buttons = translate('webSiteConstant.buttons');
  const initialList = _.range(0, 4);

  return (
    <Menu
      anchorEl={props.handleEl}
      open={props.open}
      onClose={() => {
        props.handleClose();
        props.handleCloseEl(null);
      }}
    >
      {initialList.map((val, index) => (
        <MenuItem key={index}>
          <ListItemIcon>
            <MailOutline sx={{ color: (theme) => theme.palette.grey.A400 }} />
          </ListItemIcon>
          <Box>
            <Typography variant="body2" component="div" color="grey.A100">
              خوش آمدگویی
            </Typography>
            <Typography variant="caption" component="div">
              1400/۰۵/۲۵-۱۱:۰۰
            </Typography>
          </Box>
          <Chip
            icon={<RemoveRedEyeIcon />}
            label={textField?.see}
            clickable
            sx={{
              mr: 1,
              minWidth: 100,
              fontFamily: fonts.text,
              backgroundColor: (theme) => theme.palette.background.paper,
            }}
          />
        </MenuItem>
      ))}
      <MenuItem
        sx={{
          display: 'block',
          textAlign: 'center',
          mt: 1,
          '& a': { color: (theme) => theme.palette.text.primary, textDecoration: 'none' },
        }}
      >
        <Button variant="contained" color="primary" onClick={() => props.handleClose()}>
          <Link href="/dashboard/messages">
            <a>{textField?.seeAll}</a>
          </Link>
        </Button>
      </MenuItem>
    </Menu>
  );
};

export default NotificationMenu;
