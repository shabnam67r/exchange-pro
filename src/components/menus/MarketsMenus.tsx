import React, { ReactElement } from 'react';
import { CardMedia, ListItemIcon, Menu, MenuItem, Typography } from '@mui/material';
import { Box } from '@mui/system';
import useTranslate from '../../hooks/useTranslate';
import { BazarBase } from '../../interfaces/translate';

interface Props {
  open: boolean;
  handleClose: () => void;
  handleEl: null | HTMLElement;
  handleCloseEl: (anchorEl: null | HTMLElement) => void;
}

const MarketsMenu = (props: Props): ReactElement => {
  const transition = useTranslate();
  const coinsList: BazarBase = transition('webSiteConstant.coins');

  return (
    <Menu
      anchorEl={props.handleEl}
      open={props.open}
      onClose={() => {
        props.handleClose();
        props.handleCloseEl(null);
      }}
    >
      {Object.values(coinsList)?.map((val, index) => (
        <MenuItem key={index}>
          <ListItemIcon>
            <CardMedia component="img" sx={{ width: 30, height: 30 }} src="/static/images/Bitcoin-2.png" alt="coin" />
          </ListItemIcon>
          <Box>
            <Typography variant="body2" component="div" sx={{ color: (theme) => theme?.palette.text?.secondary }}>
              {val}
            </Typography>
          </Box>
        </MenuItem>
      ))}
    </Menu>
  );
};

export default MarketsMenu;
