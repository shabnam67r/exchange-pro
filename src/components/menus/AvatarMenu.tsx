import React, { ReactElement } from 'react';
import { ListItemIcon, Menu, MenuItem, Typography } from '@mui/material';
import { Logout, PersonOutline } from '@mui/icons-material';
import useTranslate from '../../hooks/useTranslate';
import { UserPanel } from '../../interfaces/translate';
import { Box, Theme } from '@mui/system';
import { makeStyles } from '@mui/styles';
import NextLink from '../smallComponents/NextLink';

interface Props {
  open: boolean;
  handleClose: () => void;
  handleEl: null | HTMLElement;
  handleCloseEl: (anchorEl: null | HTMLElement) => void;
}

const AvatarMenu = (props: Props): ReactElement => {
  // const open = Boolean(props.open);
  const translate = useTranslate();
  const textField: UserPanel = translate('webSiteConstant.header.userPanel');

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    item: {
      '& a': {
        display: 'flex',
        alignItems: 'center',
      },
    },
  }));

  const classes = useStyles(props);

  const handleClose = () => {
    props.handleClose();
    props.handleCloseEl(null);
  };

  return (
    <Menu
      anchorEl={props.handleEl}
      open={props.open}
      onClose={handleClose}
      // transformOrigin={{ horizontal: 'right', vertical: 'top' }}
      // anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
    >
      <MenuItem className={classes.item} onClick={handleClose}>
        <NextLink href="/auth/personalIdentification">
          <ListItemIcon>
            <PersonOutline />
          </ListItemIcon>
          <Box>
            <Box>
              <Typography variant="caption">{textField?.account}</Typography>
            </Box>
            <Box>
              <Typography variant="caption">example@example.com</Typography>
            </Box>
          </Box>
        </NextLink>
      </MenuItem>
      <MenuItem className={classes.item} onClick={handleClose}>
        <NextLink href="/">
          <ListItemIcon>
            <Logout sx={{ color: (theme) => theme.palette.text?.secondary }} />
          </ListItemIcon>
          <Typography variant="caption">{textField?.logOut}</Typography>
        </NextLink>
      </MenuItem>
    </Menu>
  );
};

export default AvatarMenu;
