import React, { ReactElement } from 'react';
import DialogLayout from '../layout/DialogLayout';
import { Button, DialogActions, DialogContent, Grid } from '@mui/material';
import CustomTextField from '../forms/TextField';
import useTranslate from '../../hooks/useTranslate';
import { Buttons, Inputs, WithdrawalRequestModal } from '../../interfaces/translate';

interface Props {
  open: boolean;
  onClose: () => void;
}

const SupportRequestDialog = (props: Props): ReactElement => {
  const transition = useTranslate();
  const ticketModalText: WithdrawalRequestModal = transition('webSiteConstant.modals.ticketModal');
  const buttonText: Buttons = transition('webSiteConstant.buttons');
  const inputsText: Inputs = transition('webSiteConstant.inputs');

  return (
    <DialogLayout title={ticketModalText.title} open={props.open} onClose={props.onClose}>
      <DialogContent>
        <Grid container mt={2} spacing={{ xs: 2, md: 0 }} justifyContent="space-between">
          <Grid item xs={12} md={6}>
            <CustomTextField placeholder="www.zarinex.ir" label={inputsText.subject} />
          </Grid>
          <Grid item xs={12} md={6}>
            <CustomTextField placeholder="www.zarinex.ir" label={inputsText.title} />
          </Grid>
          <Grid item xs={12}>
            <CustomTextField placeholder="www.zarinex.ir" label={inputsText.description} multiline />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions sx={{ justifyContent: 'center' }}>
        <Button sx={{ border: 'none', py: 1.7, px: 5 }}>{buttonText.sendTicket}</Button>
      </DialogActions>
    </DialogLayout>
  );
};

export default SupportRequestDialog;
