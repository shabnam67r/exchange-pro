import React, { ReactElement } from 'react';
import DialogLayout from '../layout/DialogLayout';
import { Button, DialogActions, DialogContent, DialogContentText, Stack } from '@mui/material';
import useTranslate from '../../hooks/useTranslate';
import { Buttons, Inputs, WithdrawalRequestModal } from '../../interfaces/translate';
import ParagraphOfList from '../smallComponents/ParagraphOfList';
import CustomTextField from '../forms/TextField';

interface Props {
  open: boolean;
  onClose: () => void;
}

const WithdrawDialog = (props: Props): ReactElement => {
  const transition = useTranslate();
  const walletPageText: WithdrawalRequestModal = transition('webSiteConstant.modals.WithdrawalRequestModal');
  const buttonText: Buttons = transition('webSiteConstant.buttons');
  const inputsText: Inputs = transition('webSiteConstant.inputs');

  return (
    <DialogLayout title={walletPageText.title} open={props.open} onClose={props.onClose}>
      <DialogContent>
        <DialogContentText>
          {walletPageText.descriptions.map((val, index) => (
            <React.Fragment key={index}>
              <ParagraphOfList text={val} />
            </React.Fragment>
          ))}
        </DialogContentText>
        <Stack
          mt={{ xs: 2, md: 4 }}
          direction={{ xs: 'column', md: 'row' }}
          spacing={{ xs: 2, md: 0 }}
          justifyContent={{ xs: 'start', md: 'space-around' }}
        >
          <CustomTextField placeholder="www.zarinex.ir" label={inputsText.withdrawalAmount} />
          <CustomTextField placeholder="www.zarinex.ir" label={inputsText.DestinationIBAN} />
        </Stack>
      </DialogContent>
      <DialogActions sx={{ justifyContent: 'center' }}>
        <Button sx={{ border: 'none', py: 1.7, px: 5 }}>{buttonText.submitRequest}</Button>
      </DialogActions>
    </DialogLayout>
  );
};

export default WithdrawDialog;
