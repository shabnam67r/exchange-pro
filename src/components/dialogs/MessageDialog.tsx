import React, { ReactElement } from 'react';
import DialogLayout from '../layout/DialogLayout';
import { DialogContent, DialogContentText, Typography } from '@mui/material';
import { MessageObj } from '../../interfaces/totalData';

interface Props {
  open: boolean;
  data: MessageObj;
  onClose: () => void;
}

const MessageDialog = (props: Props): ReactElement => {
  return (
    <DialogLayout title={props.data.title} open={props.open} onClose={props.onClose}>
      <DialogContent>
        <DialogContentText>
          <Typography variant="body2">{props.data.descriptions}</Typography>
        </DialogContentText>
      </DialogContent>
    </DialogLayout>
  );
};

export default MessageDialog;
