import React, { ReactElement } from 'react';
import CounterCardLayout from '../layout/CounterCardLayout';
import { Theme, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { makeStyles } from '@mui/styles';

interface Props {
  title: string;
  icon?: ReactElement;
}

const CounterInfo = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  }));

  const classes = useStyles(props);

  return (
    <CounterCardLayout icon={props?.icon}>
      <Box className={classes.root}>
        <Typography variant="body1" sx={{ mb: 2 }}>
          {props?.title}
        </Typography>
      </Box>
    </CounterCardLayout>
  );
};

export default CounterInfo;
