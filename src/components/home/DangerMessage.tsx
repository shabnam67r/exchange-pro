import React, { ReactElement } from 'react';
import { makeStyles } from '@mui/styles';
import { SvgIcon, Theme, Typography } from '@mui/material';
import { Box } from '@mui/system';
import WarningAmberOutlinedIcon from '@mui/icons-material/WarningAmberOutlined';
import { Children } from '../../interfaces/componentProps';

interface Props {
  text: Children | string;
}

const DangerMessage = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme>((theme: Theme) => ({
    root: {
      padding: theme.spacing(1),
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: theme.shape.borderRadius,
      background: theme.palette.error.main,
    },
    box: {
      textAlign: 'center',
    },
  }));

  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <SvgIcon sx={{ mx: 1, color: (theme) => theme.palette.text.primary }}>
        <WarningAmberOutlinedIcon />
      </SvgIcon>
      <Box className={classes.root}>
        <Typography variant="caption">{props.text}</Typography>
      </Box>
    </Box>
  );
};

export default DangerMessage;
