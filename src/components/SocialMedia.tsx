import { forwardRef } from 'react';

import { Box, CardMedia, Grid, Theme, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import TelegramIcon from '@mui/icons-material/Telegram';
import SocialTheme from './Socialtheme';

import React, { ReactElement } from 'react';

import useTranslate from 'src/hooks/useTranslate';
interface Props {}
const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
  icon: {
    position: 'absolute',
  },
}));

export default function SocialMedia(props: Props, ref: any): ReactElement {
  const classes = useStyles(props);
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: { xs: 'column', md: 'row' },
        justifyContent: 'center',
        alignItems: 'center',
        // padding:'32px 0 0 32px'
      }}
    >
      <Grid>
        <Grid item sx={{ padding: '0 0 0 24px' }}>
          <Box sx={{ width: 'fit-content', margin: 'auto' }}>
            <Box sx={{ width: 'fit-content' }}>
              <Typography variant="subtitle2" sx={{ fontWeight: (theme) => theme.typography.fontWeightMedium }}>
                شبکه های اجتماعی
              </Typography>
              {/* <Divider
                  orientation="horizontal"
                  sx={{ width: 1, borderColor: (theme) => theme.palette.primary.main, borderWidth: '1px' }}
                /> */}
            </Box>
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', mt: 3 }}>
              <Grid sx={{ display: 'flex', flexDirection: 'row' }}>
                <SocialTheme>
                  <WhatsAppIcon htmlColor="#fff" className={classes.icon} />
                </SocialTheme>
                <SocialTheme>
                  <TelegramIcon htmlColor="#fff" className={classes.icon} />
                </SocialTheme>
                <SocialTheme>
                  <CardMedia
                    sx={{ width: 20 }}
                    component="img"
                    image="static/images/Linkdinicon.svg"
                    className={classes.icon}
                  />
                </SocialTheme>
              </Grid>
              <Grid sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <SocialTheme>
                  <InstagramIcon htmlColor="#fff" className={classes.icon} />
                </SocialTheme>
                <SocialTheme>
                  <TwitterIcon htmlColor="#fff" className={classes.icon} />
                </SocialTheme>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
}
