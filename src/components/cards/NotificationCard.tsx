import React, { ReactElement, useState } from 'react';
import { Badge, Button } from '@mui/material';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import NotificationMenu from '../menus/NotificationMenu';
import { Box } from '@mui/system';

interface Props {}

const NotificationCard = (props: Props): ReactElement => {
  const [openAnchor, setOpenAnchor] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
    setOpenAnchor(!openAnchor);
  };

  return (
    <Box sx={{ position: 'relative', display: 'inline-block' }}>
      <Button variant="text" sx={{ color: (theme) => theme.palette?.text?.secondary }} onClick={handleClick}>
        <Badge
          variant="dot"
          sx={{
            '.MuiBadge-dot': {
              background: 'red !important',
              top: '8px',
              right: '6px',
            },
          }}
        >
          <NotificationsNoneOutlinedIcon />
        </Badge>
      </Button>
      <NotificationMenu
        open={openAnchor}
        handleClose={() => setOpenAnchor(false)}
        handleEl={anchorEl}
        handleCloseEl={setAnchorEl}
      />
    </Box>
  );
};

export default NotificationCard;
