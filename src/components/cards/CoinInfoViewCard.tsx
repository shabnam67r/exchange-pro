import React, { ReactElement } from 'react';
import BaseLineChart from '../charts/BaseLineChart';
import { Box } from '@mui/system';
import { Button, CardMedia, Typography } from '@mui/material';
import { alpha } from '@mui/material/styles';
import ChangesRate from '../smallComponents/ChangesRate';
import { OptionChartAxesLessAndWithPoint } from '../../feature/chartOptions';
import useTranslate from '../../hooks/useTranslate';
import { Buttons, Overview } from '../../interfaces/translate';
import TransActionStartIcon from '../../icons/TransactionStart';
import { validDisplayPrice } from '../../feature/function';
import NextLink from '../smallComponents/NextLink';

interface Props {
  data: { color: string; amount: number; sellPrice: number; buyPrice: number; label: string };
}

const CoinInfoViewCard = (props: Props): ReactElement => {
  const transition = useTranslate();
  const titleText: Overview = transition('webSiteConstant.dashboard.overview');
  const buttonText: Buttons = transition('webSiteConstant.buttons');

  /******************************************************************************
   *                            Chart Info
   *****************************************************************************/

  const data = {
    datasets: [
      {
        data: [40, 38, 48, 54, 50, 46, 67, 57, 50, 40, 46, 37, 61, 49, 38, 50],
        fill: true,
        tension: 0.2,
        backgroundColor: alpha(props.data.color, 0.1),
        borderColor: props.data.color,
        borderWidth: 4,
        borderCapStyle: 'round',
        pointBorderWidth: 4,
        pointBackgroundColor: 'white',
      },
    ],
    labels: ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
  };

  return (
    <Box mt={{ md: 1 }}>
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', mb: 3 }}>
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <CardMedia component="img" sx={{ width: 40, height: 40 }} src="/static/images/Bitcoin-2.png" alt="coin" />
          <Typography variant="body2" mr={0.5}>
            {props.data.label}
          </Typography>
        </Box>
        <ChangesRate value={props.data.amount} positive={props.data.amount > 0} />
      </Box>
      <Box sx={{ width: { xs: '85%', md: '90%' }, mx: 'auto' }}>
        <BaseLineChart chartData={data} options={OptionChartAxesLessAndWithPoint(data?.datasets[0]?.data?.length)} />
      </Box>
      <Box mt={1}>
        <Box sx={{ display: 'flex', justifyContent: 'center', width: '100%', mb: 1 }}>
          <Typography variant="body2" color={(theme) => theme.palette?.success?.main} ml={0.5}>
            {titleText.buyingPrice}:
          </Typography>
          <Typography variant="body2" color={(theme) => theme.palette?.success?.main}>
            {validDisplayPrice(props.data.buyPrice)}
          </Typography>
        </Box>
        <Box sx={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
          <Typography variant="body2" color="error" ml={0.5}>
            {titleText.sellingPrice}:
          </Typography>
          <Typography variant="body2" color="error">
            {validDisplayPrice(props.data.sellPrice)}
          </Typography>
        </Box>
      </Box>
      <Box sx={{ textAlign: 'center', mt: 3 }}>
        <NextLink href="/dashboard/trade">
          <Button variant="contained" color="primary">
            <TransActionStartIcon />
            <Typography variant="body2" mr={1}>
              {buttonText.startTrading}
            </Typography>
          </Button>
        </NextLink>
      </Box>
    </Box>
  );
};

export default CoinInfoViewCard;
