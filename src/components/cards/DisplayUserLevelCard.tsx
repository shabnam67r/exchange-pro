import React, { ReactElement } from 'react';
import { Box, Theme, useTheme } from '@mui/system';
import { Card, Slider, Typography, useMediaQuery } from '@mui/material';
import { makeStyles } from '@mui/styles';
import useTranslate from '../../hooks/useTranslate';
import { UserLevel } from '../../interfaces/translate';
import Logo from '../../icons/Logo';
import SettingsRoundedIcon from '@mui/icons-material/Settings';

interface Props {}

const DisplayUserLevelsCard = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const cardText: UserLevel = transition('webSiteConstant.dashboard.userLevel');
  const tablet = useMediaQuery(theme.breakpoints.down('md'));

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      width: tablet ? '100%' : '80%',
      height: 300,
      display: 'flex',
      flexDirection: 'column',
      background:
        'linear-gradient(85.96deg, rgba(255, 255, 255, 0) -20.51%, rgba(255, 255, 255, 0.05) 26.82%, rgba(255, 255, 255, 0) 65.65%), rgba(196, 196, 196, 0.05)',
      boxShadow: 'inset 0px -2px 1px rgba(255, 255, 255, 0.1)',
      backdropFilter: 'blur(30px)',
      borderRadius: theme.shape.borderRadius,
      '& div.gold, & span.gold': {
        color: '#CCA482 !important',
      },
    },
  }));

  const classes = useStyles(props);

  function valuetext(value: number) {
    return `${value}°C`;
  }

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center', height: '100%' }}>
      <Card className={classes.root}>
        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Logo color={theme.palette.text.primary} sx={{ mx: 1, width: 40, height: 40 }} />
            <Box>
              <Typography variant="subtitle2">امیر محمد عباس منش</Typography>
              <Typography variant="caption" color={(theme) => theme.palette.text.secondary}>
                {cardText.card.levels[2]}
              </Typography>
            </Box>
          </Box>
          <Box
            sx={{
              border: '1px solid white',
              height: 'fit-content',
              borderRadius: '50%',
              pt: 1,
              px: 1.1,
              cursor: 'pointer',
            }}
          >
            <SettingsRoundedIcon />
          </Box>
        </Box>
        <Box sx={{ display: 'flex', justifyContent: 'space-between', my: 3 }}>
          <Box>
            <Typography variant="subtitle2">{cardText.card.CurrentWage} :</Typography>
          </Box>
          <Box className="gold" sx={{ textAlign: 'center' }}>
            <Typography variant="subtitle1">%0/4</Typography>
            <Typography variant="caption">{cardText.card.levels[1]}</Typography>
          </Box>
        </Box>
        <Box sx={{ mt: 'auto' }}>
          <Box>
            <Slider defaultValue={50} aria-label="Default" valueLabelDisplay="auto" />
          </Box>
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Typography variant="body1">{cardText.card.levels[1]}</Typography>
            <Typography variant="caption" className="gold">
              {cardText.card.levels[0]}
            </Typography>
          </Box>
        </Box>
      </Card>
    </Box>
  );
};

export default DisplayUserLevelsCard;
