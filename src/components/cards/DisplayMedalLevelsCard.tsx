import React, { ReactElement } from 'react';
import { Box } from '@mui/system';
import { CardMedia, Grid } from '@mui/material';

interface Props {}

const DisplayMedalLevelsCard = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Modal List
   *****************************************************************************/
  const modalList = [
    '/static/images/medals/social icon (1).png',
    '/static/images/medals/social icon (2).png',
    '/static/images/medals/social icon (3).png',
    '/static/images/medals/social icon (4).png',
    '/static/images/medals/social icon.png',
  ];

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
      <Grid container item xs={7} justifyContent="center" alignItems="center">
        {modalList.map((val, index) => (
          <Grid item xs={4} key={index}>
            <CardMedia component="img" sx={{ width: '100%', height: '100%' }} src={val} alt="coin" />
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

export default DisplayMedalLevelsCard;
