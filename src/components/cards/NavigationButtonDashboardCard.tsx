import React, { ReactElement, useState } from 'react';
import { BottomNavigation, BottomNavigationAction, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { useRouter } from 'next/router';

interface linkList {
  link: string;
  icon: ReactElement;
  text: string;
}

interface Props {
  data: linkList[];
}

const NavigationButtonDashboard = (props: Props): ReactElement => {
  const [value, setValue] = useState(0);
  const router = useRouter();

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      zIndex: theme?.zIndex?.drawer + 1,
      '& button.Mui-selected': {
        '&:before': {
          content: "''",
          display: 'block',
          position: 'absolute',
          bottom: -5,
          width: 12,
          height: 12,
          borderRadius: '50%',
          background: theme?.palette?.primary?.main,
          boxShadow: theme.shadows[14],
        },
      },
    },
  }));

  const classes = useStyles(props);

  const handleLink = (link: string) => router.push(link);

  return (
    <BottomNavigation
      showLabels
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      className={classes.root}
      sx={{
        position: 'fixed',
        bottom: 0,
        background: 'rgba(41, 35, 106, 0.6)',
        backdropFilter: 'blur(56px)',
        width: '100%',
      }}
    >
      {props?.data?.map((item, index) => (
        <BottomNavigationAction icon={item.icon} onClick={() => handleLink(item.link)} />
      ))}
    </BottomNavigation>
  );
};

export default NavigationButtonDashboard;
