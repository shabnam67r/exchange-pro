import React, { ReactElement } from 'react';
import { Box } from '@mui/system';
import { CardMedia, Theme, Typography } from '@mui/material';
import ChangesRate from '../smallComponents/ChangesRate';
import { makeStyles } from '@mui/styles';

interface Props {
  data: {
    label: string;
    amount: number;
  };
  selected?: boolean;
}

const SlideOfMarketsPageCard = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      border: `2px solid ${theme.palette.divider}`,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginBottom: 3,
      borderRadius: theme.shape.borderRadius,
      padding: theme.spacing(5, 3),
      cursor: 'pointer',
      '&.active': {
        background: theme.palette.common.primaryCommonGradient,
      },
    },
  }));

  const classes = useStyles(props);

  return (
    <Box className={`${classes.root} ${props?.selected ? 'active' : ''}`}>
      <Box sx={{ display: 'flex', alignItems: 'center' }}>
        <CardMedia component="img" sx={{ width: 50, height: 50 }} src="/static/images/Bitcoin-2.png" alt="coin" />
        <Typography variant="body1" mr={0.5}>
          {props.data.label}
        </Typography>
      </Box>
      <ChangesRate value={props.data.amount} positive={props.data.amount > 0} colorDefault />
    </Box>
  );
};

export default SlideOfMarketsPageCard;
