import { useTheme, Grid, Card, Typography, CardMedia } from '@mui/material';
import React, { ReactElement } from 'react';
import { CardsFeatureOfHome } from 'src/interfaces/translate';
import useTranslate from 'src/hooks/useTranslate';
import { Box } from '@mui/system';

interface Props {}

export default function CoinCards(props: Props): ReactElement {
  const transition = useTranslate();
  const Cards: CardsFeatureOfHome[] = transition('webSiteConstant.homePage.feature.cards');
  const theme = useTheme();

  return (
    <Box>
      <Grid container sx={{ margin: '0 auto', flexWrap: { xs: 'wrap', md: 'nowrap' } }}>
        {Cards.map((list: CardsFeatureOfHome, index) => (
          <Grid item xs={12} sm={6} md={3} key={index}>
            <Box
              key={list.header}
              sx={{
                position: 'relative',
                margin: 'auto 3%',
              }}
            >
              <Box>
                {list.index == '1' || list.index == '3' ? (
                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Box
                      sx={{
                        zIndex: 0,
                        position: 'absolute',
                        width: 1,
                        height: '75%',
                        top: '10%',
                        borderRadius: '50%',
                        background: 'linear-gradient(113.22deg, #FF9500 0, #FF5E3A 100%, #FF5E3A 100%)',
                        boxShadow: ' 0px 2px 16px rgba(255, 116, 34, 0.5)',
                      }}
                    />
                    <Box
                      sx={{
                        boxShadow: '0px 2px 16px rgba(255, 116, 34, 0.5)',
                        zIndex: 0,
                        position: 'absolute',
                        width: '65%',
                        height: '30%',
                        top: '72%',
                        borderRadius: '32px',
                        background: 'linear-gradient(113.22deg, #FF9500 0, #FF5E3A 100%, #FF5E3A 100%)',
                      }}
                    />
                  </Box>
                ) : (
                  <Box
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Box
                      sx={{
                        boxShadow: '0px 2px 16px rgb(89 44 184 / 50%)',
                        zIndex: 0,
                        position: 'absolute',
                        width: 1,
                        height: '75%',
                        top: '10%',
                        borderRadius: '50%',
                        background: 'linear-gradient(97.06deg, #C644FC 0, #5859D6 100%)',
                      }}
                    />
                    <Box
                      sx={{
                        boxShadow: '0px 2px 16px rgb(89 44 184 / 50%)',
                        zIndex: 0,
                        position: 'absolute',
                        width: '65%',
                        height: '30%',
                        top: '72%',
                        borderRadius: '32px',
                        background: 'linear-gradient(97.06deg, #C644FC 0, #5859D6 100%)',
                      }}
                    />
                  </Box>
                )}
              </Box>

              <Card
                sx={{
                  boxShadow: (theme) => theme.shadows,
                  height: { xs: '350px', sm: '400px', md: '380px', xl: '500px' },
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                  margin: '5%',
                  border: `2px solid ${theme.palette.divider}`,
                  p: '0 !important',
                }}
              >
                <CardMedia component="img" src={list.src} sx={{ width: 0.5 }} alt="1" />

                <Typography
                  variant="subtitle2"
                  sx={{ margin: '5% auto', fontWeight: (theme) => theme.typography.fontWeightMedium }}
                >
                  {list.header}
                </Typography>
                <Typography variant="body2" sx={{ padding: '5%', textAlign: 'justify' }}>
                  {list.desc}
                </Typography>
              </Card>
            </Box>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
}
