import React, { ReactElement, useState } from 'react';
import { Box } from '@mui/system';
import { Avatar, Button } from '@mui/material';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';
import AvatarMenu from '../menus/AvatarMenu';

interface Props {}

const AvatarCard = (props: Props): ReactElement => {
  const [openAnchor, setOpenAnchor] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
    setOpenAnchor(!openAnchor);
  };

  return (
    <Box>
      <Button
        variant="contained"
        color="primary"
        sx={{ ml: 2, p: '5px !important', minWidth: 82 }}
        onClick={handleClick}
      >
        <Avatar
          alt="Natacha"
          src="/static/images/avatar/32px.svg"
          sx={{
            ml: '0 !important',
            mr: '13px !important',
            boxShadow: `0px 0px 0px 2px #fff8f3ba`,
            border: '1px solid white',
            transform: 'translateX(15px)',
            width: '28px !important',
            height: '28px !important',
          }}
        />
        <KeyboardArrowDownOutlinedIcon />
      </Button>
      <AvatarMenu
        open={openAnchor}
        handleClose={() => setOpenAnchor(false)}
        handleEl={anchorEl}
        handleCloseEl={setAnchorEl}
      />
    </Box>
  );
};

export default AvatarCard;
