import React, { lazy, ReactElement, Suspense, useState } from 'react';
import { CardMedia, Chip, Grid, Stack, Theme, Typography, useMediaQuery } from '@mui/material';
import { Box } from '@mui/system';
import { makeStyles } from '@mui/styles';
import { Buttons, WalletPage } from '../../interfaces/translate';
import useTranslate from '../../hooks/useTranslate';
import { useRouter } from 'next/router';

interface Props {
  data: {
    title: string;
    amount: number;
  };
}

const AssetInfoCard = (props: Props): ReactElement => {
  const transition = useTranslate();
  const buttonText: Buttons = transition('webSiteConstant.buttons');
  const textWalletPage: WalletPage = transition('webSiteConstant.walletPage');
  const [trigger, setTrigger] = useState(false);
  const router = useRouter();
  const smallScreen = useMediaQuery('(max-width:1500px)');

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      borderRadius: 15,
      border: `2px solid ${theme.palette.divider}`,
    },
    chip: {
      width: '100%',
      borderColor: theme.palette?.text.primary,
      paddingRight: 8,
      paddingLeft: 8,
      fontSize: smallScreen ? 12 : 14,
    },
  }));

  const classes = useStyles(props);

  const DialogComponent = trigger ? lazy(() => import('../../components/dialogs/WithdrawDialog')) : null;

  const handleClickOpen = () => setTrigger(true);

  const handleClose = () => setTrigger(false);

  const handleMarketLink = () => router.push('/dashboard/trade');

  return (
    <>
      <Box className={classes.root} p={{ xs: 1, md: 2 }}>
        <Box>
          <Stack direction={{ xs: 'column', md: 'row' }} justifyContent="space-between" alignItems="center">
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <CardMedia
                component="img"
                sx={{ width: 40, height: 40 }}
                src="/static/images/Bitcoin-2.png"
                alt="Bitcoin-2.png"
              />
              <Typography variant="caption" mr={1}>
                {props?.data?.title}
              </Typography>
            </Box>
            <Box
              sx={{
                display: 'flex',
                alignItems: 'end',
                borderBottom: (theme) => `1px solid ${theme.palette.primary.main}`,
              }}
            >
              <Typography variant="caption" color={(theme) => theme?.palette?.text?.disabled}>
                {props?.data?.amount?.toLocaleString()}
              </Typography>
              <Typography variant="body2" mr={0.5}>
                {props?.data?.title === 'ریالی' ? textWalletPage.wallet.moneyUnit : textWalletPage.wallet.coinUnit}
              </Typography>
            </Box>
          </Stack>
        </Box>
        <Box mt={4} sx={{ display: 'flex', justifyContent: 'center' }}>
          <Grid container columnSpacing={1} rowSpacing={{ xs: 2, md: 0 }} justifyContent="center" item xs={8} md={12}>
            <Grid item xs={12} md={4} sx={{ textAlign: 'center' }}>
              <Chip
                label={buttonText?.withdraw}
                variant="outlined"
                clickable
                onClick={handleClickOpen}
                className={classes.chip}
                sx={{
                  color: (theme) => theme?.palette?.success?.main,
                }}
              />
            </Grid>
            <Grid item xs={12} md={4} sx={{ textAlign: 'center' }}>
              <Chip
                label={buttonText?.deposit}
                variant="outlined"
                clickable
                className={classes.chip}
                sx={{
                  color: (theme) => theme?.palette?.primary?.main,
                }}
              />
            </Grid>
            {props?.data?.title !== 'ریالی' && (
              <Grid item xs={12} md={4} sx={{ textAlign: 'center' }}>
                <Chip
                  label={buttonText?.trade}
                  variant="outlined"
                  clickable
                  onClick={handleMarketLink}
                  className={classes.chip}
                  sx={{
                    color: '#2196F3',
                  }}
                />
              </Grid>
            )}
          </Grid>
        </Box>
      </Box>
      {trigger && (
        <Suspense fallback="">
          {/*// @ts-ignore*/}
          <DialogComponent open={trigger} onClose={handleClose} />
        </Suspense>
      )}
    </>
  );
};

export default AssetInfoCard;
