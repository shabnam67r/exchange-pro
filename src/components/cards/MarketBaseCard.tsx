import React, { ReactElement, useState } from 'react';
import { Box, Theme } from '@mui/system';
import { Typography } from '@mui/material';
import useTranslate from '../../hooks/useTranslate';
import { BazarBase } from '../../interfaces/translate';
import { makeStyles } from '@mui/styles';

interface Props {}

const MarketBaseCard = (props: Props): ReactElement => {
  const [state, setState] = useState('rial');
  const transition = useTranslate();
  const buttonsText: BazarBase = transition('webSiteConstant.header.bazarBase');

  const handleClick = (currency: string): void => setState(currency);

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    textBtn: {
      cursor: 'pointer',
      marginRight: 5,
      padding: '6px 20px',
      color: theme.palette.text.secondary,
      '&.active': {
        background: theme.palette?.common?.primaryCommonGradient,
        borderRadius: '15px',
        color: theme.palette.text.primary,
      },
    },
  }));

  const classes = useStyles();

  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
      <Typography variant="body2" sx={{ mr: 3, ml: 2 }}>
        {buttonsText?.name} :
      </Typography>
      <Typography
        variant="caption"
        className={`${classes.textBtn} ${state === 'rial' ? 'active' : ''}`}
        onClick={() => handleClick('rial')}
      >
        {buttonsText?.rial}
      </Typography>
      <Typography
        variant="caption"
        className={`${classes.textBtn} ${state === 'toman' ? 'active' : ''}`}
        onClick={() => handleClick('toman')}
      >
        {buttonsText?.toman}
      </Typography>
    </Box>
  );
};

export default MarketBaseCard;
