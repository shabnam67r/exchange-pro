import { Box, Grid } from '@mui/material';
import React, { ReactElement } from 'react';
import { useForm } from 'react-hook-form';
import CustomTextField from 'src/components/TextField';

interface Props {
  label: string;
}

type Inputs = {
  name: string;
  subject: string;
  email: string;
  phoneNumber: number;
  description: string;
};
export default function Inputs({ props }: any): ReactElement {
  const {
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();
  const onSubmit = (data: any) => console.log(data);

  return (
    <Box component="form" onSubmit={handleSubmit(onSubmit)} sx={{ minWidth: { md: 500 } }}>
      <Grid container>
        <Grid item xs={12}>
          <CustomTextField />
        </Grid>
      </Grid>
    </Box>
  );
}
