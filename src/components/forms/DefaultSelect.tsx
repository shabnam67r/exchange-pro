import { ReactElement } from 'react';
import { MenuItem, MenuItemProps, Select, SelectProps, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { SxProps } from '@mui/system';

interface ItemProp {
  value?: string | number;
  label?: string;
  [key: string]: any;
}

interface Props {
  items: Array<ItemProp> | Array<string>;
  handleChange: (e?: any) => void;
  selected?: string | number;
  menuItem?: MenuItemProps;
  selectProps?: SelectProps;
  label?: string;
  variant?: 'filled' | 'outlined' | 'standard';
  sx?: SxProps;
  pill?: boolean;
}

const DefaultSelect = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      padding: 10,
      background: props?.pill ? theme?.palette?.common?.primaryCommonGradient : 'default',
      '& > div.MuiSelect-select': {
        paddingLeft: '10px !important',
      },
      '& > svg': {
        position: 'absolute',
        top: 'calc(50% - 0.5em)',
        right: '7px !important',
        left: 'unset',
      },
    },
  }));

  const classes = useStyles(props);

  return (
    <Select
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      variant={props?.variant}
      value={props?.selected || props?.items?.[0]}
      label={props?.label}
      onChange={props?.handleChange}
      className={classes.root}
      sx={{ ...props?.sx }}
      {...props?.selectProps}
    >
      {props?.items?.map((val: any, index) => (
        <MenuItem value={val?.value || val} key={index} {...props?.menuItem}>
          {val?.label || val?.value || val}
        </MenuItem>
      ))}
    </Select>
  );
};

export default DefaultSelect;
