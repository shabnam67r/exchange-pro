import { Box, Grid, Card, CardMedia } from '@mui/material';
import React, { ReactElement } from 'react';
import { useForm } from 'react-hook-form';

interface Props {
  props: any;
}
type Inputs = {
  name: string;
  subject: string;
  email: string;
  phoneNumber: number;
  description: string;
};
export default function NewPassword({ props }: Props): ReactElement {
  const { handleSubmit } = useForm<Inputs>();
  const onSubmit = (data: any) => console.log(data);

  return (
    <Box component="form" onSubmit={handleSubmit(onSubmit)}>
      <Grid container>
        <Grid
          item
          xs={12}
          md={6}
          sx={{
            margin: '10% auto',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <CardMedia
            component="img"
            src="/static/images/logoform.png"
            sx={{ top: { xs: '-2%', sm: '7%', lg: '12%' }, position: 'absolute', zIndex: 1, width: 'auto' }}
          />
          <Card
            sx={{
              margin: 'auto',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            {props.children}
          </Card>
        </Grid>
      </Grid>
    </Box>
  );
}
