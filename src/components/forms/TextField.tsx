import { Typography, TextField, StandardTextFieldProps, FormHelperText } from '@mui/material';
import React, { ReactElement } from 'react';
import { Box, BoxProps } from '@mui/system';

type TextFieldProps = Omit<StandardTextFieldProps, 'variant'>;
interface Props extends TextFieldProps {
  variant?: 'outlined' | 'filled' | 'standard';
  componentProps?: BoxProps;
  errors?: any;
  [key: string]: any;
}
export default function CustomTextField(props: Props): ReactElement {
  const { label, ...newProps } = props;

  return (
    <Box {...props.componentProps}>
      {label && (
        <Typography variant="caption" sx={{ pr: 2.5 }}>
          {label}
        </Typography>
      )}
      <TextField {...newProps} variant="outlined" />
      {props?.errors && (
        <FormHelperText id="outlined-weight-helper-text" error>
          {props.errors}
        </FormHelperText>
      )}
    </Box>
  );
}
