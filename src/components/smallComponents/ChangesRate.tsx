import { ReactElement } from 'react';
import { Typography } from '@mui/material';
import { Box, TypographyProps } from '@mui/system';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { handleNatureNumber } from '../../feature/function';

interface Props {
  value: number;
  positive: boolean;
  size?: number;
  textProps?: TypographyProps;
  colorDefault?: boolean;
}

const ChangesRate = (props: Props): ReactElement => {
  return (
    <Box>
      <Typography
        variant="caption"
        color={
          props.colorDefault
            ? (theme) => theme.palette?.text?.secondary
            : (theme) => (props.positive ? theme.palette?.success?.main : theme.palette?.error?.main)
        }
        fontSize={props?.size || 12}
        sx={{ display: 'flex', alignItems: 'center' }}
        {...props.textProps}
      >
        % {handleNatureNumber(props.value)}
        {props.positive ? '+' : '-'}
        {props.positive ? (
          <KeyboardArrowUpIcon sx={{ fontSize: 16 }} />
        ) : (
          <KeyboardArrowDownIcon sx={{ fontSize: 16 }} />
        )}
      </Typography>
    </Box>
  );
};

export default ChangesRate;
