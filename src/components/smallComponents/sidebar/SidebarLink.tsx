import { ReactElement } from 'react';
import Link from 'next/link';
import { Children } from '../../../interfaces/componentProps';

interface Props {
  children: Children;
  link: string;
}

const SidebarLink = (props: Props): ReactElement => {
  return (
    <Link href={props.link}>
      <a>{props.children}</a>
    </Link>
  );
};

export default SidebarLink;
