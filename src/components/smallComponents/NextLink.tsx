import React, { ReactElement } from 'react';
import Link from 'next/link';
import { Children } from '../../interfaces/componentProps';
import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/system';

interface Props {
  children: Children | string;
  href: string | {};
  linkProps?: any;
  color?: any;
}

const NextLink = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      textDecoration: 'none',
      color: props?.color || theme?.palette?.text?.secondary,
    },
  }));

  const classes = useStyles(props);

  return (
    <Link href={props.href} {...props.linkProps}>
      <a className={classes.root}>{props.children}</a>
    </Link>
  );
};

export default NextLink;
