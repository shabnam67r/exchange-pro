import React, { ReactElement } from 'react';
import { Avatar as Av, Button } from '@mui/material';

interface Props {}

const Avatar: React.FC<Props> = (): ReactElement => {
  return (
    <Button
      variant="text"
      color="primary"
      sx={{
        borderRadius: '50%',
        backgroundColor: (theme) => theme.palette.primary.main,
        width: 'fit-content',
        p: 0.5,
        minWidth: '0 !important',
      }}
    >
      <Av
        alt="Natacha"
        src="/static/images/avatar/32px.svg"
        sx={{
          border: '3px solid #fff8f3ba',
        }}
      />
    </Button>
  );
};

export default Avatar;
