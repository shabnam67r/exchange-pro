import React, { ReactElement } from 'react';
import { Avatar, Button, Typography } from '@mui/material';
import useTranslate from '../../../hooks/useTranslate';
import NextLink from '../NextLink';

interface Props {}

const AvatarTextButton: React.FC<Props> = (): ReactElement => {
  const transition = useTranslate();
  const text: string = transition('webSiteConstant.header.userPanel.name');

  return (
    <NextLink href="/auth/personalIdentification">
      <Button variant="contained" color="primary" sx={{ ml: 2, p: '5px !important' }}>
        <Avatar
          alt="zarinex logo"
          src="/static/images/avatar/32px.svg"
          sx={{
            ml: '0 !important',
            mr: '13px !important',
            boxShadow: `0px 0px 0px 2px #fff8f3ba`,
            border: '1px solid white',
            transform: 'translateX(15px)',
            width: '28px !important',
            height: '28px !important',
          }}
        />
        <Typography variant="caption">{text}</Typography>
      </Button>
    </NextLink>
  );
};

export default AvatarTextButton;
