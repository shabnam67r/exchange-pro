import React, { ReactElement } from 'react';
import BaseLineChart from '../charts/BaseLineChart';
import { OptionChartAxesLess } from '../../feature/chartOptions';
import { Box, BoxProps } from '@mui/system';
import { ChartInitialData } from '../../feature/chartInitialData';
import { Typography } from '@mui/material';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { handleNatureNumber } from '../../feature/function';

interface Props {
  changePercent: number;
  boxProps: BoxProps;
}

const LittleChart = (props: Props): ReactElement => {
  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Box>
        <ArrowForwardIcon
          sx={{
            fontSize: 12,
            ml: 1,
            transform: `rotate(${props.changePercent > 0 ? '-' : '+'}45deg)`,
            color: (theme) => (props.changePercent > 0 ? theme.palette?.success?.main : theme.palette?.error?.main),
          }}
        />
        <Typography
          variant="caption"
          fontSize={10}
          dir="ltr"
          color={(theme) => (props.changePercent > 0 ? theme.palette?.success?.main : theme.palette?.error?.main)}
        >
          {props.changePercent > 0 ? `+ ${props.changePercent}` : `- ${handleNatureNumber(props.changePercent)}`}
        </Typography>
      </Box>
      <Box {...props.boxProps}>
        <BaseLineChart chartData={ChartInitialData(props.changePercent > 0)} options={OptionChartAxesLess} />
      </Box>
    </Box>
  );
};

export default LittleChart;
