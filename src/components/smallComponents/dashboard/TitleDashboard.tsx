import { ReactElement } from 'react';
import { Box, BoxProps, SxProps, TypographyProps } from '@mui/system';
import { makeStyles } from '@mui/styles';
import { Theme, Typography } from '@mui/material';

interface Props {
  title: string;
  subTitle?: string | ReactElement;
  subTitleTypographyProps?: TypographyProps;
  subTitleSx?: SxProps;
  titleTypographyProps?: TypographyProps;
  filterOption?: ReactElement;
  color?: string;
  titleBox?: BoxProps;
}

const TitleDashboard = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      width: '100%',
      borderBottom: `2px solid ${theme.palette?.divider}`,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      '& > div:first-child:after': {
        content: "''",
        display: 'block',
        width: '90%',
        height: '2px',
        background: props?.color || '#FF9800',
        boxShadow: `0 0 30px 2px ${props?.color || '#FF9500'}`,
        borderRadius: '4px',
      },
    },
  }));

  const classes = useStyles(props);

  return (
    <Box className={classes.root}>
      <Box {...props.titleBox}>
        <Typography variant="subtitle2" sx={{ pr: 1, pb: 2 }} {...props.titleTypographyProps}>
          {props.title}
        </Typography>
      </Box>
      {props?.subTitle && (
        <Typography
          variant="caption"
          sx={{ pr: 1, pb: 2, color: (theme) => theme?.palette?.text?.secondary, ...props.subTitleSx }}
          {...props.subTitleTypographyProps}
        >
          {props.subTitle}
        </Typography>
      )}
      {props?.filterOption && props.filterOption}
    </Box>
  );
};

export default TitleDashboard;
