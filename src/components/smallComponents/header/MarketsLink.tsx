import React, { ReactElement, useState } from 'react';
import { Button, Typography } from '@mui/material';
import useTranslate from '../../../hooks/useTranslate';
import { Header } from '../../../interfaces/translate';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';
import { Box } from '@mui/system';
import MarketsMenu from '../../menus/MarketsMenus';

interface Props {}

const MarketsLink = (props: Props): ReactElement => {
  const [openAnchor, setOpenAnchor] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const transition = useTranslate();
  const headerText: Header = transition('webSiteConstant.header');

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
    setOpenAnchor(!openAnchor);
  };

  return (
    <Box>
      <Button
        variant="text"
        sx={{ '& p': { color: (theme) => theme.palette?.common?.primaryCommonGradient } }}
        onClick={handleClick}
      >
        <Typography variant="body1">{headerText?.links?.[1]?.name}</Typography>
        <KeyboardArrowDownOutlinedIcon />
      </Button>
      <MarketsMenu
        open={openAnchor}
        handleClose={() => setOpenAnchor(false)}
        handleEl={anchorEl}
        handleCloseEl={setAnchorEl}
      />
    </Box>
  );
};

export default MarketsLink;
