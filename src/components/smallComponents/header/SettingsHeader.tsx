import React, { ReactElement, Suspense } from 'react';
import { Box } from '@mui/system';
import { Button, Theme, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import SettingsRoundedIcon from '@mui/icons-material/Settings';
import SmsIcon from '@mui/icons-material/Sms';
import useTranslate from '../../../hooks/useTranslate';
import NotificationCard from '../../cards/NotificationCard';
import { useRouter } from 'next/router';

interface Props {}

const SettingHeader = (props: Props): ReactElement => {
  const transition = useTranslate();
  const textField = transition('webSiteConstant.buttons.support');
  const router = useRouter();

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      width: 200,
      '& button': {
        padding: '10px 5px',
        minWidth: '0 !important',
      },
    },
    btn: {
      color: theme.palette?.text?.secondary,
    },
  }));

  const classes = useStyles();

  const handleTicketLink = () => router.push('/dashboard/tickets');

  const handleSettingsLink = () => router.push('/dashboard/settings');

  return (
    <>
      <Box className={classes.root}>
        <Button variant="text" className={classes.btn} onClick={handleTicketLink}>
          <SmsIcon fontSize="small" />
          <Typography variant="body2" mr={0.5}>
            {textField}
          </Typography>
        </Button>
        <NotificationCard />
        <Button
          variant="text"
          className={classes.btn}
          onClick={() => {
            return handleSettingsLink();
            // dispatch(changeTheme());
          }}
        >
          <SettingsRoundedIcon />
        </Button>
      </Box>
    </>
  );
};

export default SettingHeader;
