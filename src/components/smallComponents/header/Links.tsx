import React, { ReactElement, useEffect, useState } from 'react';
import { Box } from '@mui/system';
import { Link as Li, Typography } from '@mui/material';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Header } from 'src/interfaces/translate';
import { Theme } from '@mui/system';

interface Props {
  data: Header;
}

export default function Links(props: Props): ReactElement {
  const router = useRouter();
  const pathname = router.pathname;
  const [certainId, setCertainId] = useState('');
  const sameHomeRoute = [
    '/auth/login',
    '/auth/register',
    '/auth/forgetPassword',
    '/auth/addNewPassword',
    '/auth/personalIdentification',
    '/',
  ];
  const pageRoute = sameHomeRoute.includes(pathname) ? '/' : pathname;

  const handleActive = (link: string) => {
    return pageRoute === link;
  };

  useEffect(() => {
    setCertainId(pageRoute as string);
  }, [router.query]);

  return (
    <Box sx={{ display: 'flex', flexDirection: { xs: 'column', md: 'row' }, width: { xs: 1, md: 'auto' } }}>
      {props.data.links.map((link, index) => (
        <Link href={link.path} key={index}>
          <Li
            sx={{
              cursor: 'pointer',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              p: 2,
              textDecoration: 'none',
              color: (theme: Theme) =>
                certainId === link.path ? theme.palette.primary.main : theme.palette.text?.primary,
              ':hover': {
                filter: (theme: Theme) => `drop-shadow(0px 2px 8px ${theme.palette.primary.main})`,
              },
            }}
          >
            <Typography
              variant={router.asPath.includes(link.path) ? 'subtitle2' : 'body2'}
              sx={
                handleActive(link.path)
                  ? {
                      position: 'relative',
                      '&::after': {
                        content: "''",
                        backgroundColor: (theme: Theme) => theme.palette.primary.main,
                        width: 15,
                        height: 2,
                        position: 'absolute',
                        bottom: 0,
                        left: '50%',
                        transform: 'translate(-50%, 5px)',
                      },
                    }
                  : {}
              }
            >
              {link.name}
            </Typography>
          </Li>
        </Link>
      ))}
    </Box>
  );
}
