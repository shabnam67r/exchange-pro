import { ReactElement } from 'react';
import { makeStyles } from '@mui/styles';
import { Theme, Typography } from '@mui/material';
import { TypographyProps } from '@mui/system';

interface Props {
  text: string | ReactElement;
  typographyProps?: TypographyProps | Partial<any>;
}

const ParagraphOfList = (props: Props): ReactElement => {
  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      marginBottom: 8,
      '&:before': {
        content: "''",
        display: 'inline-block',
        width: 12,
        height: 5,
        borderRadius: 3,
        marginRight: 8,
        background: theme.palette.common?.primaryCommonGradient,
      },
    },
  }));

  const classes = useStyles(props);

  return (
    <Typography variant="body2" className={classes.root} {...props.typographyProps}>
      {props.text}
    </Typography>
  );
};

export default ParagraphOfList;
