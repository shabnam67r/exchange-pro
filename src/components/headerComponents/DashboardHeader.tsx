import React, { ReactElement } from 'react';
import { Box, Theme, useTheme } from '@mui/system';
import Logo from '../../icons/Logo';
import { Grid, SvgIcon, Typography, useMediaQuery } from '@mui/material';
import Avatar from '../smallComponents/avatarButton/Avatar';
import { Menu } from '@mui/icons-material';
import useTranslate from '../../hooks/useTranslate';
import { Intro } from '../../interfaces/translate';
import SettingsHeader from '../smallComponents/header/SettingsHeader';
import AvatarCard from '../cards/AvatarCard';
import { makeStyles } from '@mui/styles';
import MarketBaseCard from '../cards/MarketBaseCard';
import MarketsLink from '../smallComponents/header/MarketsLink';
import { useRouter } from 'next/router';
import SwitchThemeCard from '../cards/SwitchThemeCard';

interface Props {
  handleDrawer?: () => void;
}

const DashboardHeader = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const titleText: Intro = transition('webSiteConstant.homePage.intro');
  const tablet = useMediaQuery(theme.breakpoints.down('md'));
  const router = useRouter();

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    grid: {
      height: 'fit-content',
    },
  }));

  const classes = useStyles(props);

  const handleHomeLink = () => router.push('/');

  return (
    <Grid container justifyContent="space-between" alignItems="center">
      <Grid
        item
        /* @ts-ignore */
        xs={1.5}
        sx={{
          display: 'flex',
          alignItems: 'center',
          cursor: 'pointer',
          '& img': { cursor: 'pointer' },
        }}
        onClick={handleHomeLink}
      >
        <Box sx={{ '& svg': { width: '45px', height: '45px' } }}>
          <Logo color={theme.palette.text.primary} />
        </Box>
        <Box>
          <Typography variant="h2" sx={{ fontSize: '16px', mr: 1 }}>
            {titleText?.header1}
          </Typography>
          {!tablet && (
            <Typography
              variant="caption"
              sx={{ mr: 1, color: (theme) => theme?.palette?.text?.secondary, fontSize: 10 }}
            >
              {titleText?.header2}
            </Typography>
          )}
        </Box>
      </Grid>
      {/* @ts-ignore */}
      <Grid item xs={10.5}>
        <Grid container justifyContent={{ xs: 'end', md: 'space-between' }}>
          {!tablet && (
            <Grid item sx={{ display: 'flex', alignItems: 'center' }}>
              <SwitchThemeCard />
              <MarketBaseCard />
              <MarketsLink />
            </Grid>
          )}

          {tablet ? (
            <Grid item className={classes.grid} sx={{ display: 'flex', alignItems: 'center' }}>
              <Avatar />
              <SvgIcon onClick={props.handleDrawer} sx={{ mx: 1, color: (theme) => theme.palette.text.secondary }}>
                <Menu />
              </SvgIcon>
            </Grid>
          ) : (
            <Grid item className={classes.grid}>
              <Grid container columnSpacing={1}>
                <Grid item xs={7}>
                  <SettingsHeader />
                </Grid>
                <Grid item>
                  <AvatarCard />
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default DashboardHeader;
