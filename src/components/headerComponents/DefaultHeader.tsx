import React, { ReactElement } from 'react';
import { Box, useTheme } from '@mui/system';
import { Button, Grid, SvgIcon, Typography, useMediaQuery } from '@mui/material';
import PersonIcon from '@mui/icons-material/Person';
import { Menu } from '@mui/icons-material';
import Links from '../smallComponents/header/Links';
import AvatarTextButton from '../smallComponents/avatarButton/AvatarTextButton';
import useTranslate from '../../hooks/useTranslate';
import { isUserAuthenticated } from '../../feature/authUtils';
import { Buttons, Header } from '../../interfaces/translate';
import Logo from '../../icons/Logo';
import { useRouter } from 'next/router';
import NextLink from '../smallComponents/NextLink';
import SwitchThemeCard from '../cards/SwitchThemeCard';

interface Props {
  handleDrawer?: () => void;
}

const DefaultHeader = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const userAuthenticated = isUserAuthenticated();
  const headerText: Header = transition('webSiteConstant.header');
  const buttonsText: Buttons = transition('webSiteConstant.buttons');
  const titleText: string = transition('webSiteConstant.homePage.intro.header1');
  const tablet = useMediaQuery(theme.breakpoints.down('md'));
  const router = useRouter();

  const handleLoginLink = () => router.push('/auth/login');

  const handleRegisterLink = () => router.push('/auth/register');

  return (
    <Grid container justifyContent={{ xs: 'space-between', md: 'space-around' }} alignItems="center">
      <Grid
        item
        /* @ts-ignore */
        xs={0.6}
        sx={{
          display: 'flex',
          alignItems: 'center',
          '& img': { cursor: 'pointer' },
        }}
      >
        <Box sx={{ '& svg': { width: '45px', height: '45px' } }}>
          <NextLink href="/">
            <Logo color={theme.palette.text.primary} />
          </NextLink>
        </Box>
        {tablet && (
          <Typography variant="h2" sx={{ fontSize: '16px', mr: 1 }}>
            {titleText}
          </Typography>
        )}
      </Grid>
      {tablet ? (
        <Grid item sx={{ height: 'fit-content', display: 'flex', alignItems: 'center' }}>
          <Button variant="text" color="primary" sx={{ minWidth: '0' }}>
            <PersonIcon sx={{ ml: 1 }} />
            {buttonsText.register}
          </Button>
          <SvgIcon onClick={props.handleDrawer} sx={{ mx: 1, color: (theme) => theme.palette.text.secondary }}>
            <Menu />
          </SvgIcon>
        </Grid>
      ) : (
        <>
          <Grid item xs={8} sx={{ height: 'fit-content', display: 'flex', justifyContent: 'center' }}>
            <Links data={headerText} />
          </Grid>
          {userAuthenticated ? (
            <AvatarTextButton />
          ) : (
            <Grid
              item
              /* @ts-ignore */
              xs={3.4}
              sx={{ height: 'fit-content', display: 'flex', alignItems: 'center', justifyContent: 'center' }}
            >
              <Button variant="contained" color="primary" sx={{ mx: 2 }} onClick={handleRegisterLink}>
                <PersonIcon sx={{ ml: 1 }} />
                {buttonsText.register}
              </Button>
              <Button variant="outlined" color="primary" onClick={handleLoginLink}>
                {buttonsText.enter}
              </Button>
              <Box>
                <SwitchThemeCard />
              </Box>
            </Grid>
          )}
        </>
      )}
    </Grid>
  );
};

export default DefaultHeader;
