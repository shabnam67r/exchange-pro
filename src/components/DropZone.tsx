import { CardMedia, Grid, Typography } from '@mui/material';
import React, { ReactElement, useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import { webSiteConstant } from 'src/translation/translation-fa.json';

interface Props {}

function DropzoneAreaExample({}: Props): ReactElement {
  const onDrop = useCallback((acceptedFiles) => {
    acceptedFiles.forEach((file: any) => {
      const reader = new FileReader();

      reader.onabort = () => console.log('file reading was aborted');
      reader.onerror = () => console.log('file reading has failed');
      reader.onload = () => {
        // Do whatever you want with the file contents
        const binaryStr = reader.result;
        console.log(binaryStr);
      };
      reader.readAsArrayBuffer(file);
    });
  }, []);
  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <Grid
      {...getRootProps()}
      sx={{
        display: 'flex',
        flexDirection: { xs: 'column', md: 'row' },
        alignItems: 'center',
        justifyContent: 'space-around',
      }}
    >
      <input {...getInputProps()} />
      <Typography>{webSiteConstant.registerPage.personalIdentification.identiyCardInput}</Typography>
      <CardMedia
        component="img"
        sx={{ width: '20% !important' }}
        src="/static/images/add image.png"
        alt="Dropzone Image"
      />
    </Grid>
  );
}

export default DropzoneAreaExample;
