import React, { ReactElement } from 'react';
import { Card, CardContent, TableCell, TableRow, useMediaQuery } from '@mui/material';
import { alpha, Box, useTheme } from '@mui/system';
import useTranslate from '../../hooks/useTranslate';
import { MarketPage } from '../../interfaces/translate';
import TitleDashboard from '../smallComponents/dashboard/TitleDashboard';
import PrimaryTable from '../tables/PrimaryTable';
import { RecentTransactionList } from '../../interfaces/totalData';
import { validDisplayPrice } from '../../feature/function';

interface Props {
  primaryTableProps?: any;
}

const RecentTransactionsCard = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const tablesText: MarketPage = transition('webSiteConstant.bazarPage');
  const smallScreen = useMediaQuery(theme.breakpoints.down('lg'));

  function createData(amount: number, unitPrice: number, creatAt: string): RecentTransactionList {
    return { amount, unitPrice, creatAt };
  }

  const rows = [
    createData(2, 15956789, '1400/3/8'),
    createData(2, 23756789, '1400/3/8'),
    createData(2, 26256789, '1400/3/8'),
    createData(2, 30556789, '1400/3/8'),
    createData(3, 356456789, '1400/3/8'),
    createData(3, 356456789, '1400/3/8'),
    createData(3, 356456789, '1400/3/8'),
    createData(3, 356456789, '1400/3/8'),
  ];

  return (
    <Box>
      <Card sx={{ px: '10px !important' }}>
        <TitleDashboard
          title="آخرین معاملات"
          subTitle="آزادی -ریال"
          titleTypographyProps={{ fontSize: smallScreen ? 10 : 14 }}
        />
        <CardContent sx={{ p: 0, mt: 2 }}>
          <PrimaryTable headers={tablesText?.tables?.lastChanges} {...props.primaryTableProps}>
            {rows.map((row, index) => (
              <TableRow
                key={index}
                sx={{
                  '& td': {
                    color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5),
                    fontSize: smallScreen ? 10 : 12,
                  },
                }}
              >
                <TableCell>{validDisplayPrice(row.amount)}</TableCell>
                <TableCell align="right">{validDisplayPrice(row.unitPrice)}</TableCell>
                <TableCell align="right">{row.creatAt}</TableCell>
              </TableRow>
            ))}
          </PrimaryTable>
        </CardContent>
      </Card>
    </Box>
  );
};

export default RecentTransactionsCard;
