import React, { ReactElement } from 'react';
import { Card, CardContent, TableCell, TableRow, useMediaQuery } from '@mui/material';
import useTranslate from '../../hooks/useTranslate';
import { MarketPage } from '../../interfaces/translate';
import PrimaryTable from '../tables/PrimaryTable';
import { alpha, useTheme } from '@mui/system';
import LittleChart from '../smallComponents/LittleChart';
import { CoinsInfoList } from '../../interfaces/totalData';
import { validDisplayPrice } from '../../feature/function';

interface Props {
  primaryTableProps?: any;
}

const CoinsInfoCard = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const tablesText: MarketPage = transition('webSiteConstant.bazarPage');
  const smallScreen = useMediaQuery(theme.breakpoints.down('lg'));

  function createData(name: string, price: number, changes: number): CoinsInfoList {
    return { name, price, changes };
  }

  const rows = [
    createData('سکه آزادی', 159, 1300.0),
    createData('نیم سکه آزادی', 237, -1300.0),
    createData('ربع سکه آزادی', 262, 1200.0),
    createData('سکه امامی', 305, -1400.0),
    createData('نیم سکه امامی', 356, 1100.0),
  ];

  return (
    <Card sx={{ py: '5px !important', px: '10px !important' }}>
      <CardContent sx={{ p: 0 }}>
        <PrimaryTable headers={tablesText?.tables?.lastChanges} {...props.primaryTableProps}>
          {rows.map((row, index) => (
            <TableRow
              key={index}
              sx={{
                '& td': {
                  color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5),
                  fontSize: smallScreen ? 10 : 14,
                },
              }}
            >
              <TableCell>{row.name}</TableCell>
              <TableCell align="right">{validDisplayPrice(row.price)}</TableCell>
              <TableCell align="right">
                <LittleChart changePercent={row.changes} boxProps={{ sx: { width: '80%' } }} />
              </TableCell>
            </TableRow>
          ))}
        </PrimaryTable>
      </CardContent>
    </Card>
  );
};

export default CoinsInfoCard;
