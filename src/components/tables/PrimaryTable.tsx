import React, { ReactElement } from 'react';
import {
  CircularProgress,
  CircularProgressProps,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Theme,
  useMediaQuery,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import DefaultPagination from '../DefaultPagination';
import { Box, useTheme } from '@mui/system';
import { PaginationProps } from '@mui/lab';

interface Props {
  headers: string[];
  loading?: boolean;
  children: ReactElement[] | any;
  mainHead?: boolean;
  responsive?: boolean;
  countPage?: number;
  selectPage?: number;
  paginationProps?: PaginationProps;
  circularProgressProps?: CircularProgressProps;
}

const PrimaryTable = (props: Props): ReactElement => {
  const theme = useTheme();
  const smallScreen = useMediaQuery(theme.breakpoints.down('lg'));

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      '& .MuiTableCell-root': {
        borderBottom: 'none',
        textAlign: 'center',
        padding: '10px 0',
      },
    },
    cell: {
      fontSize: smallScreen ? 10 : 14,
    },
    mainHeader: {
      backgroundColor: theme.palette?.background?.default,
    },
  }));

  const classes = useStyles(props);

  return (
    <>
      {props?.loading && props.loading ? (
        <Box sx={{ display: 'flex', width: '100%', justifyContent: 'center', mt: 2 }}>
          <CircularProgress {...props.circularProgressProps} />
        </Box>
      ) : (
        <>
          {props?.responsive ? (
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 600 }} className={classes.root}>
                <TableHead className={props?.mainHead ? classes.mainHeader : ''}>
                  <TableRow>
                    {props.headers.map((th, index) => (
                      <TableCell key={index}>{th}</TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>{props.children}</TableBody>
              </Table>
            </TableContainer>
          ) : (
            <Table className={classes.root}>
              <TableHead className={props?.mainHead ? classes.mainHeader : ''}>
                <TableRow>
                  {props.headers.map((th, index) => (
                    <TableCell key={index} className={classes.cell}>
                      {th}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>{props.children}</TableBody>
            </Table>
          )}
          {props.countPage && (
            <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', mt: 2 }}>
              <DefaultPagination
                count={props?.countPage}
                selected={props?.selectPage || 1}
                paginationProps={props.paginationProps}
              />
            </Box>
          )}
        </>
      )}
    </>
  );
};

export default PrimaryTable;
