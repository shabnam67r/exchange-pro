import { ReactElement } from 'react';
import { Box, BoxProps } from '@mui/system';
import { Line } from 'react-chartjs-2';
import { ChartOptions, DataChart } from '../../interfaces/chart';

interface Props {
  componentProps?: BoxProps;
  chartData: DataChart;
  options: ChartOptions;
}

const BaseLineChart = (props: Props): ReactElement => {
  return (
    <Box {...props?.componentProps}>
      <Line data={props?.chartData} options={props?.options} />
    </Box>
  );
};

export default BaseLineChart;
