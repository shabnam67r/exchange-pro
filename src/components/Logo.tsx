import { Theme, Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { SxProps } from '@mui/system';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
/******************************************************************************
 *                        Props Interface definition
 *****************************************************************************/
interface Props {
  idKey?: string;
  full?: boolean; // if true --> Logo with Text
  width?: number;
  height?: number;
  type?: 'primary' | 'secondary';
  ref?: React.Ref<unknown>;
  sx?: SxProps<Theme>;
}

/******************************************************************************
 *                            Style definition
 *****************************************************************************/
const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
  root: {},
}));

/******************************************************************************
 *                           Component definition
 *****************************************************************************/
const Logo: React.FC<Props> = (props) => {
  const classes = useStyles(props);

  const primaryImage: string = '/static/images/logo.svg';
  const secondaryImage: string = '/static/images/secondary-logo.svg';
  const primaryImageFull: string = '/static/images/logo-full.svg';
  const secondaryImageFull: string = '/static/images/secondary-logo-full.svg';

  const [image, setImage] = useState(primaryImageFull);

  useEffect(() => {
    let tempImage: string;
    if (props.type === 'primary') {
      if (props.full) {
        tempImage = primaryImageFull;
      } else tempImage = primaryImage;
    } else {
      if (props.full) {
        tempImage = secondaryImageFull;
      } else tempImage = secondaryImage;
    }
    setImage(tempImage);
  }, []);

  return (
    <Box ref={props.ref} sx={{ maxHeight: props.height, ...props.sx }} className={classes.root} key={props.idKey}>
      <Image
        width={props.width && props.width > 90 ? props.width : 90}
        height={props.height && props.height > 48 ? props.height : 48}
        src={image}
        alt="logo"
      />
    </Box>
  );
};

/******************************************************************************
 *                             Default values of Props
 *****************************************************************************/
Logo.defaultProps = {
  idKey: uuidv4(),
  type: 'primary',
};
export default Logo;
