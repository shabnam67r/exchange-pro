import React, { ReactElement, useEffect, useState } from 'react';
import ReactMarkdown from 'react-markdown';
import { Box } from '@mui/system';

interface Props {
  data: string;
}

export default function MarkDownViewer({ data }: Props): ReactElement {
  const [mdTexts, setMdTexts] = useState<any>();

  useEffect(() => {
    fetch(data)
      .then((response) => {
        if (response.ok) return response.text();
        else return Promise.reject("Didn't fetch text correctly");
      })
      .then((text) => {
        setMdTexts(text);
      })
      .catch((error) => console.warn(error));
  }, [data]);

  return (
    <Box sx={{ overflow: 'auto' }}>
      <ReactMarkdown>{mdTexts}</ReactMarkdown>
    </Box>
  );
}
