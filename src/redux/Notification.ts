import { createSlice } from '@reduxjs/toolkit';
//  notification setting for snackbar provider and it messages
import { MessageNotificationInterface, NotificationInterface } from 'src/interfaces/notification ';
import { v4 } from 'uuid';
import { AppDispatch } from './store';

const initialState: NotificationInterface = {
  messages: [],
};

const slice = createSlice({
  name: 'notifications',
  initialState,
  reducers: {
    onOpen(state: NotificationInterface, data) {
      // this fn will remove old message if length go over 5
      if (state.messages.length >= 5) {
        state.messages = state.messages.slice(1, 4);
      }
      state.messages.push({ message: data.payload.message, id: v4(), open: true });
    },
    onClose(state: NotificationInterface, data) {
      const newMessage = state.messages.filter((item) => item.id != data.payload);

      state.messages = newMessage;
    },
  },
});

export const { reducer } = slice;

export const onClose = (id: string) => (dispatch: AppDispatch) => {
  dispatch(slice.actions.onClose(id));
};
export const onOpen = (data: MessageNotificationInterface) => (dispatch: AppDispatch) => {
  dispatch(slice.actions.onOpen(data));
};

export default slice;
