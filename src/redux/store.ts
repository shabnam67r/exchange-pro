import { Action, configureStore, getDefaultMiddleware, ThunkAction } from '@reduxjs/toolkit';
import { Context, createWrapper, MakeStore } from 'next-redux-wrapper';
import { FLUSH, PAUSE, PERSIST, persistReducer, persistStore, PURGE, REGISTER, REHYDRATE } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { encryptTransform } from 'redux-persist-transform-encrypt';

import rootReducer from '.';

const persistConfig = {
  key: 'root',
  version: 1,
  storage,
  transforms: [
    encryptTransform({
      secretKey: 'my-super-secret-key',
      onError: (error: any) => {
        // tslint:disable-next-line: no-console
        console.log(error);

        storage.removeItem('persist:root');
      },
    }),
  ],
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  devTools: true,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  }),
});

(store as any).__persistor = persistStore(store); // Nasty hack

// create a makeStore function
// const makeStore: MakeStore<RootState> = (context: Context) => store;
const makeStore = (context: Context) => store;
export type RootState = ReturnType<typeof store.getState>;
// export an assembled wrapper
export const wrapperRedux = createWrapper(makeStore, { debug: true });

export type AppDispatch = typeof store.dispatch;

export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;
