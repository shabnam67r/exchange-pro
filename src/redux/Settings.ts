//  general setting such as theme switch
import { createSlice } from '@reduxjs/toolkit';
import { SettingInterface } from 'src/interfaces/general';
import { AppDispatch } from './store';

const initialState: SettingInterface = {
  theme: 'DARK',
};

const slice = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    changeTheme(state: SettingInterface) {
      state.theme = state.theme === 'DARK' ? 'LIGHT' : 'DARK';
    },
  },
});

export const { reducer } = slice;

export const changeTheme = () => (dispatch: AppDispatch) => {
  dispatch(slice.actions.changeTheme());
};

export default slice;
