import { createSlice } from '@reduxjs/toolkit';
import { IsMobileModeInterface } from 'src/interfaces/notification ';
import { AppDispatch } from './store';

const initialState: IsMobileModeInterface = {
  mobileMode: false,
};

const slice = createSlice({
  name: 'mobileMode',
  initialState,
  reducers: {
    onMobileMode(state: IsMobileModeInterface, data) {
      state.mobileMode = data?.payload;
    },
  },
});

export const { reducer } = slice;

export const isMobileMode = (data: {}) => (dispatch: AppDispatch) => {
  dispatch(slice.actions.onMobileMode(data));
};

export default slice;
