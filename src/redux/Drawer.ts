import { createSlice } from '@reduxjs/toolkit';
import { IsDrawerOpenInterface } from 'src/interfaces/notification ';
import { AppDispatch } from './store';

const initialState: IsDrawerOpenInterface = {
  openDrawer: false,
};

const slice = createSlice({
  name: 'drawer',
  initialState,
  reducers: {
    isDrawerOpen(state: IsDrawerOpenInterface, data) {
      state.openDrawer = data?.payload;
    },
  },
});

export const { reducer } = slice;

export const supportDrawerOpen = (data: {}) => (dispatch: AppDispatch) => {
  dispatch(slice.actions.isDrawerOpen(data));
};

export default slice;
