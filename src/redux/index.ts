import { reducer as mobileModeReducer } from './MobileMode';
import { reducer as settingsReducer } from './Settings';
import { reducer as notificationReducer } from './Notification';
import { reducer as drawerReducer } from './Drawer';
import { combineReducers } from '@reduxjs/toolkit';
import { useDispatch as useReduxDispatch, useSelector as useReduxSelector } from 'react-redux';

const rootReducer = combineReducers({
  settings: settingsReducer,
  notification: notificationReducer,
  mobileMode: mobileModeReducer,
  drawer: drawerReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;

export const useSelector = useReduxSelector;

export const useDispatch = () => useReduxDispatch();
