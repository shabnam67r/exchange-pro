import React, { ReactElement } from 'react';
import { Divider, Grid, useMediaQuery } from '@mui/material';
import CoinInfoViewCard from '../components/cards/CoinInfoViewCard';
import { useTheme } from '@mui/system';

interface Props {}

const CoinsInfoDetail = (props: Props): ReactElement => {
  const theme = useTheme();
  const media = useMediaQuery(theme.breakpoints.up('md'));

  /******************************************************************************
   *                            Initial Data
   *****************************************************************************/

  const initialData = {
    stepOne: [
      { color: '#EF4DB6', amount: 1.2, buyPrice: 1123456789, sellPrice: 1123456789, label: 'ربع سکه بهار آزادی' },
      { color: '#1AD5FD', amount: -1.2, buyPrice: 1123456789, sellPrice: 1123456789, label: 'ربع سکه بهار آزادی' },
      { color: '#87FC70', amount: 1.2, buyPrice: 1123456789, sellPrice: 1123456789, label: 'ربع سکه بهار آزادی' },
    ],
  };
  return (
    <>
      <Grid container columnSpacing={2} rowSpacing={{ xs: 3, md: 0 }} justifyContent="space-between">
        {initialData.stepOne.map((item, index) => (
          <React.Fragment key={index}>
            {/*@ts-ignore*/}
            <Grid item xs={12} md={3.9}>
              <CoinInfoViewCard data={item} />
            </Grid>
            {initialData.stepOne.length - 1 !== index && (
              <Divider
                orientation={media ? 'vertical' : 'horizontal'}
                sx={{ width: !media ? '100%' : 0, my: 2 }}
                flexItem={media}
              />
            )}
          </React.Fragment>
        ))}
      </Grid>
    </>
  );
};

export default CoinsInfoDetail;
