import React, { ReactElement, useState } from 'react';
import { Button, Card, CardActions, Grid, Typography } from '@mui/material';
import { Box } from '@mui/system';
import Link from 'next/link';
import { Buttons } from '../interfaces/translate';
import useTranslate from '../hooks/useTranslate';
import SlideOfMarketsPageCard from '../components/cards/SlideOfMarketsPageCard';

interface Props {}

const ChoicesTradeSection = (props: Props): ReactElement => {
  const transition = useTranslate();
  const buttons: Buttons = transition('webSiteConstant.buttons');
  const [selected, setSelected] = useState('ربع سکه بهار آزادی');

  // initial list
  const initialList = {
    titleItems: [
      { title: 'بیشترین', value: '100,000' },
      { title: 'کمترین', value: '100,000' },
    ],
    cards: [
      { amount: 1.2, label: 'نیم سکه بهار آزادی' },
      { amount: -1.2, label: 'ربع سکه بهار آزادی' },
      { amount: 1.2, label: 'سکه بهار آزادی' },
    ],
  };

  const handleSelected = (id: string) => setSelected(id);

  return (
    <Card sx={{ p: { xs: 0, md: 'default' } }}>
      <Grid
        container
        item
        xs={12}
        md={10}
        columnSpacing={2}
        rowSpacing={{ xs: 3, md: 0 }}
        justifyContent="space-between"
        sx={{ mx: 'auto' }}
      >
        {initialList.cards.map((item, index) => (
          <Grid item xs={12} md={4} key={index}>
            <Box mt={{ md: 1 }} onClick={() => handleSelected(item.label)}>
              <SlideOfMarketsPageCard data={item} selected={selected === item.label} />
            </Box>
          </Grid>
        ))}
      </Grid>

      <Box px={{ xs: 0, md: 10 }} my={8}>
        <Box sx={{ backgroundColor: (theme) => theme?.palette?.background?.default, my: 4 }}>
          <Grid container rowSpacing={{ xs: 2, md: 0 }} justifyContent="space-around">
            {initialList.titleItems.map((val, index) => (
              <Grid item xs={12} md={3} sx={{ display: 'flex', justifyContent: 'center', py: 2 }} key={index}>
                <Typography variant="caption" sx={{ color: (theme) => theme?.palette?.text?.secondary, mx: 1 }}>
                  {val.title}
                </Typography>
                <Typography variant="body2">{val.value} تومان</Typography>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Box>
      <CardActions sx={{ '& a': { textDecoration: 'none' }, display: 'flex', justifyContent: 'center' }}>
        <Link href="/dashboard/trade">
          <a>
            <Button sx={{ border: 'none', px: 8, py: 2 }}>{buttons.startTrading}</Button>
          </a>
        </Link>
      </CardActions>
    </Card>
  );
};

export default ChoicesTradeSection;
