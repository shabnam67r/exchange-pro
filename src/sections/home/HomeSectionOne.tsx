import React, { ReactElement } from 'react';
import { makeStyles } from '@mui/styles';
import { CardMedia, Chip, Grid, Theme, Typography, useMediaQuery } from '@mui/material';
import { Box, useTheme } from '@mui/system';
import useTranslate from '../../hooks/useTranslate';
import { Buttons, Inputs, Intro } from '../../interfaces/translate';
import CustomTextField from '../../components/forms/TextField';

interface Props {}

const HomeSectionOne = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const homeSectionOnePage: Intro = transition('webSiteConstant.homePage.intro');
  const buttons: Buttons = transition('webSiteConstant.buttons');
  const inputsText: Inputs = transition('webSiteConstant.inputs');
  const mobileMode = useMediaQuery(theme.breakpoints.down('md'));

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      position: 'relative',
    },
    background: {
      height: 400,
      width: '100%',
      background: 'url(/static/images/default/backgroundLayout.png) no-repeat',
      position: 'absolute',
      bottom: '-15%',
      left: 0,
      zIndex: -1,
    },
    textBox: {
      display: 'flex',
      alignItems: 'start',
      flexDirection: 'column',
      padding: mobileMode ? theme.spacing(5, 2, 5, 2) : theme.spacing(15, 14, 5, 14),
    },
  }));

  const classes = useStyles(props);

  return (
    <Box className={classes.root}>
      <Box>
        <Grid container direction={{ xs: 'column-reverse', md: 'row' }}>
          <Grid item xs={12} md={6} className={classes.textBox}>
            <Typography variant="h1" sx={{ mb: 4 }}>
              {homeSectionOnePage.header1}
            </Typography>
            <Typography variant="subtitle1" sx={{ mb: 2 }}>
              {homeSectionOnePage.header2}
            </Typography>
            <Typography
              variant="body2"
              sx={{ mb: 8, color: (theme) => theme.palette?.text?.secondary, lineHeight: 1.6 }}
            >
              {homeSectionOnePage.desc}
            </Typography>
            <CustomTextField
              placeholder={inputsText.phoneNumber}
              label=""
              InputProps={{
                endAdornment: (
                  <Chip
                    color="primary"
                    sx={{ p: 1, ml: 1 }}
                    clickable
                    label={
                      <Typography variant="caption" sx={{ display: 'flex', alignItems: 'center' }}>
                        {buttons.startTrades}
                      </Typography>
                    }
                  />
                ),
              }}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <CardMedia
              component="img"
              sx={{ width: '100%', height: '100%' }}
              src="/static/images/default/picture head.png"
              alt={homeSectionOnePage.imageAlt}
            />
          </Grid>
        </Grid>
      </Box>
      <Box className={classes.background} />
    </Box>
  );
};

export default HomeSectionOne;
