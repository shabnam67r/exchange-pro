import React, { ReactElement } from 'react';
import { Box, useTheme } from '@mui/system';
import { Card, Theme, Typography, useMediaQuery } from '@mui/material';
import { makeStyles } from '@mui/styles';
import useTranslate from '../../hooks/useTranslate';
import { CoinPriceOfHome } from '../../interfaces/translate';
import CoinsInfoDetail from '../CoinsInfoDetail';

interface Props {}

const HomeSectionTwo = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const homeSectionTwoPage: CoinPriceOfHome = transition('webSiteConstant.homePage.coinPrice');
  const desktop = useMediaQuery(theme.breakpoints.up('md'));

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      textAlign: 'center',
      transform: desktop ? 'translateY(-10%)' : '',
      marginBottom: theme.spacing(10),
    },
    card: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      '& > div': {
        width: '100%',
        border: `2px solid ${theme.palette.divider}`,
      },
    },
  }));

  const classes = useStyles(props);

  return (
    <Box className={classes.root}>
      <Typography variant="h2" sx={{ mb: 4 }}>
        {homeSectionTwoPage.header}
      </Typography>
      <Box className={classes.card}>
        <Card>
          <CoinsInfoDetail />
        </Card>
      </Box>
    </Box>
  );
};

export default HomeSectionTwo;
