import React, { ReactElement } from 'react';
import { Box } from '@mui/system';
import { Button, Theme, Typography } from '@mui/material';
import useTranslate from '../../hooks/useTranslate';
import { Buttons, FeatureOfHome } from '../../interfaces/translate';
import CoinCards from '../../components/cards/CoinCards';
import { makeStyles } from '@mui/styles';

interface Props {}

const HomeSectionThree = (props: Props): ReactElement => {
  const transition = useTranslate();
  const homeSectionThreePage: FeatureOfHome = transition('webSiteConstant.homePage.feature');
  const buttons: Buttons = transition('webSiteConstant.buttons');

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    title: {
      textAlign: 'center',
      marginBottom: theme.spacing(5),
    },
  }));

  const classes = useStyles(props);

  return (
    <Box>
      <Box className={classes.title}>
        <Typography variant="h2" sx={{ mb: 2 }}>
          {homeSectionThreePage.header}
        </Typography>
        <Typography variant="body2" sx={{ mb: 2, color: (theme) => theme.palette?.text?.secondary }}>
          {homeSectionThreePage.desc}
        </Typography>
      </Box>
      <Box>
        <CoinCards />
      </Box>
      <Box sx={{ textAlign: 'center', my: 5 }}>
        <Button sx={{ border: 'none', px: 8, py: 2 }}>{buttons.startNow}</Button>
      </Box>
    </Box>
  );
};

export default HomeSectionThree;
