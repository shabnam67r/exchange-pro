import React, { ReactElement } from 'react';
import { Box } from '@mui/system';
import { Grid, Theme, Typography } from '@mui/material';
import useTranslate from '../../hooks/useTranslate';
import { StatisticsOfHome } from '../../interfaces/translate';
import { makeStyles } from '@mui/styles';
import ResetIcon from '../../icons/someIcons/ResetIcon';
import UserCounterIcon from '../../icons/someIcons/UserCounterIcon';
import CounterInfo from '../../components/home/CounterOfUserNumber';

interface Props {}

const HomeSectionFour = (props: Props): ReactElement => {
  const transition = useTranslate();
  const homeSectionFourPage: StatisticsOfHome = transition('webSiteConstant.homePage.statistics');

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    title: {
      textAlign: 'center',
      marginBottom: theme.spacing(5),
    },
  }));

  const classes = useStyles(props);

  return (
    <Box mt={10} mb={5}>
      <Box className={classes.title}>
        <Typography variant="h2" sx={{ mb: 2 }}>
          {homeSectionFourPage.header}
        </Typography>
        <Typography variant="body2" sx={{ mb: 2, color: (theme) => theme.palette?.text?.secondary }}>
          {homeSectionFourPage.desc}
        </Typography>
      </Box>
      <Box mt={8}>
        <Grid container justifyContent="space-between" rowSpacing={{ xs: 8, md: 0 }}>
          <Grid item xs={12} md={5}>
            <CounterInfo icon={<ResetIcon />} title={homeSectionFourPage.cards[0]?.title} />
          </Grid>
          <Grid item xs={12} md={5}>
            <CounterInfo icon={<UserCounterIcon />} title={homeSectionFourPage.cards[1]?.title} />
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default HomeSectionFour;
