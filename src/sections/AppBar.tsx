import React, { lazy, Suspense, ReactElement, useEffect, useState } from 'react';
import { Toolbar, AppBar as Appbar, Container } from '@mui/material';
import DefaultHeader from '../components/headerComponents/DefaultHeader';
import DashboardHeader from '../components/headerComponents/DashboardHeader';
import { useRouter } from 'next/router';
import { supportDrawerOpen } from '../redux/Drawer';
import { useDispatch } from '../redux';

interface Props {}

export default function AppBar(props: Props): ReactElement {
  const router = useRouter();
  const dispatch = useDispatch();
  const isDashboard = router?.pathname?.includes('/dashboard');
  const [drawerOpen, setDrawerOpen] = useState(false);

  const DrawerComponent = drawerOpen
    ? lazy(() => import(`../components/drawer/${isDashboard ? 'DashboardDrawer' : 'HomeDrawer'}`))
    : null;

  const handleDrawer = () => setDrawerOpen(!drawerOpen);

  useEffect(() => {
    dispatch(supportDrawerOpen(drawerOpen));
  }, [drawerOpen]);

  return drawerOpen ? (
    <Suspense fallback="">
      {/*// @ts-ignore*/}
      <DrawerComponent open={drawerOpen} setOpen={setDrawerOpen} />
    </Suspense>
  ) : (
    <Appbar position="fixed" sx={{ py: 0, zIndex: (theme) => theme.zIndex.drawer + 1 }}>
      <Container maxWidth="xl" sx={{ px: '0px !important', my: 'auto' }}>
        <Toolbar
          variant="dense"
          sx={{
            p: 0,
          }}
        >
          {isDashboard ? (
            <DashboardHeader handleDrawer={handleDrawer} />
          ) : (
            <DefaultHeader handleDrawer={handleDrawer} />
          )}
        </Toolbar>
      </Container>
    </Appbar>
  );
}
