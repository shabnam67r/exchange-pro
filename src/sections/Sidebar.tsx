import React, { ReactElement } from 'react';
import { useMediaQuery } from '@mui/material';
import { useTheme } from '@mui/system';
import DashboardOutlinedIcon from '@mui/icons-material/DashboardOutlined';
import AccountBalanceWalletOutlinedIcon from '@mui/icons-material/AccountBalanceWalletOutlined';
import HistoryOutlinedIcon from '@mui/icons-material/HistoryOutlined';
import BorderColorOutlinedIcon from '@mui/icons-material/BorderColorOutlined';
import ContactPhoneOutlinedIcon from '@mui/icons-material/ContactPhoneOutlined';
import SidebarComponent from '../components/layout/SidebarComponent';
import NavigationButtonDashboardCard from '../components/cards/NavigationButtonDashboardCard';
import { useSelector } from '../redux';
import { IsDrawerOpenInterface } from '../interfaces/notification ';

interface Props {}

const Sidebar = (props: Props): ReactElement => {
  const theme = useTheme();
  const tablet = useMediaQuery(theme.breakpoints.down('md'));
  const drawer = useSelector((state: { drawer: IsDrawerOpenInterface }) => state?.drawer?.openDrawer);

  // initial list
  const initialList = [
    {
      link: '/dashboard',
      icon: <DashboardOutlinedIcon />,
      text: 'صفحه اصلی',
      selected: true,
    },
    {
      link: '/dashboard/wallet',
      icon: <AccountBalanceWalletOutlinedIcon />,
      text: 'کیف پول',
    },
    {
      link: '/dashboard/orders',
      icon: <BorderColorOutlinedIcon />,
      text: 'سفارشات من',
    },
    {
      link: '/dashboard/history',
      icon: <HistoryOutlinedIcon />,
      text: 'تاریخچه من',
    },
    {
      link: '/dashboard/inviting',
      icon: <ContactPhoneOutlinedIcon />,
      text: 'دعوت دوستان',
    },
  ];

  return !tablet ? (
    <SidebarComponent data={initialList} />
  ) : !drawer ? (
    <NavigationButtonDashboardCard data={initialList} />
  ) : (
    <></>
  );
};

export default Sidebar;
