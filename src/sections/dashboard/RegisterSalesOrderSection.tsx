import React, { ReactElement } from 'react';
import { Card, CardContent, Grid, Typography } from '@mui/material';
import TitleDashboard from '../../components/smallComponents/dashboard/TitleDashboard';
import useTranslate from '../../hooks/useTranslate';
import { Inputs, OrderObj } from '../../interfaces/translate';
import { Box } from '@mui/system';
import DefaultSelect from '../../components/forms/DefaultSelect';
import CustomTextField from '../../components/forms/TextField';
import { coinSelectBoxList } from '../../feature/defaultInitialData';

interface Props {}

const RegisterSalesOrderSection = (props: Props): ReactElement => {
  const transition = useTranslate();
  const salesOrderPageText: OrderObj = transition('webSiteConstant.tradePage.sellOrder');
  const inputsText: Inputs = transition('webSiteConstant.inputs');

  return (
    <Card>
      <TitleDashboard title={salesOrderPageText.title} subTitle={salesOrderPageText.desc} />
      <CardContent sx={{ mt: 2 }}>
        <Box>
          <Grid container justifyContent="space-between" rowSpacing={{ xs: 2, md: 0 }}>
            <Grid item xs={12} md={6}>
              <Typography variant="body2" color={(theme) => theme.palette?.text?.secondary}>
                {`${salesOrderPageText.inventory} : 123,565,555`}
              </Typography>
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography variant="body2" color={(theme) => theme.palette?.text?.secondary}>
                {`${salesOrderPageText.lowestOffer} : 885,565,555`}
              </Typography>
            </Grid>
          </Grid>
        </Box>
        <Box mt={4}>
          <Grid container alignItems="center" rowSpacing={{ xs: 2, md: 0 }}>
            <Grid item xs={12} md={2}>
              <Typography variant="body2">{`${salesOrderPageText.bazar} :`}</Typography>
            </Grid>
            <Grid item xs={12} md={5}>
              <DefaultSelect items={coinSelectBoxList} handleChange={() => {}} pill />
            </Grid>
          </Grid>
        </Box>
        <Box mt={4}>
          <CustomTextField placeholder="برای مثال : 6 سکه" label={inputsText.coinQuantity} componentProps={{ mt: 2 }} />
          <CustomTextField placeholder="100,000,000" label={inputsText.unitPrice} componentProps={{ mt: 2 }} />
        </Box>
      </CardContent>
    </Card>
  );
};

export default RegisterSalesOrderSection;
