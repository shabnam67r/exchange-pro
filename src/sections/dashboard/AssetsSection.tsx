import React, { ReactElement } from 'react';
import { Box } from '@mui/system';
import { AssetsCardData } from '../../feature/defaultInitialData';
import AssetInfoCard from '../../components/cards/AssetInfoCard';
import { Grid, useMediaQuery } from '@mui/material';

interface Props {}

const AssetsSection = (props: Props): ReactElement => {
  const smallScreen = useMediaQuery('(max-width:1500px)');

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
      <Grid container item xs={12} spacing={2} justifyContent="center">
        {AssetsCardData.map((val, index) => (
          <Grid item xs={12} md={smallScreen ? 6 : 4}>
            <AssetInfoCard data={val} />
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

export default AssetsSection;
