import React, { ReactElement } from 'react';
import { Card, CardContent, Chip, TableCell, TableRow } from '@mui/material';
import TitleDashboard from '../../components/smallComponents/dashboard/TitleDashboard';
import useTranslate from '../../hooks/useTranslate';
import { DefaultObj } from '../../interfaces/translate';
import { PurchaseOrderObj } from '../../interfaces/totalData';
import PrimaryTable from '../../components/tables/PrimaryTable';
import { alpha } from '@mui/system';
import { handleColorStatus, handleTextStatus } from '../../feature/function';

interface Props {
  data: PurchaseOrderObj[] | undefined;
}

const PurchaseOrderSection = (props: Props): ReactElement => {
  const transition = useTranslate();
  const sellersPageText: DefaultObj = transition('webSiteConstant.tradePage.myOrders');

  return (
    <Card>
      <TitleDashboard title={sellersPageText.title} />
      <CardContent sx={{ mt: 2 }}>
        <PrimaryTable
          headers={sellersPageText?.table}
          loading={!props?.data?.length}
          countPage={12}
          selectPage={7}
          responsive
          mainHead
          circularProgressProps={{ size: 40 }}
        >
          {props?.data?.map((row: PurchaseOrderObj, index) => (
            <TableRow
              key={index}
              sx={{ '& td': { color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5), fontSize: 12 } }}
            >
              <TableCell>{row.depositType}</TableCell>
              <TableCell align="right">{row.depositAmount?.toLocaleString()}</TableCell>
              <TableCell align="right">{row.paymentCode}</TableCell>
              <TableCell align="right">{row.date}</TableCell>
              <TableCell align="right">
                <Chip
                  color={handleColorStatus(row.status)}
                  label={handleTextStatus(row.status)}
                  sx={{ color: (theme) => theme.palette.common?.white }}
                />
              </TableCell>
            </TableRow>
          ))}
        </PrimaryTable>
      </CardContent>
    </Card>
  );
};

export default PurchaseOrderSection;
