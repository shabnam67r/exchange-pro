import React, { ReactElement } from 'react';
import { Card, CardContent, TableCell, TableRow } from '@mui/material';
import TitleDashboard from '../../components/smallComponents/dashboard/TitleDashboard';
import useTranslate from '../../hooks/useTranslate';
import { DefaultObj } from '../../interfaces/translate';
import { TradeObj } from '../../interfaces/totalData';
import PrimaryTable from '../../components/tables/PrimaryTable';
import { alpha } from '@mui/system';

interface Props {
  data: TradeObj[] | undefined;
  primaryTableProps?: any;
}

const SellersSection = (props: Props): ReactElement => {
  const transition = useTranslate();
  const sellersPageText: DefaultObj = transition('webSiteConstant.tradePage.sellers');

  return (
    <Card>
      <TitleDashboard title={sellersPageText.title} color="#FF4444" />
      <CardContent sx={{ mt: 2 }}>
        <PrimaryTable
          headers={sellersPageText?.table}
          loading={!props?.data?.length}
          countPage={12}
          selectPage={7}
          circularProgressProps={{ size: 40 }}
          {...props.primaryTableProps}
        >
          {props?.data?.map((row: TradeObj, index) => (
            <TableRow
              key={index}
              sx={{ '& td': { color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5), fontSize: 12 } }}
            >
              <TableCell>{row.amount}</TableCell>
              <TableCell align="right">{row.unitPrice?.toLocaleString()}</TableCell>
              <TableCell align="right">{row.totalPrice?.toLocaleString()}</TableCell>
            </TableRow>
          ))}
        </PrimaryTable>
      </CardContent>
    </Card>
  );
};

export default SellersSection;
