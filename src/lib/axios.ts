import axios from 'axios';

const axiosInstance = axios.create();

axiosInstance.defaults.baseURL = 'http://192.168.178.105:3000/api/v1';
// axiosInstance.defaults.headers = {
//   "Access-Control-Allow-Origin": "*",
// };

axiosInstance.interceptors.response.use(
  (response: any) => response,
  (error: { response: { data: any; }; }) => {
    console.log('error', error);
    Promise.reject((error.response && error.response.data) || 'Something went wrong');
  },
);
export default axiosInstance;
