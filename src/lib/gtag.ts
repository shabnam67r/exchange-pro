import * as ReactGA from "react-ga";

export const initGA = (id: any) => {
  ReactGA.initialize(id);
};
