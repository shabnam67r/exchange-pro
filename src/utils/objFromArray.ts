const objFromArray = (arr: any[], key = 'id') =>
  arr.reduce((accumulator: { [x: string]: any }, current: { [x: string]: string | number }) => {
    accumulator[current[key]] = current;
    return accumulator;
  }, {});

export default objFromArray;
