import * as React from 'react';
import { useRouter } from 'next/router';
import i18nEN from 'src/translation/translation-en.json';
import i18nFA from 'src/translation/translation-fa.json';

export const translations: any = {
  fa: i18nFA,
  en: i18nEN,
};
export function getPath(obj: any, path: string): any {
  if (!path) {
    return null;
  }
  return path.split('.').reduce((acc, item) => (acc && acc[item] ? acc[item] : null), obj);
}
