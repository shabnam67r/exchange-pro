module.exports = {
  env: {},
  images: {},
  webpack5: true,
  i18n: {
    locales: ['fa'],
    defaultLocale: 'fa',
  },
  // async redirects() {
  //   return [
  //     {
  //       source: '/setting/user',
  //       destination: '/setting/user/info',
  //       permanent: false,
  //     },
  //   ];
  // },
};
