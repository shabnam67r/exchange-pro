import React, { ReactElement } from 'react';
import HomeLayout from '../src/layout/HomeLayout';
import InternalPagesLayout from '../src/layout/IntertalPagesLayout';
import { Box, useTheme } from '@mui/system';
import useTranslate from '../src/hooks/useTranslate';
import { InternalType } from '../src/interfaces/translate';
import { Typography } from '@mui/material';

interface Props {}

const Terms = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const userManualPage: InternalType = transition('webSiteConstant.termsPage');

  return (
    <HomeLayout>
      <InternalPagesLayout title={userManualPage.header}>
        <Box px={{ xs: theme.spacing(0), md: theme.spacing(15) }}>
          <Box>
            <Typography variant="body1" sx={{ mb: 1 }}>
              شرایط اساسی و اولیه‌ی استفاده کاربران از خدمات
            </Typography>
            <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و
              متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
              کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده،
              شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
              الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری
              موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و
              جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
            </Typography>
          </Box>
          <Box mt={4}>
            <Typography variant="body1" sx={{ mb: 1 }}>
              کاربر می پذیرد که:
            </Typography>
            <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و
              متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
              کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده،
              شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
              الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری
              موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و
              جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
            </Typography>
          </Box>
          <Box mt={4}>
            <Typography variant="body1" sx={{ mb: 1 }}>
              مدیریت حساب‌ها
            </Typography>
            <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و
              متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
              کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده،
              شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
              الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری
              موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و
              جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
            </Typography>
          </Box>
          <Box mt={4}>
            <Typography variant="body1" sx={{ mb: 1 }}>
              تایید نکردن حساب کاربری
            </Typography>
            <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و
              متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و
              کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده،
              شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی
              الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری
              موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و
              جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
            </Typography>
          </Box>
        </Box>
      </InternalPagesLayout>
    </HomeLayout>
  );
};

export default Terms;
