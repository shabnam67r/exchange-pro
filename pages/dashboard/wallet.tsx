import React, { ReactElement } from 'react';
import DashboardLayout from '../../src/layout/DashboardLayout';
import { Card, CardContent, Chip, Grid, TableCell, TableRow } from '@mui/material';
import TitleDashboard from '../../src/components/smallComponents/dashboard/TitleDashboard';
import useTranslate from '../../src/hooks/useTranslate';
import { WalletPage } from '../../src/interfaces/translate';
import RadioChip from '../../src/components/RadioChip';
import AssetsSection from '../../src/sections/dashboard/AssetsSection';
import { alpha } from '@mui/system';
import { handleColorStatus, handleTextStatus, validDisplayPrice } from '../../src/feature/function';
import PrimaryTable from '../../src/components/tables/PrimaryTable';
import { initialDataWalletPage } from '../../src/feature/defaultInitialData';

interface Props {}

const Wallet = (props: Props): ReactElement => {
  const transition = useTranslate();
  const walletPageText: WalletPage = transition('webSiteConstant.walletPage');

  const filterNode = <RadioChip filterList={walletPageText?.history?.sorts} />;

  return (
    <Grid container direction="column" rowSpacing={2}>
      {/* /// ASSETS SECTION ///  */}
      <Grid item sx={{ width: '100%' }}>
        <Card>
          <TitleDashboard title={walletPageText.wallet.title} />
          <CardContent>
            <AssetsSection />
          </CardContent>
        </Card>
      </Grid>

      {/* /// HISTORY WALLET ///  */}
      <Grid item sx={{ width: '100%' }}>
        <Card>
          <TitleDashboard title={walletPageText.history.title} filterOption={filterNode} />
          <CardContent>
            <PrimaryTable headers={walletPageText?.history?.table} mainHead responsive>
              {initialDataWalletPage.map((row, index) => (
                <TableRow
                  key={index}
                  sx={{ '& td': { color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5), fontSize: 12 } }}
                >
                  <TableCell>{row.depositType}</TableCell>
                  <TableCell align="right">{validDisplayPrice(row.amount)}</TableCell>
                  <TableCell align="right">{row.paymentCode}</TableCell>
                  <TableCell align="right">{row.creatAt}</TableCell>
                  <TableCell align="right">
                    <Chip
                      color={handleColorStatus(row.status)}
                      label={handleTextStatus(row.status)}
                      sx={{ color: (theme) => theme.palette.common?.white }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </PrimaryTable>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

Wallet.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export default Wallet;
