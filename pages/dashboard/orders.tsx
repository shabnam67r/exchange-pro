import React, { ReactElement } from 'react';
import DashboardLayout from '../../src/layout/DashboardLayout';
import { Card, CardContent, Chip, Grid, TableCell, TableRow } from '@mui/material';
import TitleDashboard from '../../src/components/smallComponents/dashboard/TitleDashboard';
import PrimaryTable from '../../src/components/tables/PrimaryTable';
import { alpha } from '@mui/system';
import { handleColorStatus, handleTextStatus, validDisplayPrice } from '../../src/feature/function';
import useTranslate from '../../src/hooks/useTranslate';
import { MyOrdersPage } from '../../src/interfaces/translate';
import RadioChip from '../../src/components/RadioChip';
import { GetStaticProps } from 'next';
import { getPurchaseList } from '../../src/networks/tradeApi';
import { PurchaseOrderObj } from '../../src/interfaces/totalData';
import { REVALIDATE_TIME } from '../../src/constants/env';

interface Props {
  data?: PurchaseOrderObj[];
}

const Orders = (props: Props): ReactElement => {
  const transition = useTranslate();
  const myOrdersPageText: MyOrdersPage = transition('webSiteConstant.myOrdersPage');

  const filterNode = <RadioChip filterList={myOrdersPageText?.sorts} />;

  return (
    <Grid container direction="column" rowSpacing={2}>
      {/* /// LIST ///  */}
      <Grid item sx={{ width: '100%' }}>
        <Card>
          <TitleDashboard title={myOrdersPageText.title} filterOption={filterNode} />
          <CardContent>
            <PrimaryTable headers={myOrdersPageText?.table} mainHead responsive countPage={12} selectPage={7}>
              {props?.data?.map((row: PurchaseOrderObj, index) => (
                <TableRow
                  key={index}
                  sx={{ '& td': { color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5), fontSize: 12 } }}
                >
                  <TableCell>{row.depositType}</TableCell>
                  <TableCell align="right">{validDisplayPrice(row.depositAmount)}</TableCell>
                  <TableCell align="right">{row.paymentCode}</TableCell>
                  <TableCell align="right">{row.date}</TableCell>
                  <TableCell align="right">
                    <Chip
                      color={handleColorStatus(row.status)}
                      label={handleTextStatus(row.status)}
                      sx={{ color: (theme) => theme.palette.common?.white }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </PrimaryTable>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

/******************************************************************************
 *                            FETCH DATA FROM SSG
 *****************************************************************************/

export const getStaticProps: GetStaticProps = async () => {
  let data = await getPurchaseList();
  return {
    props: {
      data,
    },
    revalidate: REVALIDATE_TIME,
  };
};

/******************************************************************************
 *                            DASHBOARD LAYOUT
 *****************************************************************************/

Orders.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export default Orders;
