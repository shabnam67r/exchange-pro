import React, { ReactElement } from 'react';
import DashboardLayout from '../../src/layout/DashboardLayout';
import { Button, Card, CardContent, FormControlLabel, Grid, Radio, RadioGroup, Typography } from '@mui/material';
import TitleDashboard from '../../src/components/smallComponents/dashboard/TitleDashboard';
import { Box, Theme } from '@mui/system';
import useTranslate from '../../src/hooks/useTranslate';
import { Buttons, Inputs, SettingsPage } from '../../src/interfaces/translate';
import CustomTextField from '../../src/components/forms/TextField';
import ParagraphOfList from '../../src/components/smallComponents/ParagraphOfList';
import Link from 'next/link';

interface Props {}

const Settings = (props: Props): ReactElement => {
  const transition = useTranslate();
  const settingsPageText: SettingsPage = transition('webSiteConstant.settingsPage');
  const inputsText: Inputs = transition('webSiteConstant.inputs');
  const buttons: Buttons = transition('webSiteConstant.buttons');

  return (
    <Grid container direction="column" rowSpacing={2}>
      <Grid item sx={{ width: '100%' }}>
        <Card>
          <TitleDashboard title={settingsPageText.title} />
          <CardContent>
            <Box mt={4}>
              {/*  TWO STEP VERIFICATION SECTION  */}
              <Card sx={{ backgroundColor: (theme) => theme.palette?.common?.card, my: 2 }}>
                <TitleDashboard
                  title={settingsPageText?.twoStepVerification?.title}
                  color="#B52EED"
                  titleTypographyProps={{ fontSize: 16 }}
                />
                <CardContent sx={{ px: { xs: 0, md: 10 }, mt: 2 }}>
                  <Box sx={{ textAlign: 'center' }}>
                    <Typography variant="body1">{settingsPageText?.twoStepVerification?.desc}</Typography>
                    <Box>
                      <RadioGroup
                        aria-label="gender"
                        defaultValue={settingsPageText.twoStepVerification.radioButtons[0]}
                        name="radio-buttons-group"
                        sx={{ flexDirection: 'row', justifyContent: 'center', mt: 2 }}
                      >
                        {settingsPageText.twoStepVerification.radioButtons.map((val, index) => (
                          <FormControlLabel
                            key={index}
                            value={val}
                            control={<Radio />}
                            label={val}
                            sx={{ color: (theme) => theme.palette.text.secondary }}
                          />
                        ))}
                      </RadioGroup>
                    </Box>
                  </Box>
                </CardContent>
              </Card>

              {/*  CHANGE PASSWORD SECTION  */}
              <Card sx={{ backgroundColor: (theme) => theme.palette?.common?.card, my: 2 }}>
                <TitleDashboard
                  title={settingsPageText?.resetPassword?.title}
                  color="#B52EED"
                  titleTypographyProps={{ fontSize: 16 }}
                />
                <CardContent sx={{ px: { xs: 0, md: 10 }, mt: 2 }}>
                  <Box>
                    <ParagraphOfList
                      text={settingsPageText.resetPassword.desc1}
                      typographyProps={{ color: (theme: Theme) => theme.palette.text.secondary }}
                    />
                    <ParagraphOfList
                      text={settingsPageText.resetPassword.desc2}
                      typographyProps={{ color: (theme: Theme) => theme.palette.text.secondary }}
                    />
                  </Box>
                  <Box mt={3}>
                    <Grid container rowSpacing={{ xs: 2, md: 0 }} columnSpacing={2}>
                      <Grid item xs={12} md={4}>
                        <CustomTextField placeholder="********" label={inputsText.oldPassword} type="password" />
                      </Grid>
                      <Grid item xs={12} md={4}>
                        <CustomTextField placeholder="********" label={inputsText.newPassword} type="password" />
                      </Grid>
                      <Grid item xs={12} md={4}>
                        <CustomTextField placeholder="********" label={inputsText.repeatNewPassword} type="password" />
                      </Grid>
                      <Grid item xs={12} sx={{ textAlign: 'center', '& a': { textDecoration: 'none' } }}>
                        <Link href="/otp">
                          <a>
                            <Button variant="outlined">{buttons.changePassword}</Button>
                          </a>
                        </Link>
                      </Grid>
                    </Grid>
                  </Box>
                </CardContent>
              </Card>
            </Box>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

Settings.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export default Settings;
