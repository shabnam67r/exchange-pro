import React, { ReactElement } from 'react';
import DashboardLayout from '../../src/layout/DashboardLayout';
import { Card, CardContent, Chip, Grid, Stack, Typography, useMediaQuery } from '@mui/material';
import TitleDashboard from '../../src/components/smallComponents/dashboard/TitleDashboard';
import useTranslate from '../../src/hooks/useTranslate';
import { Buttons, InvitePage } from '../../src/interfaces/translate';
import { Box, useTheme } from '@mui/system';
import FriendShip from '../../src/icons/FriendShip';
import Diagram from '../../src/icons/Diagram';
import CustomTextField from '../../src/components/forms/TextField';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';

interface Props {}

const Inviting = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const media = useMediaQuery(theme.breakpoints.up('md'));
  const invitePagePageText: InvitePage = transition('webSiteConstant.invitePage');
  const buttons: Buttons = transition('webSiteConstant.buttons');

  return (
    <Grid container direction="column" rowSpacing={2}>
      {/* /// INVITE FRIEND ///  */}
      <Grid item>
        <Card sx={{ width: '100%' }}>
          <TitleDashboard title={invitePagePageText?.invite.title} />
          <CardContent>
            <Box mt={3}>
              <Typography variant="body2" sx={{ pr: 1, pb: 2 }} color={(theme) => theme.palette.text.secondary}>
                {invitePagePageText?.invite.desc}
              </Typography>
            </Box>
            <Box sx={{ mt: 2 }}>
              <Grid container columnSpacing={2} rowSpacing={{ xs: 2, md: 0 }}>
                <Grid item xs={12} md={5}>
                  <CustomTextField
                    placeholder="www.zarinex.ir"
                    label="کد دعوت شما"
                    InputProps={{
                      readOnly: true,
                      endAdornment: (
                        <Chip
                          color="primary"
                          sx={{ p: 1, ml: 1 }}
                          clickable
                          label={
                            <Typography variant="caption" sx={{ display: 'flex', alignItems: 'center' }}>
                              <ContentCopyIcon sx={{ ml: 0.5 }} />
                              {media && buttons.copy}
                            </Typography>
                          }
                        />
                      ),
                    }}
                  />
                </Grid>
                <Grid item xs={12} md={5}>
                  <CustomTextField
                    placeholder="www.zarinex.ir"
                    label="لینک دعوت شما"
                    InputProps={{
                      readOnly: true,
                      endAdornment: (
                        <Chip
                          color="primary"
                          sx={{ p: 1, ml: 1 }}
                          clickable
                          label={
                            <Typography variant="caption" sx={{ display: 'flex', alignItems: 'center' }}>
                              <ContentCopyIcon sx={{ ml: 0.5 }} />
                              {media && buttons.copy}
                            </Typography>
                          }
                        />
                      ),
                    }}
                  />
                </Grid>
              </Grid>
            </Box>
          </CardContent>
        </Card>
      </Grid>
      <Grid container item columnSpacing={2} rowSpacing={{ xs: 2, md: 0 }}>
        {/* /// NUMBER OF FRIENDS ///  */}
        <Grid item xs={12} md={6}>
          <Card sx={{ width: '100%' }}>
            <TitleDashboard title={invitePagePageText?.friends.title} />
            <CardContent>
              <Stack alignItems="center" justifyContent="center" mt={2}>
                <Box>
                  <FriendShip color={theme.palette.text.primary} />
                </Box>
                <Box mt={2}>
                  <Typography variant="body2" sx={{ pr: 1, pb: 2 }} color={(theme) => theme.palette.text.secondary}>
                    0 {invitePagePageText?.friends.unit}
                  </Typography>
                </Box>
              </Stack>
            </CardContent>
          </Card>
        </Grid>

        {/* /// PROFIT RATE ///  */}
        <Grid item xs={12} md={6}>
          <Card sx={{ width: '100%' }}>
            <TitleDashboard title={invitePagePageText?.profit.title} />
            <CardContent>
              <Stack alignItems="center" justifyContent="center" mt={2}>
                <Box>
                  <Diagram color={theme.palette.text.primary} />
                </Box>
                <Box mt={2}>
                  <Typography variant="body2" sx={{ pr: 1, pb: 2 }} color={(theme) => theme.palette.text.secondary}>
                    0 {invitePagePageText?.profit.unit}
                  </Typography>
                </Box>
              </Stack>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Grid>
  );
};

Inviting.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export default Inviting;
