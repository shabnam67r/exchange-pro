import React, { ReactElement, useEffect, useState } from 'react';
import DashboardLayout from '../../src/layout/DashboardLayout';
import { Card, CardContent, Grid, Typography } from '@mui/material';
import TitleDashboard from '../../src/components/smallComponents/dashboard/TitleDashboard';
import useTranslate from '../../src/hooks/useTranslate';
import { ShopPage } from '../../src/interfaces/translate';
import RadioChip from '../../src/components/RadioChip';
import { Box, useTheme } from '@mui/system';
import RegisterPurchaseOrderSection from '../../src/sections/dashboard/RegisterPurchaseOrderSection';
import RegisterSalesOrderSection from '../../src/sections/dashboard/RegisterSalesOrderSection';
import SellersSection from '../../src/sections/dashboard/SellersSection';
import BuyersSection from '../../src/sections/dashboard/BuyersSection';
import PurchaseOrderSection from '../../src/sections/dashboard/PurchaseOrderSection';
import { PurchaseOrderObj, TradeObj } from '../../src/interfaces/totalData';
import { getBuyersList, getPurchaseList, getSellersList } from '../../src/networks/tradeApi';
import TradingView from '../../src/components/charts/TradingView';

interface Props {}

const Trade = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const shopPageText: ShopPage = transition('webSiteConstant.shopPage');
  const [sellerList, setSellerList] = useState<TradeObj[]>();
  const [buyerList, setBuyerList] = useState<TradeObj[]>();
  const [purchaseList, setPurchaseList] = useState<PurchaseOrderObj[]>();

  // initial list
  const initialList = {
    filterList: [shopPageText?.mySite, shopPageText.wordWide],
    titleItems: [
      shopPageText.hightestPrice,
      shopPageText.lowestPrice,
      shopPageText.latest,
      shopPageText.transactionVolume,
    ],
  };

  const filterNode = <RadioChip filterList={initialList.filterList} />;

  /******************************************************************************
   *                            FETCHING DATA
   *****************************************************************************/

  const getSellerData = async () => {
    let list = await getSellersList();
    return setSellerList(list);
  };

  const getBuyerData = async () => {
    let list = await getBuyersList();
    return setBuyerList(list);
  };

  const getPurchaseData = async () => {
    let list = await getPurchaseList();
    return setPurchaseList(list);
  };

  useEffect(() => {
    getSellerData();
    getBuyerData();
    getPurchaseData();
  }, []);

  return (
    <Grid container columnSpacing={{ xs: 0, md: 2 }} rowSpacing={{ xs: 2, md: 0 }}>
      <Grid item xs={12}>
        <Card>
          <TitleDashboard title={`${shopPageText.title}سکه آزادی`} filterOption={filterNode} />
          <CardContent>
            <Box sx={{ backgroundColor: (theme) => theme?.palette?.background?.default, my: 4 }}>
              <Grid container rowSpacing={{ xs: 2, md: 0 }}>
                {initialList.titleItems.map((val: string, index) => (
                  <Grid item xs={12} md={3} sx={{ display: 'flex', justifyContent: 'center', py: 2 }} key={index}>
                    <Typography variant="caption" sx={{ color: (theme) => theme?.palette?.text?.secondary, mx: 1 }}>
                      {val}
                    </Typography>
                    <Typography variant="body2">100000000 تومان</Typography>
                  </Grid>
                ))}
              </Grid>
            </Box>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                height: 600,
                '& > article': {
                  width: '100%',
                  height: '100%',
                  '& > div > div': { height: '100% !important' },
                  '& > div': { height: '100% !important' },
                },
              }}
            >
              <TradingView theme={theme?.palette?.mode} />
            </Box>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <RegisterPurchaseOrderSection />
      </Grid>
      <Grid item xs={12} md={6}>
        <RegisterSalesOrderSection />
      </Grid>
      <Grid item xs={12} md={6}>
        <SellersSection data={sellerList} />
      </Grid>
      <Grid item xs={12} md={6}>
        <BuyersSection data={buyerList} />
      </Grid>
      <Grid item xs={12}>
        <PurchaseOrderSection data={purchaseList} />
      </Grid>
    </Grid>
  );
};

Trade.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export default Trade;
