import React, { ReactElement } from 'react';
import DashboardLayout from '../../../src/layout/DashboardLayout';
import { Button, Card, CardContent, Grid, Typography } from '@mui/material';
import TitleDashboard from '../../../src/components/smallComponents/dashboard/TitleDashboard';
import useTranslate from '../../../src/hooks/useTranslate';
import { Buttons, Inputs, OneTicket } from '../../../src/interfaces/translate';
import { Box } from '@mui/system';
import CustomTextField from '../../../src/components/forms/TextField';
import { GetStaticPaths, GetStaticProps } from 'next';
import { getTicketItem, getTicketsList } from '../../../src/networks/listApi';
import { MsgOfTicketObj, TicketObj } from '../../../src/interfaces/totalData';

interface Props {
  data: TicketObj;
}

const TicketItem = (props: Props): ReactElement => {
  const transition = useTranslate();
  const ticketsPageText: OneTicket = transition('webSiteConstant.ticket.oneTicket');
  const inputsText: Inputs = transition('webSiteConstant.inputs');
  const buttons: Buttons = transition('webSiteConstant.buttons');
  const { title, msg } = props?.data;

  return (
    <Grid container direction="column" rowSpacing={2}>
      <Grid item sx={{ width: '100%' }}>
        <Card>
          <TitleDashboard title={`${ticketsPageText.title} : ${title}`} />
          <CardContent>
            <Box mt={4}>
              {/*  SHOW TICKETS SECTION  */}
              {msg?.map((val: MsgOfTicketObj, index) => (
                <Card key={index} sx={{ backgroundColor: (theme) => theme.palette?.common?.card, my: 2 }}>
                  <TitleDashboard
                    title={`${ticketsPageText?.box?.title} : ${val.name === 'mySelf' ? 'من' : 'تیم پشتیبانی'}`}
                    color={val?.name === 'mySelf' ? '#4CAF50' : '#2196F3'}
                    subTitle={`${ticketsPageText?.box?.date} : ${val.date}`}
                    titleTypographyProps={{ fontSize: 16 }}
                  />
                  <CardContent sx={{ px: { xs: 0, md: 10 }, mt: 2 }}>
                    <Typography variant="body2">{val.text}</Typography>
                  </CardContent>
                </Card>
              ))}

              {/*  SEND AND ANSWER TICKETS SECTION  */}
              <Card sx={{ backgroundColor: (theme) => theme.palette?.common?.card, mt: 4 }}>
                <TitleDashboard title={ticketsPageText?.answer?.title} />
                <CardContent sx={{ px: 10 }}>
                  <CustomTextField
                    placeholder="متن پیام خود را وارد کنید"
                    label={inputsText.description}
                    rows={5}
                    multiline
                  />
                  <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2.5 }}>
                    <Grid container item xs={12} md={4} rowSpacing={{ xs: 2, md: 0 }} justifyContent="center">
                      <Grid item xs={12} md={6} sx={{ textAlign: 'center' }}>
                        <Button variant="outlined">{buttons.sendAndCloseTicket}</Button>
                      </Grid>
                      <Grid item xs={12} md={6} sx={{ textAlign: 'center' }}>
                        <Button>{buttons.sendTicket}</Button>
                      </Grid>
                    </Grid>
                  </Box>
                </CardContent>
              </Card>
            </Box>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

TicketItem.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

/******************************************************************************
 *                            GENERATE STATIC PATH
 *****************************************************************************/

export const getStaticPaths: GetStaticPaths = async () => {
  let list = await getTicketsList();
  let generatingPath = list?.map((val) => {
    return {
      params: { id: val?._id },
    };
  });

  return {
    paths: generatingPath,
    fallback: true,
  };
};

/******************************************************************************
 *                            FETCH DATA FROM SSG
 *****************************************************************************/

export const getStaticProps: GetStaticProps = async (context: any) => {
  let data = context?.params?.id && (await getTicketItem(context?.params?.id));
  return {
    props: {
      data,
    },
  };
};

export default TicketItem;
