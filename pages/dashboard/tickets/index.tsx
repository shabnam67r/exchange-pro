import React, { lazy, ReactElement, Suspense, useEffect, useState } from 'react';
import DashboardLayout from '../../../src/layout/DashboardLayout';
import { Button, Card, CardContent, Chip, Grid, TableCell, TableRow, Typography } from '@mui/material';
import TitleDashboard from '../../../src/components/smallComponents/dashboard/TitleDashboard';
import PrimaryTable from '../../../src/components/tables/PrimaryTable';
import { alpha } from '@mui/system';
import { handleColorStatus, handleTextStatus } from '../../../src/feature/function';
import useTranslate from '../../../src/hooks/useTranslate';
import { Buttons, DefaultObj } from '../../../src/interfaces/translate';
import { getTicketsList } from '../../../src/networks/listApi';
import { TicketObj } from '../../../src/interfaces/totalData';
import VisibilityIcon from '@mui/icons-material/Visibility';
import AddToPhotosOutlinedIcon from '@mui/icons-material/AddToPhotosOutlined';
import { useRouter } from 'next/router';

interface Props {}

const Tickets = (props: Props): ReactElement => {
  const transition = useTranslate();
  const [data, setData] = useState<TicketObj[]>([]);
  const ticketsPageText: DefaultObj = transition('webSiteConstant.ticket.allTicket');
  const buttons: Buttons = transition('webSiteConstant.buttons');
  const [trigger, setTrigger] = useState(false);
  const router = useRouter();

  const DialogComponent = trigger ? lazy(() => import('../../../src/components/dialogs/SupportRequestDialog')) : null;

  const handleClickOpen = () => setTrigger(true);

  const handleClose = () => setTrigger(false);

  const newTicketElement = (): ReactElement => {
    return (
      <Button sx={{ border: 'none' }} onClick={() => handleClickOpen()}>
        <AddToPhotosOutlinedIcon sx={{ fontSize: 18, ml: 1 }} />
        {buttons.newTicket}
      </Button>
    );
  };

  const handleGoingToTicketDetail = (link: string) => {
    return router.push({
      pathname: '/dashboard/tickets/[id]',
      query: { id: link },
    });
  };

  /******************************************************************************
   *                            FETCHING DATA
   *****************************************************************************/

  const getTicketList = async () => {
    let list = await getTicketsList();
    return setData(list);
  };

  useEffect(() => {
    getTicketList();
  }, []);

  return (
    <>
      <Grid container direction="column" rowSpacing={2}>
        {/* /// LIST ///  */}
        <Grid item sx={{ width: '100%' }}>
          <Card>
            <TitleDashboard title={ticketsPageText.title} subTitle={newTicketElement()} subTitleSx={{ pb: 1 }} />
            <CardContent>
              <PrimaryTable
                headers={ticketsPageText?.table}
                mainHead
                responsive
                loading={!data.length}
                countPage={12}
                selectPage={7}
                circularProgressProps={{ size: 40 }}
              >
                {data?.map((row, index) => (
                  <TableRow
                    key={index}
                    sx={{ '& td': { color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5), fontSize: 12 } }}
                  >
                    <TableCell>{row.title}</TableCell>
                    <TableCell align="right">{row.date}</TableCell>
                    <TableCell align="right">
                      <Chip
                        variant="outlined"
                        color={handleColorStatus(row.status)}
                        label={handleTextStatus(row.status)}
                        sx={{ borderRadius: '10px !important' }}
                      />
                    </TableCell>
                    <TableCell>
                      <Chip
                        sx={{ background: (theme) => theme.palette?.common?.primaryCommonGradient }}
                        clickable
                        onClick={() => handleGoingToTicketDetail(row._id)}
                        label={
                          <Typography variant="caption" sx={{ display: 'flex', alignItems: 'center' }}>
                            <VisibilityIcon sx={{ ml: 0.5 }} />
                            {buttons.see}
                          </Typography>
                        }
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </PrimaryTable>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      {trigger && (
        <Suspense fallback="">
          {/*// @ts-ignore*/}
          <DialogComponent open={trigger} onClose={handleClose} />
        </Suspense>
      )}
    </>
  );
};

Tickets.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export default Tickets;
