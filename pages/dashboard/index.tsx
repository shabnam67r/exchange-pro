import React, { ReactElement } from 'react';
import DashboardLayout from '../../src/layout/DashboardLayout';
import { Card, CardContent, Grid, TableCell, TableRow } from '@mui/material';
import TitleDashboard from '../../src/components/smallComponents/dashboard/TitleDashboard';
import useTranslate from '../../src/hooks/useTranslate';
import { DefaultObj, Overview, UserLevel } from '../../src/interfaces/translate';
import DisplayMedalLevelsCard from '../../src/components/cards/DisplayMedalLevelsCard';
import DisplayUserLevelsCard from '../../src/components/cards/DisplayUserLevelCard';
import CoinsInfoDetail from '../../src/sections/CoinsInfoDetail';
import { alpha } from '@mui/system';
import { validDisplayPrice } from '../../src/feature/function';
import PrimaryTable from '../../src/components/tables/PrimaryTable';
import { ActiveOrdersList, AllOrdersList } from '../../src/interfaces/totalData';

const Dashboard = () => {
  const transition = useTranslate();
  const titleOverviewText: Overview = transition('webSiteConstant.dashboard.overview');
  const titleUserLevelText: UserLevel = transition('webSiteConstant.dashboard.userLevel');
  const titleActiveOrdersText: DefaultObj = transition('webSiteConstant.dashboard.activeOrders');
  const titleAllOrdersText: DefaultObj = transition('webSiteConstant.dashboard.allOrders');

  /******************************************************************************
   *                            Initial Data
   *****************************************************************************/

  const initialData: { activeOrderList: ActiveOrdersList[]; allOrderList: AllOrdersList[] } = {
    activeOrderList: [
      {
        orderType: '4',
        market: 'سکه آزادی',
        amount: 2,
        unitPrice: 15956789,
        totalPrice: 123456789,
        full: 123456,
      },
      {
        orderType: '4',
        market: 'سکه آزادی',
        amount: 2,
        unitPrice: 15956789,
        totalPrice: 123456789,
        full: 123456,
      },
      {
        orderType: '4',
        market: 'سکه آزادی',
        amount: 2,
        unitPrice: 15956789,
        totalPrice: 123456789,
        full: 123456,
      },
      {
        orderType: '4',
        market: 'سکه آزادی',
        amount: 2,
        unitPrice: 15956789,
        totalPrice: 123456789,
        full: 123456,
      },
      {
        orderType: '4',
        market: 'سکه آزادی',
        amount: 2,
        unitPrice: 15956789,
        totalPrice: 123456789,
        full: 123456,
      },
      {
        orderType: '4',
        market: 'سکه آزادی',
        amount: 2,
        unitPrice: 15956789,
        totalPrice: 123456789,
        full: 123456,
      },
    ],
    allOrderList: [
      { orderType: '3', market: 'سکه امامی', amount: 2, unitPrice: 15956789, creatAt: '1400/3/8' },
      { orderType: '3', market: 'سکه امامی', amount: 2, unitPrice: 15956789, creatAt: '1400/3/8' },
      { orderType: '3', market: 'سکه امامی', amount: 2, unitPrice: 15956789, creatAt: '1400/3/8' },
      { orderType: '3', market: 'سکه امامی', amount: 2, unitPrice: 15956789, creatAt: '1400/3/8' },
      { orderType: '3', market: 'سکه امامی', amount: 2, unitPrice: 15956789, creatAt: '1400/3/8' },
      { orderType: '3', market: 'سکه امامی', amount: 2, unitPrice: 15956789, creatAt: '1400/3/8' },
      { orderType: '3', market: 'سکه امامی', amount: 2, unitPrice: 15956789, creatAt: '1400/3/8' },
      { orderType: '3', market: 'سکه امامی', amount: 2, unitPrice: 15956789, creatAt: '1400/3/8' },
    ],
  };

  return (
    <Grid container direction="column" rowSpacing={2}>
      {/* /// OVERVIEW OF THE MARKET ///  */}
      <Grid item>
        <Card sx={{ width: '100%' }}>
          <TitleDashboard title={titleOverviewText.title} subTitle={titleOverviewText.desc} />
          <CardContent>
            <CoinsInfoDetail />
          </CardContent>
        </Card>
      </Grid>

      {/* /// USER LEVEL ///  */}
      <Grid item>
        <Card sx={{ width: '100%' }}>
          <TitleDashboard title={titleUserLevelText?.title} />
          <CardContent>
            <Grid container rowSpacing={{ xs: 2, md: 0 }}>
              <Grid item xs={12} md={6} sx={{ display: 'flex', alignItems: 'center' }}>
                <DisplayMedalLevelsCard />
              </Grid>
              <Grid item xs={12} md={6}>
                <DisplayUserLevelsCard />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>

      {/* /// ACTIVE ORDERS LIST ///  */}
      <Grid container item>
        <Card sx={{ width: '100%' }}>
          <TitleDashboard title={titleActiveOrdersText?.title} />
          <CardContent>
            <PrimaryTable headers={titleActiveOrdersText?.table} mainHead responsive>
              {initialData.activeOrderList.map((row, index) => (
                <TableRow
                  key={index}
                  sx={{ '& td': { color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5), fontSize: 12 } }}
                >
                  <TableCell>{row.orderType}</TableCell>
                  <TableCell align="right">{row.market}</TableCell>
                  <TableCell align="right">{validDisplayPrice(row.amount)}</TableCell>
                  <TableCell align="right">{validDisplayPrice(row.unitPrice)}</TableCell>
                  <TableCell align="right">{validDisplayPrice(row.totalPrice)}</TableCell>
                  <TableCell align="right">{validDisplayPrice(row.full)}</TableCell>
                </TableRow>
              ))}
            </PrimaryTable>
          </CardContent>
        </Card>
      </Grid>

      <Grid container item columnSpacing={2} rowSpacing={{ xs: 2, md: 0 }}>
        {/* /// CHART INFO ///  */}
        <Grid item xs={12} md={5}>
          <Card sx={{ width: '100%' }}>
            <TitleDashboard title={titleActiveOrdersText?.title} />
            <CardContent />
          </Card>
        </Grid>

        {/* /// ALL ORDERS LIST ///  */}
        <Grid item xs={12} md={7}>
          <Card sx={{ width: '100%' }}>
            <TitleDashboard title={titleAllOrdersText?.title} />
            <CardContent>
              <PrimaryTable headers={titleAllOrdersText?.table} mainHead responsive>
                {initialData.allOrderList.map((row, index) => (
                  <TableRow
                    key={index}
                    sx={{ '& td': { color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5), fontSize: 12 } }}
                  >
                    <TableCell>{row.orderType}</TableCell>
                    <TableCell align="right">{row.market}</TableCell>
                    <TableCell align="right">{validDisplayPrice(row.amount)}</TableCell>
                    <TableCell align="right">{validDisplayPrice(row.unitPrice)}</TableCell>
                    <TableCell align="right">{row.creatAt}</TableCell>
                  </TableRow>
                ))}
              </PrimaryTable>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Grid>
  );
};

Dashboard.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export default Dashboard;
