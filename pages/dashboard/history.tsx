import React, { ReactElement, useState } from 'react';
import DashboardLayout from '../../src/layout/DashboardLayout';
import { Card, CardContent, Chip, Grid, TableCell, TableRow } from '@mui/material';
import TitleDashboard from '../../src/components/smallComponents/dashboard/TitleDashboard';
import PrimaryTable from '../../src/components/tables/PrimaryTable';
import { alpha, Box } from '@mui/system';
import { handleColorStatus, handleTextStatus, validDisplayPrice } from '../../src/feature/function';
import useTranslate from '../../src/hooks/useTranslate';
import { MyHistoryPage } from '../../src/interfaces/translate';
import { GetStaticProps } from 'next';
import { getHistoryList } from '../../src/networks/listApi';
import { HistoryObj } from '../../src/interfaces/totalData';
import { coinAndMoneySelectBoxList } from '../../src/feature/defaultInitialData';
import DefaultSelect from '../../src/components/forms/DefaultSelect';
import { REVALIDATE_TIME } from '../../src/constants/env';

interface Props {
  data?: HistoryObj[];
}

const History = (props: Props): ReactElement => {
  const transition = useTranslate();
  const myOrdersPageText: MyHistoryPage = transition('webSiteConstant.myHistoryPage');
  const [state, setState] = useState('برداشت ریالی');

  const handleDepositList = (e: any) => setState(e?.target?.value);

  const filterNode = (
    <Box sx={{ width: '18%', mb: 1 }}>
      <DefaultSelect items={coinAndMoneySelectBoxList} handleChange={handleDepositList} pill selected={state} />
    </Box>
  );

  return (
    <Grid container direction="column" rowSpacing={2}>
      {/* /// LIST ///  */}
      <Grid item sx={{ width: '100%' }}>
        <Card sx={{ width: '100%' }}>
          <TitleDashboard
            title={myOrdersPageText?.coin.title}
            filterOption={filterNode}
            titleBox={{ sx: { position: 'relative', bottom: '-8px' } }}
          />
          <CardContent>
            <PrimaryTable
              headers={myOrdersPageText[state === 'برداشت ریالی' ? 'money' : 'coin']?.table}
              mainHead
              responsive
            >
              {props?.data?.map((row, index) => (
                <TableRow
                  key={index}
                  sx={{ '& td': { color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5), fontSize: 12 } }}
                >
                  <TableCell>{row.depositType}</TableCell>
                  <TableCell align="right">{row.creatAt}</TableCell>
                  <TableCell align="right">{row.loginDate}</TableCell>
                  <TableCell align="right">{validDisplayPrice(row.amount)}</TableCell>
                  <TableCell align="right">{row.paymentCode}</TableCell>
                  <TableCell align="right">{validDisplayPrice(row.deposit)}</TableCell>
                  <TableCell align="right">
                    <Chip
                      color={handleColorStatus(row.status)}
                      label={handleTextStatus(row.status)}
                      sx={{ color: (theme) => theme.palette.common?.white }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </PrimaryTable>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

/******************************************************************************
 *                            FETCH DATA FROM SSG
 *****************************************************************************/

export const getStaticProps: GetStaticProps = async () => {
  let data = await getHistoryList();
  return {
    props: {
      data,
    },
    revalidate: REVALIDATE_TIME,
  };
};

/******************************************************************************
 *                            DASHBOARD LAYOUT
 *****************************************************************************/

History.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export default History;
