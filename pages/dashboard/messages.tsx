import React, { lazy, ReactElement, Suspense, useEffect, useState } from 'react';
import DashboardLayout from '../../src/layout/DashboardLayout';
import { Card, CardContent, Chip, Grid, TableCell, TableRow, Typography } from '@mui/material';
import TitleDashboard from '../../src/components/smallComponents/dashboard/TitleDashboard';
import PrimaryTable from '../../src/components/tables/PrimaryTable';
import { alpha } from '@mui/system';
import { handleColorStatus, handleTextStatus } from '../../src/feature/function';
import useTranslate from '../../src/hooks/useTranslate';
import { Buttons, DefaultObj } from '../../src/interfaces/translate';
import { getMessagesList } from '../../src/networks/listApi';
import { MessageObj } from '../../src/interfaces/totalData';
import VisibilityIcon from '@mui/icons-material/Visibility';

interface Props {}

const Messages = (props: Props): ReactElement => {
  const transition = useTranslate();
  const [data, setData] = useState<MessageObj[]>([]);
  const messagesPageText: DefaultObj = transition('webSiteConstant.mailPage');
  const buttons: Buttons = transition('webSiteConstant.buttons');
  const [trigger, setTrigger] = useState(false);
  const [selectItem, setSelectItem] = useState<MessageObj>();

  const getMessageList = async () => {
    let list = await getMessagesList();
    return setData(list);
  };

  const DialogComponent = trigger ? lazy(() => import('../../src/components/dialogs/MessageDialog')) : null;

  const handleClickOpen = (val: MessageObj) => {
    setSelectItem(val);
    setTrigger(true);
  };

  const handleClose = () => setTrigger(false);

  useEffect(() => {
    getMessageList();
  }, []);

  return (
    <>
      <Grid container direction="column" rowSpacing={2}>
        {/* /// LIST ///  */}
        <Grid item sx={{ width: '100%' }}>
          <Card>
            <TitleDashboard title={messagesPageText.title} />
            <CardContent>
              <PrimaryTable
                headers={messagesPageText?.table}
                mainHead
                responsive
                loading={!data.length}
                countPage={12}
                selectPage={7}
                circularProgressProps={{ size: 40 }}
              >
                {data?.map((row, index) => (
                  <TableRow
                    key={index}
                    sx={{ '& td': { color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5), fontSize: 12 } }}
                  >
                    <TableCell>{row.title}</TableCell>
                    <TableCell align="right">{row.date}</TableCell>
                    <TableCell align="right">
                      <Chip
                        variant="outlined"
                        color={handleColorStatus(row.status)}
                        label={handleTextStatus(row.status)}
                        sx={{ borderRadius: '10px !important' }}
                      />
                    </TableCell>
                    <TableCell>
                      <Chip
                        sx={{ background: (theme) => theme.palette.common.primaryCommonGradient }}
                        clickable
                        onClick={() => handleClickOpen(row)}
                        label={
                          <Typography variant="caption" sx={{ display: 'flex', alignItems: 'center' }}>
                            <VisibilityIcon sx={{ ml: 0.5 }} />
                            {buttons.see}
                          </Typography>
                        }
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </PrimaryTable>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      {trigger && (
        <Suspense fallback="">
          {/*// @ts-ignore*/}
          <DialogComponent open={trigger} onClose={handleClose} data={selectItem} />
        </Suspense>
      )}
    </>
  );
};

Messages.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};

export default Messages;
