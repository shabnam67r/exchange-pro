import { ReactElement } from 'react';
import HomeLayout from '../src/layout/HomeLayout';
import HomeSectionOne from '../src/sections/home/HomeSectionOne';
import HomeSectionTwo from '../src/sections/home/HomeSectionTwo';
import HomeSectionThree from '../src/sections/home/HomeSectionThree';
import HomeSectionFour from '../src/sections/home/HomeSectionFour';
import { Box, useTheme } from '@mui/system';
import { makeStyles } from '@mui/styles';
import { Theme, useMediaQuery } from '@mui/material';

interface Props {}

const HomePage = (props: Props): ReactElement => {
  const theme = useTheme();
  const desktop = useMediaQuery(theme.breakpoints.up('md'));

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      width: desktop ? '70%' : '90%',
      margin: '0 auto',
    },
  }));

  const classes = useStyles(props);

  return (
    <HomeLayout>
      <HomeSectionOne />
      <Box className={classes.root}>
        <HomeSectionTwo />
        <HomeSectionThree />
        <HomeSectionFour />
      </Box>
    </HomeLayout>
  );
};

export default HomePage;
