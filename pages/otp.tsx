import React, { ReactElement } from 'react';
import HomeLayout from '../src/layout/HomeLayout';
import { Button, Grid, Typography } from '@mui/material';
import FormCardLayout from '../src/components/layout/FormCardLayout';
import { Box } from '@mui/system';
import CustomTextField from '../src/components/forms/TextField';
import useTranslate from '../src/hooks/useTranslate';
import { Buttons, Inputs, TwoStepVerificationPage } from '../src/interfaces/translate';
import Link from 'next/link';

interface Props {}

const Otp = (props: Props): ReactElement => {
  const transition = useTranslate();
  const twoStepVerificationPageText: TwoStepVerificationPage = transition('webSiteConstant.twoStepVerificationPage');
  const inputsText: Inputs = transition('webSiteConstant.inputs');
  const buttons: Buttons = transition('webSiteConstant.buttons');

  return (
    <HomeLayout>
      <Grid container justifyContent="center">
        <Grid item xs md={4} mt={5}>
          <FormCardLayout>
            <Typography variant="subtitle1" sx={{ textAlign: 'center' }}>
              {twoStepVerificationPageText.title}
            </Typography>
            <Box mt={8}>
              <CustomTextField placeholder="******09" label={inputsText.phoneNumber} />
              <Box sx={{ mt: 6, textAlign: 'center', '& a': { textDecoration: 'none' } }}>
                <Link href="/otpVerifyCode">
                  <a>
                    <Button sx={{ px: 5, py: 1 }}>{buttons.sendConfirmCode}</Button>
                  </a>
                </Link>
              </Box>
            </Box>
          </FormCardLayout>
        </Grid>
      </Grid>
    </HomeLayout>
  );
};

export default Otp;
