import React, { ReactElement } from 'react';
import HomeLayout from '../src/layout/HomeLayout';
import InternalPagesLayout from '../src/layout/IntertalPagesLayout';
import { Box, useTheme } from '@mui/system';
import useTranslate from '../src/hooks/useTranslate';
import { InternalType } from '../src/interfaces/translate';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material';

interface Props {}

const Wage = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const wagePage: InternalType = transition('webSiteConstant.wagePage');

  function createData(amount: string, wage: number, level: string) {
    return { amount, wage, level };
  }

  const rows = [
    createData('کمتر از ۲۰ میلیون تومان', 0.4, 'سطح برنزی'),
    createData('بین ۲۰ تا ۵۰ میلیون تومان', 0.4, 'سطح نقره ای'),
    createData('بین ۵۰ تا ۱۰۰ میلیون تومان', 0.4, 'سطح طلایی'),
    createData('بین ۱۰۰ تا ۲۰۰ میلیون تومان', 0.4, 'سطح پلاتینیوم'),
    createData('بیشتر از ۲۰۰ میلیون تومان', 0.4, 'سطح اورانیوم'),
  ];

  return (
    <HomeLayout>
      <InternalPagesLayout title={wagePage.header}>
        <Box px={{ xs: theme.spacing(0), md: theme.spacing(15) }}>
          <Box>
            <Typography variant="body1" sx={{ mb: 1 }}>
              کارمزد معاملات زرینکس به صورت درصد از مبلغ کل معامله است و محاسبه آن بر اساس ملاحظات زیر صورت می گیرد.
            </Typography>
            <ul>
              <li>
                <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
                  کارمزد خریدار ارزدیجیتال به صورت ارزدیجیتال کسر و کارمزد فروشنده ارزدیجیتال از مبلغ دریافتی به تومان
                  کسر می شود.
                </Typography>
              </li>
              <li>
                <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
                  کارمزد از هر دو سمت معامله کسر می شود.
                </Typography>
              </li>
              <li>
                <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
                  در هنگام ثبت معاملات در والکس، کارمزد به شما نمایش داده می شود.
                </Typography>
              </li>
            </ul>
          </Box>
          <Box mt={4}>
            <Typography variant="body1" sx={{ mb: 1 }}>
              کارمزد معاملات :
            </Typography>
            <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
              با توجه به حجم معاملات سه ماه اخیر کاربر، کارمزد معاملات طبق جدول زیر به صورت پلکانی محاسبه می گردد:
            </Typography>
            <Box my={5}>
              <TableContainer sx={{ width: '50%', mx: 'auto' }}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell align="right">مقدار حجم معامله در سه ماه اخیر</TableCell>
                      <TableCell align="right">کارمزد معامله</TableCell>
                      <TableCell align="right">سطح کاربری</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map((row, index) => (
                      <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                        <TableCell align="right">
                          <Typography variant="caption" color={(theme) => theme.palette.text.secondary}>
                            {row.amount}
                          </Typography>
                        </TableCell>
                        <TableCell align="right">
                          <Typography variant="caption" color={(theme) => theme.palette.text.secondary}>
                            % {row.wage}
                          </Typography>
                        </TableCell>
                        <TableCell align="right">
                          <Typography variant="caption" color={(theme) => theme.palette.text.secondary}>
                            {row.level}
                          </Typography>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          </Box>
          <Box mt={4}>
            <Typography variant="body1" sx={{ mb: 1 }}>
              کارمزد برداشت ریالی :
            </Typography>
            <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
              برداشت‌های ریالی برای تمامی حساب‌ها، در قالب انتقال پایا و یک بار در روز خواهد بود. بدیهی است مدت زمان
              انجام تسویه ریالی، بسته به زمان درخواست برداشت، در روزهای غیر تعطیل می‌تواند حداکثر تا ۲۴ ساعت به طول
              انجامد.
            </Typography>
          </Box>
        </Box>
      </InternalPagesLayout>
    </HomeLayout>
  );
};

export default Wage;
