import React, { ReactElement } from 'react';
import HomeLayout from '../src/layout/HomeLayout';
import { Button, Grid, OutlinedInput, Theme, Typography } from '@mui/material';
import FormCardLayout from '../src/components/layout/FormCardLayout';
import { Box } from '@mui/system';
import useTranslate from '../src/hooks/useTranslate';
import { Buttons, TwoStepVerificationPage } from '../src/interfaces/translate';
import AccessTimeOutlinedIcon from '@mui/icons-material/AccessTimeOutlined';
import EditIcon from '../src/icons/someIcons/EditIcon';
import { makeStyles } from '@mui/styles';

interface Props {}

const OtpVerifyCode = (props: Props): ReactElement => {
  const transition = useTranslate();
  const twoStepVerificationPageText: TwoStepVerificationPage = transition('webSiteConstant.twoStepVerificationPage');
  const buttons: Buttons = transition('webSiteConstant.buttons');

  const initialData = [1, 2, 3, 4, 5];

  /******************************************************************************
   *                            Make Styles
   *****************************************************************************/
  const useStyles = makeStyles<Theme, Props>((theme: Theme) => ({
    root: {
      borderRadius: Number(theme.shape.borderRadius) / 1.5,
      '& > input': {
        padding: '10px',
        textAlign: 'center',
      },
    },
  }));

  const classes = useStyles(props);

  return (
    <HomeLayout>
      <Grid container justifyContent="center">
        <Grid item xs md={4} mt={5}>
          <FormCardLayout>
            <Typography variant="subtitle1" sx={{ textAlign: 'center' }}>
              {twoStepVerificationPageText.title}
            </Typography>
            <Box mt={6} px={{ xs: 0, md: 5 }}>
              <Typography variant="body2" sx={{ textAlign: 'center' }} color={(theme) => theme.palette.text.secondary}>
                {twoStepVerificationPageText.desc}
              </Typography>
              <Grid container columnSpacing={2} justifyContent="center" my={3}>
                {initialData.map((val, index) => (
                  <Grid key={index} item xs={2}>
                    <OutlinedInput className={classes.root} />
                  </Grid>
                ))}
              </Grid>
              <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'cetner', mt: 2 }}>
                <AccessTimeOutlinedIcon sx={{ ml: 1, color: (theme) => theme.palette.text.secondary }} />
                <Typography variant="caption" color={(theme) => theme.palette.text.secondary}>
                  {twoStepVerificationPageText.leftTime}:
                </Typography>
                <Typography variant="caption" color="error" mx={1}>
                  50 {twoStepVerificationPageText.timeUnit}
                </Typography>
              </Box>
              <Box sx={{ textAlign: 'center', '& svg': { ml: 1 } }}>
                <Button variant="text">
                  <EditIcon />
                  {twoStepVerificationPageText.edit}
                </Button>
              </Box>
              <Box sx={{ mt: 3, textAlign: 'center' }}>
                <Button sx={{ px: 5, py: 1 }}>{buttons.confirmPhoneNumber}</Button>
              </Box>
            </Box>
          </FormCardLayout>
        </Grid>
      </Grid>
    </HomeLayout>
  );
};

export default OtpVerifyCode;
