import React, { ReactElement, useEffect, useState } from 'react';
import HomeLayout from '../src/layout/HomeLayout';
import InternalPagesLayout from '../src/layout/IntertalPagesLayout';
import { Box, useTheme } from '@mui/system';
import useTranslate from '../src/hooks/useTranslate';
import { MarketsPage } from '../src/interfaces/translate';
import TitleDashboard from '../src/components/smallComponents/dashboard/TitleDashboard';
import { Card, CardContent, Grid } from '@mui/material';
import { getBuyersList, getSellersList } from '../src/networks/tradeApi';
import { TradeObj } from '../src/interfaces/totalData';
import BuyersSection from '../src/sections/dashboard/BuyersSection';
import SellersSection from '../src/sections/dashboard/SellersSection';
import CoinsInfoCard from '../src/components/dashboard/CoinsInfoCard';
import ChoicesTradeSection from '../src/sections/ChoicesTrade';
import TradingView from '../src/components/charts/TradingView';

interface Props {}

const Markets = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const marketsPage: MarketsPage = transition('webSiteConstant.bazarPage');
  const titleOverviewText: string = transition('webSiteConstant.dashboard.overview.title');
  const [sellerList, setSellerList] = useState<TradeObj[]>();
  const [buyerList, setBuyerList] = useState<TradeObj[]>();

  /******************************************************************************
   *                            FETCHING DATA
   *****************************************************************************/

  const getSellerData = async () => {
    let list = await getSellersList();
    return setSellerList(list);
  };

  const getBuyerData = async () => {
    let list = await getBuyersList();
    return setBuyerList(list);
  };

  useEffect(() => {
    getSellerData();
    getBuyerData();
  }, []);

  return (
    <HomeLayout>
      <InternalPagesLayout title={marketsPage.header} topComponent={<ChoicesTradeSection />}>
        <Box>
          <Card sx={{ width: '100%', p: { xs: 0, md: 'default' } }}>
            <Box mb={3}>
              <TitleDashboard title={titleOverviewText} />
            </Box>
            <CardContent sx={{ p: { xs: 0, md: 'default' } }}>
              <Grid container columnSpacing={{ xs: 0, md: 2 }} rowSpacing={{ xs: 2, md: 0 }}>
                <Grid item xs={12} md={9}>
                  <Box
                    sx={{
                      display: 'flex',
                      justifyContent: 'center',
                      height: '100%',
                      '& > article': {
                        width: '100%',
                        height: '100%',
                        '& > div > div': { height: '100% !important' },
                        '& > div': { height: '100% !important' },
                      },
                    }}
                  >
                    <TradingView theme={theme?.palette?.mode} />
                  </Box>
                </Grid>
                <Grid item xs={12} md={3}>
                  <CoinsInfoCard primaryTableProps={{ mainHead: true }} />
                </Grid>
              </Grid>
              <Grid container columnSpacing={{ xs: 0, md: 2 }} rowSpacing={{ xs: 2, md: 0 }}>
                <Grid item xs={12} md={6}>
                  <SellersSection data={sellerList} primaryTableProps={{ mainHead: true }} />
                </Grid>
                <Grid item xs={12} md={6}>
                  <BuyersSection data={buyerList} primaryTableProps={{ mainHead: true }} />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Box>
      </InternalPagesLayout>
    </HomeLayout>
  );
};

export default Markets;
