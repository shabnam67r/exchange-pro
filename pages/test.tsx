import { Button, Checkbox, Chip, Slider, Switch } from '@mui/material';
import { Box } from '@mui/system';
import { useDispatch } from 'src/redux';
import { changeTheme } from 'src/redux/Settings';
import { CustomSwitch } from '../src/components/switchs/Switch';
import DefaultFooter from 'src/components/footer/DefaultFooter';
import { Statistics } from 'src/interfaces/translate';

interface Props {
  data: Statistics;
}
export default function Test() {
  const dispatch = useDispatch();

  return (
    <Box>
      <Button
        onClick={() => {
          dispatch(changeTheme());
        }}
        variant="contained"
        color="primary"
      >
        Button
      </Button>
      <CustomSwitch />
      <DefaultFooter />
      <Button
        onClick={() => {
          dispatch(changeTheme());
        }}
        variant="contained"
        color="primary"
      >
        Button
      </Button>
      <Button variant="outlined" color="primary">
        Button
      </Button>
      <Button variant="outlined" color="secondary">
        Button
      </Button>
      <Button variant="text" color="primary">
        Button
      </Button>
      <Button variant="text" color="primary" disabled>
        Button
      </Button>
      <Checkbox />
      <Chip label="sdasdas" color="primary" />
      <Slider />
      <Switch />
      <CustomSwitch />
      <DefaultFooter />
    </Box>
  );
}
