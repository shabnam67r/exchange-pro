import React, { ReactElement, useState } from 'react';
import HomeLayout from '../src/layout/HomeLayout';
import InternalPagesLayout from '../src/layout/IntertalPagesLayout';
import { Box } from '@mui/system';
import useTranslate from '../src/hooks/useTranslate';
import { InternalTypeWithList } from '../src/interfaces/translate';
import { Accordion, AccordionDetails, AccordionSummary, Typography } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MarkDownViewer from '../src/components/markDown/MarkDownViewer';

interface Props {}

const FrequentlyAskedQuestions = (props: Props): ReactElement => {
  const transition = useTranslate();
  const commonQuestionsPage: InternalTypeWithList = transition('webSiteConstant.commonQuestionsPage');
  const [expanded, setExpanded] = useState<string | false>(false);

  const handleChange = (panel: string) => (event: React.SyntheticEvent, newExpanded: boolean) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
    <HomeLayout>
      <InternalPagesLayout title={commonQuestionsPage.header}>
        <Box>
          {commonQuestionsPage.titles.map((val) => (
            <Accordion key={val.index} expanded={expanded === val.index} onChange={handleChange(val.index)}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon color="primary" />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                sx={{ '& > div:first-child': { justifyContent: 'space-between', width: '100%' } }}
              >
                <Typography>{val.title}</Typography>
                <Typography variant="caption" color="primary" sx={{ ml: 1 }}>
                  {expanded === val.index
                    ? commonQuestionsPage.captionText.seeLess
                    : commonQuestionsPage.captionText.seeMore}
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography>
                  <MarkDownViewer data={val.path} />
                </Typography>
              </AccordionDetails>
            </Accordion>
          ))}
        </Box>
      </InternalPagesLayout>
    </HomeLayout>
  );
};

export default FrequentlyAskedQuestions;
