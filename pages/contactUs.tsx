import React, { ReactElement } from 'react';
import HomeLayout from '../src/layout/HomeLayout';
import InternalPagesLayout from '../src/layout/IntertalPagesLayout';
import { Box, useTheme } from '@mui/system';
import useTranslate from '../src/hooks/useTranslate';
import { InternalType } from '../src/interfaces/translate';
import { CardMedia, Typography } from '@mui/material';

interface Props {}

const ContactUs = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const contactUsPage: InternalType = transition('webSiteConstant.contactUsPage');

  return (
    <HomeLayout>
      <InternalPagesLayout title={contactUsPage.header}>
        <Box px={{ xs: theme.spacing(0), md: theme.spacing(5) }}>
          <Box>
            <Typography variant="body1" sx={{ mb: 1 }}>
              اطلاعات تماس :
            </Typography>
            <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
              ﭘﺴﺖ اﻟﮑﺘﺮوﻧﯿﮏ : info@wallex.ir
            </Typography>
            <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
              تلفن : ۹۱۰۰۶۵۵۵ - ۰۲۱ (۷ روز هفته از ساعت ۸ صبح تا ۱۸)
            </Typography>
          </Box>
          <Box mt={2}>
            <Typography variant="body1" sx={{ mb: 1 }}>
              آدرس :
            </Typography>
            <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
              تهران -میرداماد - پلاک 99
            </Typography>
          </Box>
          <Box
            sx={{
              width: '100%',
              height: 500,
              mt: 4,
            }}
          >
            <CardMedia
              component="img"
              sx={{ width: '100%', height: '100%' }}
              src="/static/images/default/map.png"
              alt="Destination, map"
            />
          </Box>
        </Box>
      </InternalPagesLayout>
    </HomeLayout>
  );
};

export default ContactUs;
