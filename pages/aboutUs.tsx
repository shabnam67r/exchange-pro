import React, { ReactElement } from 'react';
import HomeLayout from '../src/layout/HomeLayout';
import InternalPagesLayout from '../src/layout/IntertalPagesLayout';
import { Box, useTheme } from '@mui/system';
import useTranslate from '../src/hooks/useTranslate';
import { InternalType } from '../src/interfaces/translate';
import { Grid, Typography } from '@mui/material';
import Logo from '../src/icons/Logo';

interface Props {}

const AboutUs = (props: Props): ReactElement => {
  const theme = useTheme();
  const transition = useTranslate();
  const aboutUsPage: InternalType = transition('webSiteConstant.aboutUsPage');

  return (
    <HomeLayout>
      <InternalPagesLayout title={aboutUsPage.header}>
        <Box px={{ xs: theme.spacing(0), md: theme.spacing(15) }}>
          <Box>
            <Typography variant="body1" sx={{ mb: 1 }}>
              ما که هستیم؟
            </Typography>
            <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
              ما نیز همانند بسیار از شما مراجعه کنندگان به این پایگاه مجازی، در برهه ای زمان ، سختی ها و پیچیدگی های
              مراحل خرید فروش و سکه طلا در شکل سنتی آن را چشیده ام به خوبی می دانیم که یافتن بهترین قیمت خرید یا فروش
              سکه در بازار شلوغ کنونی کار بس پر دغدغه ،زمان بر ، و گیج کنند است. در سایت ما ساده سازی و روشن سازی فرایند
              معاملات سکه انجام دهید و با کمترین دردسترس و در کمترین زمان انجام دهید. اگر چه نهایت سعی خود را نمودایم تا
              به حداکثر سادگی در فرآیند انتخاب معاملات سکه دست پیدا کنیم، لیکن همواره مشتاق دریافت نظرات و پیشنهادات
              سازنده شما برای هر چه بهتر و کارآمدتر کردن این فرآیند هستیم .در صورتی که علاقمند در میان گذاشتن نظرات خود
              هستید آنها را به ادرس زیر ارسال کنید
            </Typography>
          </Box>
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              justifyContent: 'center',
              mt: 4,
              '& svg': {
                width: '100%',
                height: '100%',
              },
            }}
          >
            <Grid item xs={9} md={3}>
              <Logo color={theme.palette.text.primary} />
            </Grid>
          </Box>
        </Box>
      </InternalPagesLayout>
    </HomeLayout>
  );
};

export default AboutUs;
