import type { AppProps } from 'next/app';
import React, { ReactElement, ReactNode, useState } from 'react';
import { useStore } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import NextHead from 'next/head';
import { create } from 'jss';
import jssRtl from 'jss-rtl';
import acceptLanguage from 'accept-language';
import { wrapperRedux } from 'src/redux/store';
import DocsStyledEngineProvider from 'src/utils/StyledEngineProvider';
import { createCustomTheme } from 'src/styles/styles';
import { RootState, useDispatch, useSelector } from 'src/redux';
import { CssBaseline, StyledEngineProvider, useMediaQuery } from '@mui/material';
import { useEffect } from 'react';
import { ThemeProvider, Theme } from '@mui/material/styles';
import { StylesProvider, jssPreset } from '@mui/styles';
import SnackbarProvider from 'src/providers/SnackbarProvider';
import { isMobileMode } from '../src/redux/MobileMode';
import '../src/fonts/fonts.scss';
import { useTheme } from '@mui/system';
import { NextPage } from 'next';

declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}

// Configure JSS
export const jss = create({
  plugins: [...jssPreset().plugins, jssRtl()],
  // insertionPoint: process.browser ? document.querySelector('#insertion-point-jss') : undefined,
});

acceptLanguage.languages(['fa', 'en']);

// console.log(`
//
// `);
// console.log(
//   `%c
//   Developed by: Ariana germany Team
//   www.arianagermany.com
// `,
//   'font-family:monospace;color:red;font-size:18px;',
// );

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

function AppWrapper({ Component, pageProps }: AppPropsWithLayout) {
  const themePro = useTheme();
  const dispatch = useDispatch();
  const store = useStore();
  const isMobileModeQuery = useMediaQuery(`(max-width:${themePro?.breakpoints?.values?.md})`);
  const settings = useSelector((state: RootState) => state.settings);
  const [theme, setTheme] = useState(createCustomTheme(settings.theme));
  const getLayout = Component.getLayout ?? ((page) => page);

  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles?.parentElement?.removeChild(jssStyles);
    }
  }, []);

  useEffect(() => {
    setTheme(createCustomTheme(settings.theme));
  }, [settings.theme]);
  let fonts = ['https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700&display=swap'];

  useEffect(() => {
    dispatch(isMobileMode(isMobileModeQuery));
  }, [isMobileModeQuery]);

  return (
    <React.Fragment>
      <NextHead>
        {fonts.map((font) => (
          <link rel="stylesheet" href={font} key={font} />
        ))}
      </NextHead>
      <PersistGate persistor={(store as any).__persistor} loading={<div>Loading</div>}>
        <StyledEngineProvider injectFirst>
          <StylesProvider jss={jss}>
            <StyledEngineProvider injectFirst>
              <ThemeProvider theme={theme}>
                <CssBaseline />
                <DocsStyledEngineProvider>
                  <SnackbarProvider>{getLayout(<Component {...pageProps} />)}</SnackbarProvider>
                </DocsStyledEngineProvider>
              </ThemeProvider>
            </StyledEngineProvider>
          </StylesProvider>
        </StyledEngineProvider>
      </PersistGate>
    </React.Fragment>
  );
}

export default wrapperRedux.withRedux(AppWrapper);
