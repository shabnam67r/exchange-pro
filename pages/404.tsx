import { Typography } from '@mui/material';

const NotFound = () => {
  return (
    <div>
      <Typography variant="h2">404</Typography>
    </div>
  );
};

export default NotFound;
