import React, { ReactElement } from 'react';
import useTranslate from '../../src/hooks/useTranslate';
import { Buttons, ForgetPasswordPageObj, Inputs } from '../../src/interfaces/translate';
import { Button, CircularProgress, FormControl, Typography } from '@mui/material';
import { Box } from '@mui/system';
import AuthLayout from '../../src/components/layout/AuthLayout';
import wait from '../../src/utils/wait';
import { REQUIRED, WAITING_WITH } from '../../src/constants/env';
import * as Yup from 'yup';
import { useRouter } from 'next/router';
import { Form, Formik } from 'formik';
import CustomTextField from '../../src/components/forms/TextField';
import { LoadingButton } from '@mui/lab';
import Link from 'next/link';

interface Props {}

const ForgetPassword = (props: Props): ReactElement => {
  const router = useRouter();
  const transition = useTranslate();
  const forgetPasswordPage: ForgetPasswordPageObj = transition('webSiteConstant.forgetPasswordPage');
  const inputsText: Inputs = transition('webSiteConstant.inputs');
  const buttons: Buttons = transition('webSiteConstant.buttons');

  /******************************************************************************
   *                            Formik Territory
   *****************************************************************************/
  const formik = {
    onSubmit: async (values: any, { setSubmitting }: any) => {
      await wait(WAITING_WITH);
      setSubmitting(false);
      return router.push('/auth/addNewPassword');
    },
    initialValues: {
      email: '',
    },
    validationSchema: Yup.object().shape({
      email: Yup.string().email().required(REQUIRED),
    }),
  };

  return (
    <AuthLayout size="small">
      <Typography variant="subtitle1" sx={{ textAlign: 'center' }}>
        {forgetPasswordPage.forgetPasswordHeader}
      </Typography>
      <Box mt={6}>
        <Formik
          initialValues={formik.initialValues}
          validationSchema={formik.validationSchema}
          onSubmit={formik.onSubmit}
        >
          {({ values, errors, handleChange, handleSubmit, touched, isSubmitting }) => (
            <Form onSubmit={handleSubmit}>
              <FormControl variant="outlined" fullWidth sx={{ mb: 3 }}>
                <CustomTextField
                  name="email"
                  placeholder="example@example.com"
                  label={inputsText.email}
                  value={values.email}
                  onChange={handleChange}
                  errors={errors.email && touched.email && errors.email}
                />
              </FormControl>
              <Box sx={{ textAlign: 'center', my: 3 }}>
                <LoadingButton
                  type="submit"
                  loading={isSubmitting}
                  loadingIndicator={<CircularProgress color="inherit" size={22} />}
                  sx={{
                    border: 'none',
                    minWidth: 180,
                    py: (theme) => theme.spacing(2),
                  }}
                >
                  {buttons.sendRestoreLink}
                </LoadingButton>
              </Box>
            </Form>
          )}
        </Formik>

        {/*  SOME RECOMMENDS FIELD BOX  */}
        <Box my={3}>
          <Typography
            variant="caption"
            color={(theme) => theme.palette.text.secondary}
            sx={{ '& a': { textDecoration: 'none' }, display: 'block' }}
          >
            {forgetPasswordPage.alreadyHaveAccount?.title}
            <Link href={forgetPasswordPage.alreadyHaveAccount?.path}>
              <a>
                <Button variant="text" sx={{ minWidth: 60, pr: 1 }}>
                  {forgetPasswordPage.alreadyHaveAccount?.linkText}
                </Button>
              </a>
            </Link>
          </Typography>
        </Box>
      </Box>
    </AuthLayout>
  );
};

export default ForgetPassword;
