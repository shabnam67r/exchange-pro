import React, { ReactElement, useState } from 'react';
import useTranslate from '../../src/hooks/useTranslate';
import { Buttons, Inputs, LoginPageObj } from '../../src/interfaces/translate';
import { Button, CircularProgress, FormControl, IconButton, InputAdornment, Typography } from '@mui/material';
import { Box } from '@mui/system';
import AuthLayout from '../../src/components/layout/AuthLayout';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import wait from '../../src/utils/wait';
import { REQUIRED, WAITING_WITH } from '../../src/constants/env';
import { useRouter } from 'next/router';
import CustomTextField from '../../src/components/forms/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { LoadingButton } from '@mui/lab';
import Link from 'next/link';

interface Props {}

const Login = (props: Props): ReactElement => {
  const router = useRouter();
  const transition = useTranslate();
  const loginPage: LoginPageObj = transition('webSiteConstant.loginPage');
  const inputsText: Inputs = transition('webSiteConstant.inputs');
  const buttons: Buttons = transition('webSiteConstant.buttons');
  const [passwordValue, setPasswordValue] = useState<boolean>(false);

  const handleClickShowPassword = () => setPasswordValue(!passwordValue);

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  /******************************************************************************
   *                            Formik Territory
   *****************************************************************************/
  const formik = {
    onSubmit: async (values: any, { setSubmitting }: any) => {
      await wait(WAITING_WITH);
      setSubmitting(false);
      return router.push('/dashboard');
    },
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: Yup.object().shape({
      email: Yup.string().email().required(REQUIRED),
      password: Yup.string().required(REQUIRED),
    }),
  };

  return (
    <AuthLayout size="small">
      <Typography variant="subtitle1" sx={{ textAlign: 'center' }}>
        {loginPage.header}
      </Typography>
      <Box mt={6}>
        <Formik
          initialValues={formik.initialValues}
          validationSchema={formik.validationSchema}
          onSubmit={formik.onSubmit}
        >
          {({ values, errors, handleChange, handleSubmit, touched, isSubmitting }) => (
            <Form onSubmit={handleSubmit}>
              <FormControl variant="outlined" fullWidth sx={{ mb: 3 }}>
                <CustomTextField
                  name="email"
                  placeholder="example@example.com"
                  label={inputsText.email}
                  value={values.email}
                  onChange={handleChange}
                  errors={errors.email && touched.email && errors.email}
                />
              </FormControl>
              <FormControl variant="outlined" fullWidth sx={{ mb: 3 }}>
                <CustomTextField
                  name="password"
                  type={passwordValue ? 'text' : 'password'}
                  placeholder="*********"
                  label={inputsText.password}
                  value={values.password}
                  onChange={handleChange}
                  errors={errors.password && touched.password && errors.password}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <IconButton
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          sx={{ background: 'inherit', ml: 2 }}
                        >
                          {passwordValue ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </FormControl>
              <Box sx={{ textAlign: 'center', my: 3 }}>
                <LoadingButton
                  type="submit"
                  loading={isSubmitting}
                  loadingIndicator={<CircularProgress color="inherit" size={22} />}
                  sx={{
                    border: 'none',
                    minWidth: 180,
                    py: (theme) => theme.spacing(2),
                  }}
                >
                  {buttons.login}
                </LoadingButton>
              </Box>
            </Form>
          )}
        </Formik>

        {/*  SOME RECOMMENDS FIELD BOX  */}
        <Box my={3}>
          <Typography
            variant="caption"
            color={(theme) => theme.palette.text.secondary}
            sx={{ '& a': { textDecoration: 'none' }, display: 'block' }}
          >
            {loginPage.forgetPassword?.title}
            <Link href={loginPage.forgetPassword?.path}>
              <a>
                <Button variant="text" sx={{ minWidth: 60, pr: 1 }}>
                  {loginPage.forgetPassword?.linkText}
                </Button>
              </a>
            </Link>
          </Typography>
          <Typography
            variant="caption"
            color={(theme) => theme.palette.text.secondary}
            sx={{ '& a': { textDecoration: 'none' }, display: 'block' }}
          >
            {loginPage.newAccount?.title}
            <Link href={loginPage.newAccount?.path}>
              <a>
                <Button variant="text" sx={{ minWidth: 60 }}>
                  {loginPage.newAccount?.linkText}
                </Button>
              </a>
            </Link>
          </Typography>
        </Box>
      </Box>
    </AuthLayout>
  );
};

export default Login;
