import React, { ReactElement, useState } from 'react';
import useTranslate from '../../src/hooks/useTranslate';
import { Buttons, ForgetPasswordPageObj, Inputs } from '../../src/interfaces/translate';
import { Button, CircularProgress, FormControl, IconButton, InputAdornment, Typography } from '@mui/material';
import { Box } from '@mui/system';
import AuthLayout from '../../src/components/layout/AuthLayout';
import { Form, Formik } from 'formik';
import CustomTextField from '../../src/components/forms/TextField';
import { LoadingButton } from '@mui/lab';
import Link from 'next/link';
import wait from '../../src/utils/wait';
import { REQUIRED, WAITING_WITH } from '../../src/constants/env';
import * as Yup from 'yup';
import { useRouter } from 'next/router';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Visibility from '@mui/icons-material/Visibility';

interface Props {}

const AddNewPassword = (props: Props): ReactElement => {
  const router = useRouter();
  const transition = useTranslate();
  const forgetPasswordPage: ForgetPasswordPageObj = transition('webSiteConstant.forgetPasswordPage');
  const inputsText: Inputs = transition('webSiteConstant.inputs');
  const buttons: Buttons = transition('webSiteConstant.buttons');
  const [passwordValue, setPasswordValue] = useState<boolean>(false);
  const [repeatPasswordValue, setRepeatPasswordValue] = useState<boolean>(false);

  const handleClickShowPassword = () => setPasswordValue(!passwordValue);

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  const handleClickShowRepeatPassword = () => setRepeatPasswordValue(!repeatPasswordValue);

  /******************************************************************************
   *                            Formik Territory
   *****************************************************************************/
  const formik = {
    onSubmit: async (values: any, { setSubmitting }: any) => {
      await wait(WAITING_WITH);
      setSubmitting(false);
      return router.push('/dashboard');
    },
    initialValues: {
      password: '',
      repeatPassword: '',
    },
    validationSchema: Yup.object().shape({
      password: Yup.string().required(REQUIRED),
      repeatPassword: Yup.string().required(REQUIRED),
    }),
  };

  return (
    <AuthLayout size="small">
      <Typography variant="subtitle1" sx={{ textAlign: 'center' }}>
        {forgetPasswordPage.newPasswordHeader}
      </Typography>
      <Box mt={6}>
        <Formik
          initialValues={formik.initialValues}
          validationSchema={formik.validationSchema}
          onSubmit={formik.onSubmit}
        >
          {({ values, errors, handleChange, handleSubmit, touched, isSubmitting }) => (
            <Form onSubmit={handleSubmit}>
              <FormControl variant="outlined" fullWidth sx={{ mb: 3 }}>
                <CustomTextField
                  name="password"
                  type={passwordValue ? 'text' : 'password'}
                  placeholder="*********"
                  label={inputsText.password}
                  value={values.password}
                  onChange={handleChange}
                  errors={errors.password && touched.password && errors.password}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <IconButton
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          sx={{ background: 'inherit', ml: 2 }}
                        >
                          {passwordValue ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </FormControl>
              <FormControl variant="outlined" fullWidth sx={{ mb: 3 }}>
                <CustomTextField
                  name="repeatPassword"
                  type={repeatPasswordValue ? 'text' : 'password'}
                  placeholder="*********"
                  label={inputsText.passwordRepeat}
                  value={values.repeatPassword}
                  onChange={handleChange}
                  errors={errors.repeatPassword && touched.repeatPassword && errors.repeatPassword}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="start">
                        <IconButton
                          onClick={handleClickShowRepeatPassword}
                          onMouseDown={handleMouseDownPassword}
                          sx={{ background: 'inherit', ml: 2 }}
                        >
                          {repeatPasswordValue ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </FormControl>
              <Box sx={{ textAlign: 'center', my: 3 }}>
                <LoadingButton
                  type="submit"
                  loading={isSubmitting}
                  loadingIndicator={<CircularProgress color="inherit" size={22} />}
                  sx={{
                    border: 'none',
                    minWidth: 180,
                    py: (theme) => theme.spacing(2),
                  }}
                >
                  {buttons.confirmNewPassword}
                </LoadingButton>
              </Box>
            </Form>
          )}
        </Formik>

        {/*  SOME RECOMMENDS FIELD BOX  */}
        <Box my={3}>
          <Typography
            variant="caption"
            color={(theme) => theme.palette.text.secondary}
            sx={{ '& a': { textDecoration: 'none' }, display: 'block' }}
          >
            {forgetPasswordPage.alreadyHaveAccount?.title}
            <Link href={forgetPasswordPage.alreadyHaveAccount?.path}>
              <a>
                <Button variant="text" sx={{ minWidth: 60, pr: 1 }}>
                  {forgetPasswordPage.alreadyHaveAccount?.linkText}
                </Button>
              </a>
            </Link>
          </Typography>
        </Box>
      </Box>
    </AuthLayout>
  );
};

export default AddNewPassword;
