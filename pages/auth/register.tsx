import React, { ReactElement, useState } from 'react';
import useTranslate from '../../src/hooks/useTranslate';
import { Buttons, GlobalObj, Inputs } from '../../src/interfaces/translate';
import { Button, Checkbox, CircularProgress, FormControl, FormControlLabel, Grid, Typography } from '@mui/material';
import { Box } from '@mui/system';
import AuthLayout from '../../src/components/layout/AuthLayout';
import { Form, Formik } from 'formik';
import CustomTextField from '../../src/components/forms/TextField';
import { LoadingButton } from '@mui/lab';
import wait from '../../src/utils/wait';
import { REQUIRED, WAITING_WITH } from '../../src/constants/env';
import * as Yup from 'yup';
import { useRouter } from 'next/router';
import DangerMessage from '../../src/components/home/DangerMessage';
import NextLink from '../../src/components/smallComponents/NextLink';

interface Props {}

const Register = (props: Props): ReactElement => {
  const router = useRouter();
  const transition = useTranslate();
  const registerPage: GlobalObj = transition('webSiteConstant.registerPage.global');
  const inputsText: Inputs = transition('webSiteConstant.inputs');
  const buttons: Buttons = transition('webSiteConstant.buttons');
  const [acceptTerms, setAcceptTerms] = useState<boolean>(false);

  /******************************************************************************
   *                            Formik Territory
   *****************************************************************************/
  const formik = {
    onSubmit: async (values: any, { setSubmitting }: any) => {
      await wait(WAITING_WITH);
      setSubmitting(false);
      return router.push('/auth/personalIdentification');
    },
    initialValues: {
      email: '',
      referralId: '',
      password: '',
      repeatPassword: '',
    },
    validationSchema: Yup.object().shape({
      email: Yup.string().email().required(REQUIRED),
      referralId: Yup.string(),
      password: Yup.string().required(REQUIRED),
      repeatPassword: Yup.string().required(REQUIRED),
    }),
  };

  const handleAcceptTerms = () => setAcceptTerms(!acceptTerms);

  return (
    <AuthLayout>
      <Typography variant="subtitle1" sx={{ textAlign: 'center' }}>
        {registerPage.header}
      </Typography>
      <Box sx={{ textAlign: 'center' }} mt={3}>
        <DangerMessage text={registerPage.caution} />
      </Box>
      <Box mt={3}>
        <Formik
          initialValues={formik.initialValues}
          validationSchema={formik.validationSchema}
          onSubmit={formik.onSubmit}
        >
          {({ values, errors, handleChange, handleSubmit, touched, isSubmitting }) => (
            <Form onSubmit={handleSubmit}>
              <Grid container columnSpacing={{ xs: 0, md: 2 }} rowSpacing={{ xs: 2, md: 0 }}>
                <Grid item xs={12} md={6}>
                  <FormControl variant="outlined" fullWidth>
                    <CustomTextField
                      name="email"
                      placeholder="example@example.com"
                      label={inputsText.email}
                      value={values.email}
                      onChange={handleChange}
                      errors={errors.email && touched.email && errors.email}
                    />
                  </FormControl>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormControl variant="outlined" fullWidth>
                    <CustomTextField
                      name="referralId"
                      placeholder="برای مثال : 123456"
                      label={inputsText.inviteCode}
                      value={values.referralId}
                      onChange={handleChange}
                      errors={errors.referralId && touched.referralId && errors.referralId}
                    />
                  </FormControl>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormControl variant="outlined" fullWidth>
                    <CustomTextField
                      name="password"
                      type="password"
                      placeholder="*********"
                      label={inputsText.password}
                      value={values.password}
                      onChange={handleChange}
                      errors={errors.password && touched.password && errors.password}
                    />
                  </FormControl>
                </Grid>
                <Grid item xs={12} md={6}>
                  <FormControl variant="outlined" fullWidth>
                    <CustomTextField
                      name="repeatPassword"
                      type="password"
                      placeholder="*********"
                      label={inputsText.passwordRepeat}
                      value={values.repeatPassword}
                      onChange={handleChange}
                      errors={errors.repeatPassword && touched.repeatPassword && errors.repeatPassword}
                    />
                  </FormControl>
                </Grid>
              </Grid>

              <Box sx={{ textAlign: 'center' }} mt={3}>
                <FormControlLabel
                  control={<Checkbox checked={acceptTerms} onChange={handleAcceptTerms} />}
                  label={
                    <Typography variant="caption" sx={{ '& a': { color: (theme) => theme.palette.primary.main } }}>
                      <Typography sx={{ display: 'inline', ml: 1 }}>
                        <NextLink href={registerPage.checkBox?.path}>{registerPage.checkBox.linkText}</NextLink>
                      </Typography>
                      {registerPage.checkBox.title}
                    </Typography>
                  }
                />
              </Box>

              <Box sx={{ textAlign: 'center', my: 3 }}>
                <LoadingButton
                  type="submit"
                  loading={isSubmitting}
                  disabled={!acceptTerms}
                  loadingIndicator={<CircularProgress color="inherit" size={22} />}
                  sx={{
                    border: 'none',
                    minWidth: 180,
                    py: (theme) => theme.spacing(2),
                  }}
                >
                  {buttons.register}
                </LoadingButton>
              </Box>
            </Form>
          )}
        </Formik>

        {/*  SOME RECOMMENDS FIELD BOX  */}
        <Box my={3}>
          <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
            {registerPage.forgetPassword?.title}
            <NextLink href={registerPage.forgetPassword?.path}>
              <Button variant="text" sx={{ minWidth: 60, pr: 1 }}>
                {registerPage.forgetPassword?.linkText}
              </Button>
            </NextLink>
          </Typography>
          <Typography variant="caption" color={(theme) => theme.palette.text.secondary} sx={{ display: 'block' }}>
            {registerPage.newAccount?.title}
            <NextLink href={registerPage.newAccount?.path}>
              <Button variant="text" sx={{ minWidth: 60 }}>
                {registerPage.newAccount?.linkText}
              </Button>
            </NextLink>
          </Typography>
        </Box>
      </Box>
    </AuthLayout>
  );
};

export default Register;
