import React, { ReactElement, useState } from 'react';
import useTranslate from '../../src/hooks/useTranslate';
import { Buttons, Inputs, PersonalIdentificationObj } from '../../src/interfaces/translate';
import ErrorOutlineOutlinedIcon from '@mui/icons-material/ErrorOutlineOutlined';
import ModeEditOutlineOutlinedIcon from '@mui/icons-material/ModeEditOutlineOutlined';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import {
  Button,
  CardMedia,
  CircularProgress,
  Divider,
  FormControl,
  Grid,
  Paper,
  SvgIcon,
  TableCell,
  TableRow,
  Typography,
} from '@mui/material';
import { alpha, Box } from '@mui/system';
import AuthLayout from '../../src/components/layout/AuthLayout';
import { Form, Formik } from 'formik';
import CustomTextField from '../../src/components/forms/TextField';
import { LoadingButton } from '@mui/lab';
import wait from '../../src/utils/wait';
import { REQUIRED, WAITING_WITH } from '../../src/constants/env';
import * as Yup from 'yup';
import { useRouter } from 'next/router';
import AddToPhotosOutlinedIcon from '@mui/icons-material/AddToPhotosOutlined';
import FileUploader from '../../src/components/FileUploader';
import PrimaryTable from '../../src/components/tables/PrimaryTable';

interface Props {}

const PersonalIdentification = (props: Props): ReactElement => {
  const router = useRouter();
  const transition = useTranslate();
  const personalIdentificationPage: PersonalIdentificationObj = transition(
    'webSiteConstant.registerPage.personalIdentification',
  );
  const inputsText: Inputs = transition('webSiteConstant.inputs');
  const buttons: Buttons = transition('webSiteConstant.buttons');
  const [bankInfo, setBankInfo] = useState<{ IbanNumber: number | null; creditCardNumber: number | null }>({
    IbanNumber: null,
    creditCardNumber: null,
  });

  // initial data
  const initialData = [
    { creditCardNumber: 56655656565656, IBAN: 'IR 6856248484849249' },
    { creditCardNumber: 56655656565656, IBAN: 'IR 6856248484849249' },
  ];

  const handleBankInfo = (type: string, value: any) => {
    return setBankInfo({ ...bankInfo, [type]: value });
  };

  /******************************************************************************
   *                            Formik Territory
   *****************************************************************************/
  const formik = {
    onSubmit: async (values: any, { setSubmitting }: any) => {
      await wait(WAITING_WITH);
      setSubmitting(false);
      return router.push('/dashboard');
    },
    initialValues: {
      firstName: '',
      lastName: '',
      phoneNumber: '',
      staticNumber: '',
      address: '',
      nationalCode: '',
      postalCode: '',
      birthDate: '',
    },
    validationSchema: Yup.object().shape({
      firstName: Yup.string().required(REQUIRED),
      lastName: Yup.string().required(REQUIRED),
      phoneNumber: Yup.string().required(REQUIRED),
      staticNumber: Yup.string().required(REQUIRED),
      address: Yup.string().required(REQUIRED),
      nationalCode: Yup.string().required(REQUIRED),
      postalCode: Yup.string().required(REQUIRED),
      birthDate: Yup.string().required(REQUIRED),
    }),
  };

  return (
    <AuthLayout>
      {/*  SECTION TOP  */}
      <Typography variant="subtitle1" sx={{ textAlign: 'center', mb: 3 }}>
        {personalIdentificationPage.header}
      </Typography>
      <Formik
        initialValues={formik.initialValues}
        validationSchema={formik.validationSchema}
        onSubmit={formik.onSubmit}
      >
        {({ values, errors, handleChange, handleSubmit, touched, isSubmitting }) => (
          <Form onSubmit={handleSubmit}>
            <Grid container columnSpacing={{ xs: 0, md: 2 }} rowSpacing={{ xs: 2, md: 0 }}>
              <Grid item xs={12} md={6}>
                <FormControl variant="outlined" fullWidth>
                  <CustomTextField
                    name="firstName"
                    placeholder="برای مثال : رضا"
                    label={inputsText.firstName}
                    value={values.firstName}
                    onChange={handleChange}
                    errors={errors.firstName && touched.firstName && errors.firstName}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} md={6}>
                <FormControl variant="outlined" fullWidth>
                  <CustomTextField
                    name="lastName"
                    placeholder="برای مثال : کریمی"
                    label={inputsText.lastName}
                    value={values.lastName}
                    onChange={handleChange}
                    errors={errors.lastName && touched.lastName && errors.lastName}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} md={6}>
                <FormControl variant="outlined" fullWidth>
                  <CustomTextField
                    name="nationalCode"
                    placeholder="برای مثال : 123456"
                    label={inputsText.nationalCode}
                    value={values.nationalCode}
                    onChange={handleChange}
                    errors={errors.nationalCode && touched.nationalCode && errors.nationalCode}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} md={6}>
                <FormControl variant="outlined" fullWidth>
                  <CustomTextField
                    name="birthDate"
                    placeholder="برای مثال : 1400/2/3"
                    label={inputsText.birthday}
                    value={values.birthDate}
                    onChange={handleChange}
                    errors={errors.birthDate && touched.birthDate && errors.birthDate}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} md={6}>
                <FormControl variant="outlined" fullWidth>
                  <CustomTextField
                    name="phoneNumber"
                    placeholder="برای مثال : 093*****"
                    label={inputsText.phoneNumber}
                    value={values.phoneNumber}
                    onChange={handleChange}
                    errors={errors.phoneNumber && touched.phoneNumber && errors.phoneNumber}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} md={6}>
                <FormControl variant="outlined" fullWidth>
                  <CustomTextField
                    name="staticNumber"
                    placeholder="برای مثال : 44******"
                    label={inputsText.landlinePhoneNumber}
                    value={values.staticNumber}
                    onChange={handleChange}
                    errors={errors.staticNumber && touched.staticNumber && errors.staticNumber}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <FormControl variant="outlined" fullWidth>
                  <CustomTextField
                    name="address"
                    placeholder="برای مثال : تهران ......"
                    label={inputsText.address}
                    value={values.address}
                    onChange={handleChange}
                    multiline
                    rows={4}
                    errors={errors.address && touched.address && errors.address}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} md={6}>
                <FormControl variant="outlined" fullWidth>
                  <CustomTextField
                    name="postalCode"
                    placeholder="برای مثال : 123456"
                    label={inputsText.postcode}
                    value={values.postalCode}
                    onChange={handleChange}
                    errors={errors.postalCode && touched.postalCode && errors.postalCode}
                  />
                </FormControl>
              </Grid>
            </Grid>

            <Divider sx={{ my: 5 }} />
            {/*  SECTION BOTTOM  */}

            <Typography variant="subtitle1" sx={{ textAlign: 'center', mb: 3 }}>
              {personalIdentificationPage.header}
            </Typography>
            <Box>
              <Box my={5}>
                <Grid container columnSpacing={{ xs: 0, md: 2 }} rowSpacing={{ xs: 2, md: 0 }}>
                  <Grid item xs={12} md={6}>
                    <FormControl variant="outlined" fullWidth>
                      <CustomTextField
                        placeholder="IR 45454545454"
                        label={inputsText.IBAN}
                        value={bankInfo.IbanNumber}
                        onChange={(e) => handleBankInfo('IbanNumber', e?.target?.value)}
                      />
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <FormControl variant="outlined" fullWidth>
                      <CustomTextField
                        placeholder="برای مثال : 123456"
                        label={inputsText.creditCardNumber}
                        value={bankInfo.creditCardNumber}
                        onChange={(e) => handleBankInfo('creditCardNumber', e?.target?.value)}
                      />
                    </FormControl>
                  </Grid>
                </Grid>
                <Grid container justifyContent="space-between" my={2}>
                  <Grid item xs={12} md={8} sx={{ display: 'flex', alignItems: 'center' }}>
                    <SvgIcon sx={{ mx: 1, color: (theme) => theme.palette.primary.main }}>
                      <ErrorOutlineOutlinedIcon />
                    </SvgIcon>
                    <Typography variant="caption" color="primary">
                      {personalIdentificationPage.caution}
                    </Typography>
                  </Grid>
                  <Grid item xs={12} md={3}>
                    <Button variant="outlined">
                      <AddToPhotosOutlinedIcon sx={{ fontSize: 18, ml: 1 }} />
                      {buttons.addingCard}
                    </Button>
                  </Grid>
                </Grid>
              </Box>

              {/*  TABLE  */}
              <Box>
                <PrimaryTable headers={personalIdentificationPage?.table} mainHead responsive>
                  {initialData?.map((row: any, index) => (
                    <TableRow
                      key={index}
                      sx={{ '& td': { color: (theme) => alpha(theme?.palette?.text?.secondary, 0.5), fontSize: 12 } }}
                    >
                      <TableCell>{row.creditCardNumber}</TableCell>
                      <TableCell align="right">{row.IBAN}</TableCell>
                      <TableCell align="right">
                        <SvgIcon
                          sx={{
                            mx: 1,
                            color: (theme) => theme.palette.primary.main,
                            border: (theme) => `1px solid ${theme.palette.primary.main}`,
                            borderRadius: '50%',
                            cursor: 'pointer',
                          }}
                        >
                          <ModeEditOutlineOutlinedIcon />
                        </SvgIcon>
                        <SvgIcon
                          sx={{
                            mx: 1,
                            color: (theme) => theme.palette.primary.main,
                            border: (theme) => `1px solid ${theme.palette.primary.main}`,
                            borderRadius: '50%',
                            cursor: 'pointer',
                          }}
                        >
                          <DeleteOutlinedIcon />
                        </SvgIcon>
                      </TableCell>
                    </TableRow>
                  ))}
                </PrimaryTable>
              </Box>

              <Divider />

              <Box my={5}>
                <Box my={3}>
                  <Typography
                    variant="body2"
                    dangerouslySetInnerHTML={{ __html: personalIdentificationPage.identiyCard }}
                  />
                </Box>

                {/*  UPLOADER  */}
                <Box my={5}>
                  <Paper
                    sx={{
                      border: (theme) => `2px dashed ${alpha(theme.palette.text?.secondary, 0.5)}`,
                      cursor: 'pointer',
                    }}
                  >
                    <FileUploader />
                  </Paper>
                </Box>
                <Box my={3}>
                  <Typography
                    variant="body2"
                    dangerouslySetInnerHTML={{ __html: personalIdentificationPage.pictureText }}
                  />
                </Box>
                <Typography variant="body2" sx={{ my: 3 }}>
                  {personalIdentificationPage.sampleTitle}
                </Typography>
                <CardMedia
                  component="img"
                  sx={{ borderRadius: '20px' }}
                  src="/static/images/acceotablepic.png"
                  alt="personal identification"
                />
              </Box>
            </Box>

            <Box sx={{ textAlign: 'center', my: 3 }}>
              <LoadingButton
                type="submit"
                loading={isSubmitting}
                loadingIndicator={<CircularProgress color="inherit" size={22} />}
                sx={{
                  border: 'none',
                  minWidth: 180,
                  py: (theme) => theme.spacing(2),
                }}
              >
                {buttons.profileRegistration}
              </LoadingButton>
            </Box>
          </Form>
        )}
      </Formik>
    </AuthLayout>
  );
};

export default PersonalIdentification;
